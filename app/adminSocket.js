var io;


exports.init = function(sio, socket) {
	io = sio;
	this.socket = socket;
	
	socket.on('subscribe', subscribe);
	socket.on('getLobbies', getLobbies);
	socket.on('getLobby', getLobby);

	socket.emit('initialized');
}

function getLobbies() {
	var stats = require('./stats');
	this.emit('getLobbies', stats.getAll());
}

function getLobby(statId) {
	var stats = require('./stats');
	var data = stats.getLobbyStats(statId);
	console.log(data);
	this.emit('getLobby', data);
}	

function subscribe(statId) {
	var stats = require('./stats');
	stats.subscribe(statId, this);
};

function getLobbyInfo(socket) {
	var lobbyManager = require('./lobbyManager');
	var stats = require('./stats');
	var data = [];

	for (var i = 0; i < lobbyManager.getLobbies().length; i++) {
		var lobby = lobbyManager.getLobbies()[i];
		
		if (lobby !== undefined) {
			var lobbyData = {
				players : [],
			};

			//Get player names
			for (var j = 0; j < lobby.players.length; j++) {
				lobbyData.players.push(lobby.players[j].name);
			}
			
			//Active game
			if (lobby.playing)
				lobbyData.game = lobby.game;

			//Playing - could be left out since we send the game
			//only when we are playing
			lobbyData.playing = lobby.playing;

			data.push(lobbyData);
		}
	}

	socket.emit('lobbyInfo', data);
}
