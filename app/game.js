function Game(pathname, config) {
	this.name = config.name;
	this.description = config.description;
	this.minPlayers = config.minPlayers;
	this.maxPlayers = config.maxPlayers;
	this.options = config.options;

	this.path = pathname;
};

module.exports = Game;