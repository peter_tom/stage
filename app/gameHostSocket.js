var io;
var socket;

exports.init = function(sio, socket) {
	io = sio;
	this.socket = socket;

	var lobbyManager = require('./lobbyManager');
	var sessionId = socket.handshake.sessionId;
	var lobby = lobbyManager.getHostLobby(sessionId);

	if (lobby !== undefined && lobby.host.socket === undefined) {
		socket.join(lobby.id);
		
		socket.lobbyId = lobby.id;

		socket.on('emit', onEmit);
		socket.on('emitAll', onEmitAll);
		socket.on('returnToLobby', onReturnToLobby);

		socket.on('disconnect', onDisconnect);
		socket.notifyConnect = checkPlayerState;	
		socket.notifyDisconnect = onPlayerDisconnect;
		
		lobby.host.socket = socket;
		socket.emit('initialized', lobby.game.parameters);
		socket.notifyConnect();

		var stats = require('./stats');
		stats.registerGame(lobby.id, lobby.game.name);
	} else {
		redirectToIndex(socket);
	}
}

function redirectToIndex(socket) {
	socket.emit('redirect', '/');	
}

function onEmit(data) {
	var lobbyManager = require('./lobbyManager');
	var stats = require('./stats');

	var lobby = lobbyManager.lobby(this.lobbyId);
	
	if (lobby !== undefined) {
		if (data !== undefined){
			var player = lobby.getPlayerByName(data.player);
			
			if (player !== undefined) {
				player.socket.emit(data.func, data.data);
				stats.log(lobby.id, stats.type.Screen, data.func);
			} else {
				this.emit('sError', 'Player not found.');
				stats.log(lobby.id, stats.type.Screen, 'sError');
			}
		} else {
			this.emit('sError', 'Data was corrupt.');
			stats.log(lobby.id, stats.type.Screen, 'sError');
		}
	} else {
		redirectToIndex(this);
	}
}

function onEmitAll(data) {
	var lobbyManager = require('./lobbyManager');
	var stats = require('./stats');
	
	var lobby = lobbyManager.lobby(this.lobbyId);

	if (lobby !== undefined) {
		stats.log(lobby.id, stats.type.Screen, data.func);
		lobby.host.socket.broadcast.emit(data.func, data.data);
	} else {
		console.log("onEmitAll redirect host");
		redirectToIndex(this);
	}
}

function onReturnToLobby() {
	var lobbyManager = require('./lobbyManager');

	var lobby = lobbyManager.lobby(this.lobbyId);

	if (lobby !== undefined) {
		lobby.playing = false;
		lobby.host.socket = undefined;

		lobby.players.forEach(function(player) {
			if (player.socket !== undefined) {
				player.socket.emit('redirect', '/lobby/' + lobby.id);
				player.socket = undefined;
			}
		});
		this.emit('redirect', '/lobby/');
	} else {
		redirectToIndex(this);
	}	
}

function checkPlayerState() {
	if (allPlayersLoaded(this.lobbyId)) {	
		var lobbyManager = require('./lobbyManager');

		var lobby = lobbyManager.lobby(this.lobbyId);

		this.emit('allPlayersLoaded', lobby.getAllPlayerData());
	}
}

function allPlayersLoaded(lobby) {
	var loaded = true;

	var lobbyManager = require('./lobbyManager');

	var lobby = lobbyManager.lobby(lobby);
	for (var i = 0; i < lobby.players.length && loaded; i++) {
		var player = lobby.players[i];
		if (player.socket === undefined) {
			loaded = false;
		}
	}
	return loaded;
}

function onPlayerDisconnect(player) {
	console.log('Player disconnected from game ' + player);
	this.emit('playerDisconnected', player);
}

function onDisconnect() {
	var lobbyManager = require('./lobbyManager');

	var lobby = lobbyManager.lobby(this.lobbyId);

	if (lobby !== undefined) {
		if (lobby.playing) {
			//disconnect fo real
			lobby.players.forEach(function(player) {
				if (player.socket !== undefined){
					player.socket.emit('redirect', '/');
				}
			});

			lobbyManager.removeLobby(this.lobbyId);
		}
	}
}