var io;
var socket;

exports.init = function(sio, socket){
	io = sio;
	this.socket = socket;

	var lobbyManager = require('./lobbyManager');

	var sessionId = socket.handshake.sessionId;
	var lobby = lobbyManager.getPlayerLobby(sessionId);

	if (lobby !== undefined) {
		var user = lobby.getPlayerById(sessionId);
		//user is always defined since getPlayerLobby uses it, but check anyway.

		if (user !== undefined && user.socket === undefined) {
			console.log('Player connected to game.');

			socket.join(lobby.id);
			socket.lobbyId = lobby.id;
			socket.sessionId = sessionId;
			
			user.socket = socket;

			//Check if host already loaded his socket.
			//If so, inform him that we connected.
			if (lobby.host.socket !== undefined) {
				//If this method is not available it means that the host is active on
				//a page different then the game (eg. lobby)
				//If this happens, send him to the lobby screen.
				if (typeof lobby.host.socket.notifyConnect == 'function') {
					lobby.host.socket.notifyConnect();	
				} else {
					socket.emit('redirect', '/lobby/' + lobby.id);
				}
			}

			socket.on('emit', onEmit);
			socket.on('disconnect', onDisconnect);

			socket.emit('sendPlayerData', lobby.getPlayerData(user));
			socket.emit('initialized');
		} else {
			console.log('Game: User socket already set');
			redirectToIndex(socket);	
		}
	} else {
		console.log('Game: Lobby does not exist for ' + sessionId);
		redirectToIndex(socket);
	}
}

function redirectToIndex(socket) {
	socket.emit('redirect', '/');	
}

function onEmit(data) {
	var lobbyManager = require('./lobbyManager');
	var stats = require('./stats');

	var lobby = lobbyManager.lobby(this.lobbyId);

	if (lobby !== undefined) {
		var host = lobby.host.socket;
		
		if (host !== undefined) {
			var user = lobby.getPlayerById(this.sessionId);
			if (user !== undefined) {
				sendData = {
					player : user.name,
					data : data.data,
				};
				stats.log(lobby.id, stats.type.Controller, data.func, user.name);
				host.emit(data.func, sendData);
			} else {
				redirectToIndex(this);
			}
		} else {
			stats.log(lobby.id, stats.type.Controller, 'cError', 'undefined');
			this.emit('cError', 'Host not connected?');
		}
	} else {
		console.log("emit redirect player");
		redirectToIndex(this);
	}
}

function onDisconnect() {
	console.log('Player disconnected from game');
	var lobbyManager = require('./lobbyManager');

	var lobby = lobbyManager.lobby(this.lobbyId);

	//redirect to lobby
	if (lobby !== undefined) {
		var user = lobby.getPlayerById(this.sessionId);
		
		if (user !== undefined) {
			if (lobby.playing) {
				lobby.removePlayer(this.sessionId);
			}
		}
	}
}