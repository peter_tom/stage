var io;
var gameSocket;
var room;
/**
 * This function is called by index.js to initialize a new game instance.
 */
exports.init = function(sio, socket, sroom){
    io = sio;
    gameSocket = socket;
    room = sroom;

    gameSocket.join(room);
    gameSocket.on('moveController', move);

    getHost();
    
    gameSocket.emit('socketLoaded');
}

function getHost() {
	//TODO: geen foreach.
	//Move naar andere klasse, abstract pls
    io.sockets.clients(room).forEach(function(socket) {
    	if (socket.game) {
    		console.log("Found host: " + socket.id);
    		gameSocket.host = socket;
    	}
    });
}

function move(dir) {
    this.host.move(dir);
}

function joinRoom(data) {
	gameSocket.leave(gameSocket.room);
	gameSocket.join(data.room);

	if(io.sockets.in(data.room).count == 1) {
		gameSocket.host = true;
		gameSocket.on('getPostition', getPostition);
		gameSocket.on('receiveMove', receiveMove);
	}
}