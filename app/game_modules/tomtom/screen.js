var io;
var gameSocket;
var game = "tomtom"
/**
 * This function is called by index.js to initialize a new game instance.
 */
exports.init = function(sio, socket){
    io = sio;
    gameSocket = socket;

    gameId = (Math.random() * 10000) | 0;
    gameSocket.join(gameId);
    gameSocket.room = gameId;
    gameSocket.game = game;

    var square = {
        x : 100,
        y : 100,
    }

    gameSocket.square = square;

    gameSocket.move = move;
    gameSocket.on('getRoom', onGetRoom);

    gameSocket.emit('socketLoaded');
}

function onGetRoom() {
    console.log("Sending room " + gameSocket.room);
    gameSocket.emit('roomId', gameSocket.room);
}

function move(dir) {
    dir = Number(dir);
    switch(dir) {
        case 1: this.square.y -= 10;
                break;
        case 2: this.square.y += 10;
                break;
        case 3: this.square.x -= 10;
                break;
        case 4: this.square.x += 10;
                break;
    }
    gameSocket.emit('sync', this.square);
}