var games = new Array();
var fs = require('fs');
var folderGames = fs.readdirSync("./public/games");
var Game = require("./game");

var games;
for (var i = 0; i < folderGames.length; i++) {
	var game = folderGames[i];
	var config = require('./public/games/' + game + '/config');
	games.push(new Game(game, config));
}

module.exports = games;