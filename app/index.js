var io;
var socket;

/**
 * This function is called by index.js to initialize a new game instance.
 */
exports.init = function(sio, socket){
	io = sio;
	this.socket = socket;

	socket.on('createLobby', onCreateLobby);
	socket.on('joinLobby', onJoinLobby);
}

function onCreateLobby() {
	var lobbyManager = require('./lobbyManager');

	var sessionId = this.handshake.sessionId;

	if(lobbyManager.getHostLobby(sessionId) !== undefined){
		this.emit("sError", "You are already hosting a lobby.");
	} else {
		var playerLobby = lobbyManager.getPlayerLobby(sessionId);
	
		if(playerLobby !== undefined && playerLobby.getPlayerById(sessionId).socket !== undefined){
			this.emit("sError", "You are already in a lobbby.");
		} else {
			this.emit('redirect', "/lobby");
		}
	}
}

function onJoinLobby(lobbyId, override) {
	if (override === null)
		override = false;

	var lobbyManager = require('./lobbyManager');
	var lobby = lobbyManager.lobby(lobbyId);

	var sessionId = this.handshake.sessionId;

	if (lobby === undefined) //Lobby not found
		this.emit("sError", "Lobby not found.");
	else if (lobby.playing) //Already in-game
		this.emit("sError", "A game is active, you can only join when they are in the lobby.")
	else {
		var hostLobby = lobbyManager.getHostLobby(sessionId);

		if (hostLobby !== undefined) { //Already has a lobby
			if (!override)
				this.emit("sForceJoinQuestion", "You are already hosting a lobby.");
			else {
				lobbyManager.removeLobby(hostLobby.id);
				this.emit('redirect', "/lobby/" + lobbyId);
			}
		} else { //Already in a lobby
			var playerLobby = lobbyManager.getPlayerLobby(sessionId);
		
			if (playerLobby !== undefined && playerLobby.getPlayerById(sessionId).socket !== undefined) {
				if (!override)
					this.emit("sForceJoinQuestion", "You are already in a lobby.");
				else {
					playerLobby.getPlayerById(sessionId).socket.emit('redirect', '/');
					playerLobby.removePlayer(sessionId);
					this.emit('redirect', "/lobby/" + lobbyId);
				}
			} else {
				this.emit('redirect', "/lobby/" + lobbyId);
			}
		}
	}
}