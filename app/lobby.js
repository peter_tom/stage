var User = require('./user');

function Lobby(id, host) {
	this.id = id;
	this.host = host;
	this.game = {name : undefined, parameters : undefined};
	this.playing = false;

	this.players = new Array();
};

Lobby.prototype.addPlayer = function(sessionId) {
	if (sessionId === undefined)
		return false;

	var user = new User(sessionId);
	user.name = this.getUniqueName();
	
	this.players.push(user);

	return user;
};

Lobby.prototype.removePlayer = function(sessionId) {
	var ply = this.getPlayerById(sessionId);

	if (ply !== undefined) {
		for (var i = 0; i < this.players.length; i++) {
			if (this.players[i].sessionId === sessionId) {
				if (this.host.socket !== undefined) {
					if (this.playing) {			
						if (typeof this.host.socket.notifyDisconnect !== "undefined")
							this.host.socket.notifyDisconnect(ply.name);
					} else {
						this.update();
					}
				}
				if (ply.socket !== undefined)
					ply.socket.emit('redirect', '/');
				
				this.players.splice(i, 1);
				break;
			}	
		}
	}
};

Lobby.prototype.getAllPlayerData = function() {
	var data = new Array();
	if (this.players !== undefined) {
		for (var i = 0; i < this.players.length; i++) {
			data.push(this.getPlayerData(this.players[i]));
		};
	}

	return data;
};

Lobby.prototype.getPlayerData = function(user) {
	var data = undefined;
	if(user !== undefined){
		var data = {
			name : user.name,
			color : user.color,
			connected : (user.socket !== undefined),
		};
	}
	return data;
};


Lobby.prototype.getPlayerById = function(sessionId){
	for(var i = 0; i < this.players.length; i++){
		if(this.players[i].sessionId === sessionId){
			return this.players[i];
		}
	}
	return undefined;
};

Lobby.prototype.getPlayerByName = function(name) {
	for(var i = 0; i < this.players.length; i++){
		if(this.players[i].name === name){
			return this.players[i];
		}
	}
	return undefined;	
};

Lobby.prototype.updatePlayer = function(plyId, data) {
	var ply = this.getPlayerById(plyId);

	if (ply !== undefined && data !== undefined && !this.playing) {
		if (this.isUniqueName(data.name)) {
			ply.name = data.name;
		}
		ply.color = data.color;

		this.update();
	}
};

var names = new Array('Tom', 'Peter', 'Sinterklaas', 'Zwarte Piet', 'Piet Huysentruyt', 'Luc Janssens', 'Emma Watson', 'Sophia Bush', 'Emma Stone', 'De buurman', 'Postbode Jos', 'De Royco expert', 'McTommyTom', 'Lebron James', 'Big boy', 'The Hulk');
Lobby.prototype.getUniqueName = function() {
	var name = names[Math.floor(Math.random()*names.length)];
	var count = 1;
	if (!this.isUniqueName(name)) {
		while (!this.isUniqueName(name + ' ' + count)) {
			count++;
		}
		name = name + ' ' + count;
	}
	return name;
};

Lobby.prototype.isUniqueName = function(name) {
	var unique = true;

	this.players.forEach(function(user) {
		if (user.name === name) {
			unique = false;
			return;
		}
	});
	return unique;
};

Lobby.prototype.update = function() {
	if (this.host.socket !== undefined && typeof this.host.socket.update == 'function') {
		this.host.socket.update(this.getAllPlayerData());
	}
};

Lobby.prototype.socketCheck = function(socket, sessionId) {
	var ply = this.getPlayerById(sessionId);

	return ply !== undefined && socket !== undefined && ply.socket !== undefined && ply.socket.id === socket.id;
}

module.exports = Lobby;