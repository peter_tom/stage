var io;
var socket;

exports.init = function(sio, socket){
	io = sio;
	this.socket = socket;

	var lobbyManager = require('./lobbyManager');
	var sessionId = socket.handshake.sessionId;

	if(lobbyManager.getPlayerLobby(sessionId) !== undefined){
		redirectToIndex(socket);
	} else {
		var lobby = lobbyManager.getHostLobby(sessionId);

		if(lobby === undefined){
			lobby = lobbyManager.createLobby(sessionId);
			initSocket(socket, lobby);
		} else if (lobby.host.socket === undefined) {
			initSocket(socket, lobby);
		} else {
			redirectToIndex(socket);
		}
	}
}

function initSocket(socket, lobby) {
	socket.join(lobby.id);
	socket.lobbyId = lobby.id;

	lobby.host.socket = socket;

	socket.on('startGame', onStartGame);
	socket.on('disconnect', onDisconnect);
	socket.on('kick', onKickPlayer);
	socket.update = update;	

	var games = require('./games');

	var data = {
		id : lobby.id,
		selectedGame : lobby.game,
		games : games,
	};
	
	socket.emit('sendLobbyData', data);
	lobby.update();
}

function redirectToIndex(socket) {
	socket.emit('redirect', '/');
}

function onKickPlayer(name) {
	var lobbyManager = require('./lobbyManager');
	var lobby = lobbyManager.lobby(this.lobbyId);
	
	try {
		lobby.removePlayer(lobby.getPlayerByName(name).sessionId);
	} catch(e) {

	}
	lobby.update();
}

function onStartGame(data) {	
	var lobbyManager = require('./lobbyManager');
	var lobby = lobbyManager.lobby(this.lobbyId);

	if (lobby !== undefined) {
		lobby.playing = true;
		lobby.host.socket = undefined;
		
		lobby.game.name = data.game;
		lobby.game.parameters = data.parameters;

		lobby.players.forEach(function(player) {
			if (player.socket !== undefined) {
				player.socket.emit('redirect', '/play/' + lobby.game.name + '/');
				player.socket = undefined;
			}
		});

		this.emit('redirect', '/host/' + lobby.game.name + '/');
	} else {
		redirectToIndex(this);
	}

}

function update(data) {
	this.emit('update', data);
}

function onDisconnect() {
	var lobbyManager = require('./lobbyManager');

	var lobby = lobbyManager.lobby(this.lobbyId);

	if (lobby !== undefined) {
		if (!lobby.playing) {
			lobbyManager.removeLobby(this.lobbyId);
		}
	}
}