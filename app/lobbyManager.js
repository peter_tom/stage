var MAX_ROOMS = 10;
var lobbies = new Array();
var currIndex = 0;

function getHostLobby(sessionId) {
	if (sessionId === undefined)
		return undefined;

	var foundLobby = undefined;

	lobbies.forEach(function(lobby) {
		if (lobby.host.sessionId === sessionId) {
			foundLobby = lobby;
		}
	});

	return foundLobby;
}

function lobby(id) {
	return lobbies[id];
}

function createLobby(sessionId) {
	if (getHostLobby(sessionId) === undefined) {
		var User = require('./user');
		var user = new User(sessionId);

		var Lobby = require('./lobby');
		var lobby = new Lobby(getUniqueLobbyId(), user);

		lobbies[lobby.id] = lobby;

		return lobby;
	}
	return undefined;
}

function removeLobby(lobbyId) {
	var lobby = lobbies[lobbyId];
	if (lobby !== undefined) {
		//Redirect host
		if (lobby.host.socket !== undefined) {
			lobby.host.socket.emit('redirect', '/');
		}
		
		//Redirect players
		lobby.players.forEach(function(player) {
			if (player.socket !== undefined) {
				player.socket.emit('redirect', '/');
			}
		});
		delete lobbies[lobbyId];
	}
}

function getUniqueUserId(lobby) {
	if (lobby !== undefined && lobby.users !== undefined)
		return getUniqueId(lobby.users, 256);
	return undefined;
}

function getUniqueLobbyId() {
	return getUniqueId(lobbies, MAX_ROOMS);
}

function getUniqueId(array, max) {
	var shitSaver = 0;
	
	while(array[currIndex] !== undefined) {
		currIndex++;
		shitSaver++;
		if (shitSaver >= MAX_ROOMS) {
			console.log('Shit saved');
			return undefined;
		}
	}

	return currIndex;
}

function getPlayerLobby(sessionId){
	var result = undefined;
	lobbies.forEach(function(lobby){
		var user = lobby.getPlayerById(sessionId);
		if(user !== undefined){
			result = lobby;
			return result;
		}
	});
	return result;
}

function reset() {
	lobbies = new Array();
	currIndex = 0;
}

function getLobbies() {
	return lobbies;
}

module.exports.getHostLobby = getHostLobby;
module.exports.lobby = lobby;
module.exports.createLobby = createLobby;
module.exports.removeLobby = removeLobby;
module.exports.getUniqueUserId = getUniqueUserId;
module.exports.getUniqueLobbyId = getUniqueLobbyId;
module.exports.getPlayerLobby = getPlayerLobby;
module.exports.reset = reset;
module.exports.getLobbies = getLobbies;