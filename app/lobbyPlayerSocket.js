var io;
var socket;

exports.init = function(sio, socket, lobbyId){
	io = sio;
	this.socket = socket;

	var sessionId = socket.handshake.sessionId;
	var lobbyManager = require('./lobbyManager');
	var lobby = lobbyManager.lobby(lobbyId);
	
	console.log('Player is trying to join a lobby.')
	console.log(socket.id);

	if (lobby === undefined || lobby.playing) {
		redirectToIndex(socket);
	} else {
		var hostLobby = lobbyManager.getHostLobby(sessionId);

		if (hostLobby === undefined) {
			//If he's hosting a lobby, return him to index, we aint
			//gon allow that mah boi
			var ply = lobby.getPlayerById(sessionId);

			if (ply === undefined) {
				//The player is new
				var ply = lobby.addPlayer(sessionId);

				initSocket(socket, lobby, ply);
			} else if(ply.socket === undefined) {
				//The player is returning from a game to the lobby.
				//Update the socket so we can send shit to him.
				initSocket(socket, lobby, ply);
			} else {
				//If the socket is defined it means that there is
				//already a socket connected, we don't want
				//more then one tab to control the lobby,
				//so redirect this socket/tab to the index
				redirectToIndex(socket);
			}
		} else {
			redirectToIndex(socket);
		}
	}
}

function initSocket(socket, lobby, ply) {
	socket.lobbyId = lobby.id;
	socket.sessionId = socket.handshake.sessionId;
	socket.join(lobby.id);
	socket.on('updatePlayerData', onUpdatePlayerData);
	socket.on('disconnect', onDisconnect);

	ply.socket = socket;
	ply.socket.emit('getPlayerData', lobby.getPlayerData(ply));
	
	lobby.update();
}

function onUpdatePlayerData(data) {
	var lobbyManager = require('./lobbyManager');
	var lobby = lobbyManager.lobby(this.lobbyId);
	var sessionId = this.sessionId;

	if (lobby !== undefined && lobby.socketCheck(this, sessionId)) {
		lobby.updatePlayer(this.sessionId, data);
	} else {
		redirectToIndex(this);
	}
}

function redirectToIndex(socket){
	socket.emit('redirect', '/');
}

function onDisconnect() {
	var lobbyManager = require('./lobbyManager');
	var lobby = lobbyManager.lobby(this.lobbyId);
	var sessionId = this.sessionId;

	//Als lobby niet bestaat, doe niks.
	if (lobby !== undefined && lobby.socketCheck(this, sessionId)) {
		if (!lobby.playing) {
			lobby.removePlayer(this.sessionId);
			lobby.update();	
		}
	}
}