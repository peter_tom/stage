var express = require('express');
var app = express();
var cookiesecret = 'We can go where we want to, we can leave your friends behind.';

var MemoryStore = express.session.MemoryStore,
	sessionStore = new MemoryStore();

// Create a simple Express application
app.configure(function() {
	//Log every request
	//app.use(express.logger('dev'));
	app.use(express.cookieParser());
	app.use(express.session({
		store : sessionStore,
		key : 'ply.sid',
		secret : cookiesecret,
	}));
	app.use(app.router);
	app.use(express.static(__dirname + '/public'));
});

var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

var parseSignedCookie = require('connect').utils.parseSignedCookie;

//Verminder logging, anders staat de console vol me DEBUG shit.
io.set('log level', 1);
var port = process.env.PORT || 80;
server.listen(port);

var indexSocket = require('./index');
var lobbyHostSocket = require('./lobbyHostSocket');
var lobbyPlayerSocket = require('./lobbyPlayerSocket');
var gameHostSocket = require('./gameHostSocket');
var gamePlayerSocket = require('./gamePlayerSocket');
var adminSocket = require('./adminSocket');

var Games = require('./games');
var User = require('./user');
var Lobby = require('./lobby');
var lobbyManager = require('./lobbyManager');

app.get('/host/:game', function(req, res) {
	var game = req.params.game;

	res.sendfile(__dirname + '/public/games/' + game + '/screen.html');
});

app.get(/host\/(.*)/, function(req, res) {
	res.sendfile(__dirname + '/public/games/' + req.params);
});

app.get('/play/:game', function(req, res) {
	var game = req.params.game;

	res.sendfile(__dirname + '/public/games/' + game + '/controller.html');
});

app.get(/play\/(.*)/, function(req, res) {
	res.sendfile(__dirname + '/public/games/' + req.params);
});

app.get('/createlobby', function(req, res) {
	res.sendfile(__dirname + '/public/lobby/hostlobby.html');
});

app.get('/lobby/:id', function(req, res) {

	var id = req.params.id;

	if (isNaN(id)) {
		res.sendfile(__dirname + '/public/lobby/' + id);
	} else {
		res.sendfile(__dirname + '/public/lobby/playerlobby.html');
	}
});

app.get('/lobby/', function(req, res) {	
	res.sendfile(__dirname + '/public/lobby/hostlobby.html');
});

//Als we de vorige app.get's niet hebben getriggered gaan we gewoon standaard pagina doen.
app.get('/', function(req, res) {
	res.sendfile(__dirname + '/public/index.html');
});

io.configure(function() {
	io.set('authorization', function (handshakeData, callback) {
		var cookie = handshakeData.headers.cookie;
	    if (cookie) {
			cookie = cookie.split("; ");
			var realcookie = undefined;
			cookie.forEach(function(c){
				var key = c.split("=");
				if (key[0] === 'ply.sid')
					realcookie = key[1].substring(4, 28);
			});

			if (realcookie === undefined) {
				callback('No valid cookie', false);
			} 

			handshakeData.sessionId = realcookie;
		} else {
		    return callback('No cookie transmitted.', false);
		} 

		callback(null, true);
  	});
});

io.sockets.on('connection', function(socket) {

	socket.on('homeSocket', function() {
		indexSocket.init(io, socket);
	});

	socket.on('lobbyHostSocket', function() {
		lobbyHostSocket.init(io, socket);
	});

	socket.on('lobbyPlayerSocket', function(lobby) {
		lobbyPlayerSocket.init(io, socket, lobby);
	});

	socket.on('gameHostSocket', function() {
		gameHostSocket.init(io, socket);
	});

	socket.on('gamePlayerSocket', function() {
		gamePlayerSocket.init(io, socket);
	});

	socket.on('adminSocket', function() {
		adminSocket.init(io, socket);
	});

	socket.emit('connected');
});