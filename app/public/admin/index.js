Dashboard = {
    overviewStats : {},

    init : function() {
        Dashboard.socket = io.connect();
        Dashboard.bindEvents(); 
    },

    bindEvents : function() {
        Dashboard.socket.on('lobbyInfo', Dashboard.loadLobbyInfo);
        Dashboard.socket.on('stats', Dashboard.loadStats);
        Dashboard.socket.on('connected', Dashboard.onConnected);
    },

    onConnected : function() {
        Dashboard.socket.emit('adminSocket');
    },

    loadLobbyInfo : function(data) {
        console.log(data);
        var nLobbies = data.length;
        var nPlayers = 0;

        for (var i = 0; i < data.length; i++) {
            nPlayers += data[i].players.length;
        }

        var avgPlayers = nPlayers / nLobbies;
        var playingLobbies = 0;

        for (var i = 0; i < data.length; i++) {
            var lobby = data[i];

            if (lobby.playing)
                playingLobbies++;
        }

        //Set HTML
        $('#nLobbies').text(nLobbies);
        $('#nPlayers').text(nPlayers);
        $('#avgPlayers').text(avgPlayers);
        $('#playingLobbies').text(playingLobbies);
    },

    loadStats : function(data) {
        Dashboard.overviewStats = data;

        var cats = [];
        var series = [];
        for (var func in data[1].screenEmits) {
            cats.push(func);
            series.push(data[1].screenEmits[func]);
        }

        $('#chart').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Game Controller'
            },
            xAxis: {
                categories: cats
            },
            yAxis: {
                min : 0,
                title: {
                    text: 'nCalls'
                }
            },
            series: [{
                name: 'Controller',
                data: series
            }],
        });
    },
};

jQuery(function($){    
    Dashboard.init();
});