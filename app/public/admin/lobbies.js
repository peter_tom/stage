Lobbies = {
    overviewStats : {},
    selectedLobby : -1,

    init : function() {
        Lobbies.socket = io.connect();
        Lobbies.bindEvents(); 
    },

    bindEvents : function() {
        Lobbies.socket.on('connected', Lobbies.onConnected);        
        Lobbies.socket.on('initialized', Lobbies.onInitialized);

        Lobbies.socket.on('lobbyInfo', Lobbies.loadLobbyInfo);
        Lobbies.socket.on('getLobbies', Lobbies.loadLobbies);
    },

    onConnected : function() {
        console.log('Connected to server.');
        Lobbies.socket.emit('adminSocket');
    },

    onInitialized : function() {
        console.log('Initialized');
        Lobbies.socket.emit('getLobbies');
    },

    loadLobbies : function(data) {
        console.log('Loading lobbies..');
        Lobbies.overviewStats = data;
        var table = $('#lobbyTable');

        function makeTd(data) {
            return '<td>' + data + '</td>';
        }

        for(var i = 0; i < data.length; i++) {
            var lobby = data[i];
            var row = '<tr>';
            var screenEmits = 0;
            var controllerEmits = 0;

            for(var s in lobby.screenEmits)
                screenEmits += lobby.screenEmits[s];
            for(var c in lobby.controllerEmits) {
                for(var func in lobby.controllerEmits[c]) {
                    controllerEmits += lobby.controllerEmits[c][func];
                }
            }

            row += makeTd(lobby.id);
            row += makeTd(lobby.game);
            row += makeTd(screenEmits);
            row += makeTd(controllerEmits);
            row += makeTd('<a href="lobbyDetail.html?id=' + lobby.id + '">Detail</a>');
            row += '</tr>';
            $('#lobbyTable > tbody:last').append(row);
        }
    },
};

jQuery(function($){    
    Lobbies.init();
});