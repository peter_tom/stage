LobbyDetail = {
    lobbyStats : {},

    init : function() {
        LobbyDetail.socket = io.connect();
        LobbyDetail.bindEvents(); 
    },

    bindEvents : function() {
        LobbyDetail.socket.on('connected', LobbyDetail.onConnected);
        LobbyDetail.socket.on('initialized', LobbyDetail.onInitialized);

        LobbyDetail.socket.on('getLobby', LobbyDetail.loadLobby);
        LobbyDetail.socket.on('notify', LobbyDetail.notify)
    },

    onConnected : function() {
        LobbyDetail.socket.emit('adminSocket');
    },

    onInitialized : function() {
        //Get url
        var sParam = "id";
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        var value;
        for (var i = 0; i < sURLVariables.length; i++) 
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) 
            {
                value = sParameterName[1];
            }
        }
        if (value === undefined)
            window.location = '/admin/lobbies.html';

        LobbyDetail.socket.emit('subscribe', value);
        LobbyDetail.socket.emit('getLobby', value);
    },

    loadLobby : function(data) {
        console.log(data);
        LobbyDetail.lobbyStats = data;

        //LobbyDetail.loadPlayers();
        LobbyDetail.initGraphs();
        LobbyDetail.updateGraphs();
    },

    loadPlayers : function() {
        var table = $('#playerTable');
        var data = LobbyDetail.lobbyStats;

        if (data === null) {
            //window.location = '/admin/lobbies.html';
            //return;
        }

        function makeTd(data) {
            return '<td>' + data + '</td>';
        }            

        for(var c in data.controllerEmits) {
            var ply = data.controllerEmits[c];
            
            var row = '<tr>';
            row += makeTd(lobby.id);
            row += makeTd(lobby.game);
            row += makeTd(screenEmits);
            row += makeTd(controllerEmits);
            row += makeTd('<a href="#" onClick="Lobbies.selectLobby(' + lobby.id + ')">View Graph</a>');
            row += '</tr>';
            $('#lobbyTable > tbody').empty();
            $('#lobbyTable > tbody:last').append(row);
        }
    },

    notify : function(data) {
        console.log('Got notified');
        console.log(data);
        LobbyDetail.lobbyStats = data;

        //Lobbies.updateTableRow();
        LobbyDetail.updateGraphs();
    },

    updateTableRow : function(data) {

    },

    initGraphs : function() {
        $('#screenChart').highcharts({
            chart: {
                type: 'column',
            },
            title: {
                text: 'Game',
            },
            xAxis: {
                categories: [],
            },
            yAxis: {
                min : 0,
                title: {
                    text: 'Calls',
                },
            },
            series: [{
                name : 'Function',
                data : [],
            }],
        });


        $('#controllerChart').highcharts({
            chart: {
                type: 'column',
            },
            title: {
                text: 'Controllers',
            },
            xAxis: {
                categories: [],
            },
            yAxis: {
                min : 0,
                title: {
                    text: 'Calls'
                },
            },
            series: [{
                name : 'Function',
                data : [],
            }],
        });

        $('#screenVScontrollerChart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Total emits'
            },
            plotOptions: {
                pie: {
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Emits',
                data: []
            }]
        });
    },

    updateGraphs : function() {
        if (LobbyDetail.lobbyStats === undefined)
            return;

        var screenChart = $('#screenChart').highcharts();
        var controllerChart = $('#controllerChart').highcharts();
        var totalEmitChart = $('#screenVScontrollerChart').highcharts();
        var data = LobbyDetail.lobbyStats;

        var screenCats = [];
        var controllerCats = controllerChart.xAxis[0].categories;
        var screenSeries = [];
        var controllerSeries = [];

        var sEmits = 0;
        var cEmits = 0;

        for (var func in data.screenEmits) {
            screenCats.push(func);

            var emits = data.screenEmits[func];
            screenSeries.push(emits);
            sEmits += emits;
        }
        //Loop through all users
        for (var user in data.controllerEmits) {
            var userSeries = {
                name : user,
                data : [],
            };

            for (var func in data.controllerEmits[user]) {
                var index = LobbyDetail.findControllerCatIndex(controllerCats, func);
                if (index === -1) {
                    controllerCats.push(func);
                    index = controllerCats.length - 1;
                }
                var emits = data.controllerEmits[user][func]
                userSeries.data[index] = emits;
                cEmits += emits;
            }
            controllerSeries.push(userSeries);
        }

        //Total chart
        totalEmitChart.series[0].setData([['Screen', sEmits], ['Controller', cEmits]]);

        //Screen chart
        screenChart.xAxis[0].setCategories(screenCats);
        screenChart.series[0].setData(screenSeries);

        //Controller chart
        controllerChart.xAxis[0].setCategories(controllerCats);

        for (var i = 0; i < controllerSeries.length; i++) {
            var serie = controllerSeries[i];

            //Process data
            for (var j = 0; j < controllerCats.length; j++) {
                var seriePoint = serie.data[j];
                if (seriePoint === undefined)
                    serie.data[j] = 0;
            }

            if (controllerChart.series[i] === undefined) {
                controllerChart.addSeries({data : serie.data, name : serie.name})
            } else {
                console.log(serie.data);
                controllerChart.series[i].setData(serie.data);
                controllerChart.series[i].update({ name : serie.name});
            }
        }
        /*
        //Calculate max
        screenChart.yAxis[0].max = null;
        controllerChart.yAxis[0].max = null;
        
        var max = screenChart.yAxis[0].max;
        var max2 = controllerChart.yAxis[0].max;
        console.log()
        if (max < max2)
            max = max2;

        screenChart.yAxis[0].max = max;
        controllerChart.yAxis[0].max = max;
        console.log(max);*/
    },

    findControllerCatIndex : function(a, func) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] === func)
                return i;
        }
        return -1;
    },
};

jQuery(function($){    
    LobbyDetail.init();
});