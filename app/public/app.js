    var IO = {

        init: function() {
            IO.socket = io.connect();
            IO.bindEvents();
            $('#inputLobby').focus();
        },

        bindEvents : function() {
            IO.socket.on('connected', IO.onConnected);
            IO.socket.on('redirect', IO.onRedirect);
            IO.socket.on('lobbyNotFound', App.lobbyNotFound);
            IO.socket.on('sError', App.onError);
            IO.socket.on('sForceJoinQuestion', App.onForceJoinQuestion)
        },

        onConnected : function() {
            IO.socket.emit('homeSocket');
        },

        onRedirect : function(url) {
            window.location = url;
        },

        onError: function(message) {
            console.log(message);
        },
    };

    var App = {
        init: function() {
            App.$doc = $(document);
            App.bindEvents();
        },

        bindEvents : function() {
            App.$doc.on('click', '#btnCreateLobby', function() { App.onCreateLobby(false); } );
            App.$doc.on('click', '#btnJoinLobby', function() { App.onJoinLobby(false); } );
            App.$doc.on('keypress', function(event) {
                if(event.which == 13) {
                    event.preventDefault();
                    App.onJoinLobby();
                }
            });
        },

        onCreateLobby : function() {
            IO.socket.emit('createLobby');
        },

        onJoinLobby : function(override) {
            if (override === undefined)
                override = false;

            var lobby = $("#inputLobby").val();
            IO.socket.emit('joinLobby', lobby, override);
        },

        onError : function(msg) {
            $('#errorBox').empty();
            $('#errorBox').append(App.makeAlert(msg));
        },

        onForceJoinQuestion : function(msg) {
            $('#errorBox').empty();
            $('#errorBox').append(App.makeAlert(msg + ' <a href="#" onClick=App.onJoinLobby(true)>Discard and join anyway.</a>'));
        },

        makeAlert : function(msg) {
            return '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Hold up!</strong> ' + msg + '</div>'
        },
    };
jQuery(function($){ 
    IO.init();
    App.init();
}($));