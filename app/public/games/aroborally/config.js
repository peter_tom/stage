var config = {
	name : 'Robo Rally',
	description : 'Race tegen de andere robots.',
	minPlayers : 0,
	maxPlayers : 8,
	options : [ 
		{
			settings : {
				name : "map",
				type : "dropdown",
				changesMaxPlayers : true,
			},
			choices : [
				{ value : "Exchange", nPlayers : 8},
           		{ value : "Whirlwind", nPlayers : 8},
           		{ value : "Megatron", nPlayers : 32},
           		{ value : "MazeMap", nPlayers : 8},
           		{ value : "Roundabout", nPlayers : 8},
           		{ value : "Arena", nPlayers : 16},
			],
        }, 
        {
			settings : {
				name : "bots",
				type : "checkbox",
				default : false,
			},
        },
	],
};

module.exports = config;