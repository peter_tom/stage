App = {
    init: function() {
        window.scrollTo(0, 0);

        App.canvas = $("#canvas")[0];
        App.stage = new createjs.Stage("canvas");
        this.width = window.innerWidth || document.body.clientWidth; 
        this.height = window.innerHeight || document.body.clientHeight; 
        var canvas = App.canvas;
        var stage = App.stage;
        var width = this.width;
        var height = this.height;

        App.ow = width;
        App.oh = height;

        App.topcontwidth = 1400;
        App.topcontheight = 103;
        App.cardcontwidth = 1261;
        App.cardcontheight = 516;

        App.health = 10;
        App.lives = 3;
        App.lockedcards = [];  
        App.powerDownState = 1;

        canvas.width = width;
        canvas.height = height;

        window.addEventListener('resize', function(){
            App.resize();
        });
        window.onorientationchange = App.resize;

        App.initComponents(width, height, stage);

        cLib.init();
        App.bindEvents();
    },

    bindEvents : function(){
        cLib.on('sendCards', App.loaddata);
        cLib.on('takeDamage', App.takeDamage);
        cLib.on('repair', App.repair);
        cLib.on('poweredDown', App.handlePoweredDown);
        cLib.on('destroy', App.handleDestroy);
        cLib.on('giveLockedCard', App.handleGiveLockedCard);
        cLib.on('powerDownCheck', App.createYesNo);
    },

    resize : function() {
        width = window.innerWidth || document.body.clientWidth; 
        height = window.innerHeight || document.body.clientHeight; 
        this.width = width;
        this.height = height;

        App.canvas.width = width;
        App.canvas.height = height;

        var container = App.container;
        App.container.scaleX = width/App.ow;
        App.container.scaleY = height/App.oh;

        App.stage.update();
    },

    loaddata : function(data){
        App.drawPoweredDownState(App.powerDownState);
        App.updateLivesText();
        App.updateHealthText();

        App.cards = data;
        App.data = data;
        App.selectedCards = [undefined, undefined, undefined, undefined, undefined];

        App.drawCards();
        App.addLockedCards();

        App.stage.update();
    },

    addReadyButton : function(){
        width = window.innerWidth || document.body.clientWidth; 
        height = window.innerHeight || document.body.clientHeight; 
        width = App.cardcontwidth;
        height = App.cardcontheight;

        App.readybuttontext = new createjs.Text("READY", "25px Arial", "#000000");
        App.readybuttontext.x = width/2-App.readybuttontext.getMeasuredWidth();
        App.readybuttontext.y = (height)*0.90;
        App.readybuttontext.on("click", App.handleReady);

        var textheight = App.readybuttontext.getMeasuredHeight();

        var readybutton = new createjs.Shape();
        readybutton.graphics.beginFill("#FF0000").drawRect(0, 0, App.readybuttontext.getMeasuredWidth(), textheight );
        App.readybuttontext.hitArea = readybutton;
        App.readybuttontext.ready = false;
        App.readybuttontext.color = "#FFFFFF";

        App.buttonbg = new createjs.Shape();
        var buttonbg = App.buttonbg;
        buttonbg.graphics.beginFill("#FF0000").drawRect(0, 0, App.cardcontwidth * 0.90, textheight*2);
        App.buttonbg.x = App.cardcontwidth*0.05;
        App.buttonbg.y = App.readybuttontext.y - textheight/2;
        App.buttonbg.on("click", App.handleReady);

        App.cardcontainer.addChild(App.buttonbg);
        App.cardcontainer.addChild(App.readybuttontext);
    },

    roundOver : function () {
        cLib.emit("roundOver", App.selectedCardscards);
    },

    handleCardClick : function(event) {
        if(!App.readybuttontext.ready){
            var target = event.target;
            var height = window.innerHeight || document.body.clientHeight; 
            var indexselected = App.cardcontainer.children.indexOf(target);
            var indexcards = App.cards.indexOf(target.card);
            var card = App.cardcontainer.children[indexselected];
            var xoriginal = card.x;
            var ydiff = target.height * 1.05;
            if(!App.isLocked(card)){
                if(!target.selectedcard){
                if(App.countSelectedCards() != 5){
                    var spot = App.findSpotInSelectedCards();
                    card.x = ((spot*target.width*1.10)+ (App.cardcontwidth*0.02));//1261 omdat ik voorlopig scale van die breedte
                    card.y += ydiff;
                    target.selectedcard = true;
                    delete App.cards[indexcards];
                    App.selectedCards[spot] = target.card;  
                }
                } else {
                    delete App.selectedCards[App.selectedCards.indexOf(target.card)];
                    var spot = App.findSpotInCards();
                    card.x = ((spot *target.width*1.10)+ (App.cardcontwidth*0.02));//1261 omdat ik voorlopig scale van die breedte
                    ydiff = ydiff*-1;
                    card.y += ydiff;
                    target.selectedcard = false;
                    App.cards[spot] = target.card;
                }  
                target.text.x = card.x;
                target.text.y = card.y;

                card.obj.x = card.x;
                card.obj.y = card.y + target.height/3 - 5;
                if(App.countSelectedCards() !== 5){
                    //App.animateCardClickParts(card.obj,card.text, card, ydiff, xoriginal);
                }
                App.stage.update();
            }
        }   
        
    },

    findSpotInCards : function(){
        for(var i = 0; i < App.cards.length; i++){
            if(App.cards[i] === undefined)
                return i;
        }
    },

    findSpotInSelectedCards : function(){
        for(var i = 0; i < 5; i++){
            if(App.selectedCards[i] === undefined)
                return i;
        }
    },

    countSelectedCards : function(){
        var count = 0;
        for(var i = 0; i < App.selectedCards.length; i++){
            if(App.selectedCards[i] !== undefined)
                count++;
        }

        return count;
    },

    handleReady : function(event){
        console.log('ready');
        if(App.countSelectedCards() === 5){
            if(App.readybuttontext.ready !== true){
                App.readybuttontext.ready = true;
                App.readybuttontext.color = "#FFFFFF";

                var g = App.buttonbg.graphics;
                g.clear();
                g.beginFill("green").drawRect(0, 0, App.cardcontwidth*0.90, App.readybuttontext.getMeasuredHeight()*2);

                data = {
                    cards : App.selectedCards,
                    poweredDown : App.powerDownState,
                };
                cLib.emit("ready", data);

                if(App.powerDownState === 2){
                    App.powerDownState++;
                }
            }
            App.stage.update();
        }
    },

    drawCards : function(){
        var width = App.cardcontwidth;
        var height = App.cardcontheight;

        App.cardcontainer.removeAllChildren();

        var cardcount = App.cards.length;
        for(var i = 0; i < cardcount; i++){
          App.drawCard(App.cards[i],i, false);
        }

        App.cardcontainer.scaleX = App.middlecontainer.width/App.cardcontwidth;
        App.cardcontainer.scaleY = App.middlecontainer.height/App.cardcontheight;
        App.addReadyButton();
    },

    drawCard : function(c,spot,selected){
            var width = App.cardcontwidth;
            var height = App.cardcontheight;

            var card = new createjs.Shape();
            card.width = ((width-width*0.07)-(5*width*0.01))/9;
            card.height = height/3.25;

            card.x = ((spot*card.width*1.10)+ (width*0.02));
            card.y = height * 0.10;
            var color = "#117EF2";
            if(selected === true){
                color = "#FF0000";
                card.y += card.height * 1.05;
            }
            card.graphics.beginFill(color).drawRect(0,0, card.width, card.height);
            card.card = c;
            card.on("click", App.handleCardClick);

            var text = new createjs.Text("", "22px Arial", "#FFFFFF");
            switch(c.type) {
                case "MoveCard" : text.text = "     priority\n       " + c.priority;
                    break;
                case "RotateCard" : text.text = "     priority\n       " + c.priority;
                    break;
            }
            text.x = card.x;
            text.y = card.y;
            card.text = text;

            App.cardcontainer.addChild(card);
            App.cardcontainer.addChild(text);
            App.addCardImage(card);
    },

    initComponents : function(width, height, stage){
        App.container = new createjs.Container();
        var container = App.container;

        //container top
        App.topcontainer = new createjs.Container();
        var topcontainer = App.topcontainer;
        topcontainer.width = width;
        topcontainer.height = height*0.20;
        topcontainer.x = 0;
        topcontainer.y = 0;

        //background top
        App.topbg = new createjs.Shape();
        var topbg = App.topbg;
        topbg.width = App.topcontwidth;
        topbg.height = App.topcontheight;
        topbg.graphics.beginFill("#000000").drawRect(0, 0, App.topcontwidth, App.topcontheight);
        topcontainer.addChild(topbg);

        //text components top
        App.livestext = new createjs.Text("Lives: 3", "40px Arial", "#FFFFFF");
        var livestext = App.livestext;
        livestext.x = App.topcontwidth * 0.70;
        livestext.y = App.topcontheight/2;
        topcontainer.addChild(livestext);

        App.healthtext = new createjs.Text("Health: 10", "40px Arial", "#FFFFFF");
        var healthtext = App.healthtext;
        healthtext.x = App.topcontwidth* 0.55;
        healthtext.y = App.topcontheight/2;
        topcontainer.addChild(healthtext);

        App.redbutton = new createjs.Shape();
        var redbutton = App.redbutton;
        redbutton.graphics.beginFill("green").drawRect(0, 0, App.topcontwidth*0.08, App.topcontwidth*0.06);
        redbutton.x = App.topcontwidth * 0.85;
        redbutton.y = App.topcontheight/12;
        App.topcontainer.addChild(redbutton);
        redbutton.on("click", App.handlePowerDownButton);

        App.redbuttontext = new createjs.Text("     Power\n     down\n     next\n     round", "18px Arial", "#FFFFFF");
        var redbuttontext = App.redbuttontext;
        redbuttontext.x = redbutton.x
        redbuttontext.y = redbutton.y;
        topcontainer.addChild(redbuttontext);

        App.healthbarcontainer = new createjs.Container();
        App.drawHealthBars();
        App.topcontainer.addChild(App.healthbarcontainer);

        topcontainer.scaleX = App.topcontainer.width/App.topcontwidth;
        topcontainer.scaleY = App.topcontainer.height/App.topcontheight;

        //container middle
        App.middlecontainer = new createjs.Container();
        var middlecontainer = App.middlecontainer;
        middlecontainer.x = 0;
        middlecontainer.y = topcontainer.height;
        middlecontainer.width = width;
        middlecontainer.height = height*0.80;

        App.midbg = new createjs.Shape();
        var midbg = App.midbg;
        midbg.width = width;
        midbg.height = middlecontainer.height;
        midbg.graphics.beginFill("#999999").drawRect(0, 0, middlecontainer.width, middlecontainer.height);
        middlecontainer.addChild(midbg);

        App.cardcontainer = new createjs.Container();
        App.middlecontainer.addChild(App.cardcontainer);

        App.container.addChild(topcontainer);
        App.container.addChild(middlecontainer);

        stage.addChild(container);
        stage.update();
    },

    addCardImage : function(card){
        var test = card;
        var kaart = card.card;
        if(kaart.type === "RotateCard"){
            switch(kaart.degree) {
                case -1: 
                    card.img = "rotate_left";
                    break;
                case 1:
                    card.img = "rotate_right";
                    break;
                case 2:
                    card.img = "u_turn";
                    break;
            }
        }

        if(kaart.type === "MoveCard"){
            switch(kaart.amount) {
                case 1:
                    card.img = "move_1"; 
                    break;
                case 2:
                    card.img = "move_2";
                    break;
                case 3:
                    card.img = "move_3";
                    break;
                case -1:
                    card.img = "move_backwards";
                    break;
            }
        }
        var img = Resources.getResult(card.img);

        card.obj = new createjs.Bitmap(img);
        card.obj.x = card.x;
        card.obj.y = card.y + card.height/3 - 5;
        card.obj.scaleX = (card.width) / img.width;
        card.obj.scaleY = (card.height*0.66) / img.height;
        // App.animateGiveCard(card.obj);
        // App.animateGiveCard(card.text);
        // App.animateGiveCard(card);
        App.cardcontainer.addChild(card.obj);

    },

    takeDamage : function(data){
        App.health--;
        if(App.health === 0){
            App.health = 10;
            App.lives--;
            App.lockedcards.splice(0, app.lockedcards.length);
        } 

        var lockedlength = App.lockedcards.length;
        if(App.health <= 5 && App.countSelectedCards() > 0) {
            var card = App.selectedCards[4-lockedlength];
            App.lockedcards.push(card);
            App.lockSelectedCard(card);
        }

        App.updateLivesText();
        App.updateHealthText();
        App.drawHealthBars();
    },

    repair : function(data) {
        App.health += data;
        App.updateHealthText();
        App.drawHealthBars();
        if(App.health <= 6)
            App.lockedcards.splice(App.lockedcards.length - 1, 1);
    },

    updateHealthText : function(){
        App.healthtext.text = "Health: " + App.health;
        App.stage.update();
    },

    updateLivesText : function(){
        App.livestext.text = "Lives: " + App.lives;
        App.stage.update(); 
    },

    addLockedCards : function(){
        for(var i = 0; i < App.lockedcards.length; i++){
            App.selectedCards[4-i] = App.lockedcards[i];
            var spot = 5-App.countSelectedCards();
            App.drawCard(App.lockedcards[i],spot, true);
            App.stage.update();
        }
    },

    handlePowerDownButton: function(){
        if(App.readybuttontext.ready !== true){
            var state = App.powerDownState;
            switch(state){
                case 1: App.powerDownState++;
                        App.drawPoweredDownState(App.powerDownState);
                    break;
                case 2: App.powerDownState = 1;
                        App.drawPoweredDownState(App.powerDownState);
                    break;
            }         
        }
    },

    isLocked : function(card){
        for(var i = 0; i < App.lockedcards.length; i++){
            if(card.card.priority === App.lockedcards[i].priority)
                return true;
        }
        return false;
    },

    drawPoweredDownState : function(state){
        height = App.topcontwidth; 
        width = App.topcontheight; 
        var g = App.redbutton.graphics;
        g.clear();
        switch(state){
            case 1: 
                g.beginFill("green").drawRect(0, 0, height*0.08, height*0.06);
                App.redbuttontext.text = "     Power\n     down\n     next\n     round";
                break;
            case 2: 
                g.beginFill("purple").drawRect(0, 0, height*0.08, height*0.06);
                App.redbuttontext.text = "\n  Powering\n       off";
                break;
            case 3: var g = App.redbutton.graphics;
                g.beginFill("red").drawRect(0, 0, height*0.08, height*0.06);
                App.redbuttontext.text = "\n      Powered\n       off";
                break;
        }
        App.redbuttontext.font = "18px Arial";

        App.stage.update();
    },

    handlePoweredDown : function(){
        App.health = 10;
        App.cardcontainer.removeAllChildren();
        App.lockedcards = [];
        App.selectedCards = [];
        App.drawPoweredDownState(3);
        App.updateHealthText();
        App.drawHealthBars();
        App.stage.update();
    },

    createYesNo : function(){
        var width = App.cardcontwidth;
        var height = App.cardcontheight;

        var yesnotext = new createjs.Text("Would you like to power on?", "30px Arial", "#FFFFFF");
        yesnotext.x = width/2 - yesnotext.getMeasuredWidth()/3;
        yesnotext.y = height/4;
        App.cardcontainer.addChild(yesnotext);

        var yesbutton = new createjs.Shape();
        yesbutton.graphics.beginFill("green").drawRect(0, 0, height*0.17, height*0.17);
        yesbutton.x = width * 0.45;
        yesbutton.y = height/2;
        App.cardcontainer.addChild(yesbutton);
        yesbutton.on("click", App.handleYes);

        var yesbuttontext = new createjs.Text("Yes", "20px Arial", "#FFFFFF");
        yesbuttontext.x = yesbutton.x
        yesbuttontext.y = yesbutton.y;
        App.cardcontainer.addChild(yesbuttontext);

        var nobutton = new createjs.Shape();
        nobutton.graphics.beginFill("red").drawRect(0, 0, height*0.17, height*0.17);
        nobutton.x = width * 0.55;
        nobutton.y = height/2;
        App.cardcontainer.addChild(nobutton);
        nobutton.on("click", App.handleNo);

        var nobuttontext = new createjs.Text("No", "20px Arial", "#FFFFFF");
        nobuttontext.x = nobutton.x
        nobuttontext.y = nobutton.y;
        App.cardcontainer.addChild(nobuttontext);

        App.stage.update();
    },

    handleYes : function(){
        App.cardcontainer.removeAllChildren();
        App.stage.update();
        App.powerDownState = 1;
        cLib.emit("powerDownInfo" , App.powerDownState);
    },

    handleNo : function() {
        App.handlePoweredDown();

        cLib.emit("powerDownInfo" , App.powerDownState)
    },

    handleGiveLockedCard : function(data) {
        App.lockedcards.push(data);
    },

    handleDestroy : function() {
        App.health = 10;
        App.lives--;
        App.updateLivesText();
        App.updateHealthText();
        App.drawHealthBars();
        App.powerDownState = 1;
        App.drawPoweredDownState();

        App.lockedcards = [];
        App.addDeathText();
    },

    addDeathText : function(){
        App.cardcontainer.removeAllChildren();
        var width = App.cardcontwidth;
        var height = App.cardcontheight;
        var text = "You died! You will respawn next round";
        if(App.lives === 0){
            text = "You are out of lives!";
        } 
        var deathtext = new createjs.Text(text, "30px Arial", "#000000");
        deathtext.x = width/2 - deathtext.getMeasuredWidth()/3;
        deathtext.y = height/4;
        App.cardcontainer.addChild(deathtext);
        App.stage.update();
    },

    lockSelectedCard : function(card){
        for(var i = 0; i < App.cardcontainer.children.length;i++){
            if(App.cardcontainer.children[i].card !== undefined){
                if(App.cardcontainer.children[i].card === card){
                    var x = App.cardcontainer.children[i];
                    var width = x.width;
                    var height = x.height;
                    var g = x.graphics;
                    g.clear;
   
                    g.beginFill("#FF0000").drawRect(0,0, width, height);
                    App.stage.update();
                    return;
                }
            }
        }
    },

    drawHealthBars : function(){
        App.healthbarcontainer.removeAllChildren();
        var red = 10-App.health;
        var topW = App.topcontwidth;
        var topH = App.topcontheight;
        for(var i = 0; i < App.health; i++){
            var healthbar = new createjs.Shape();
            healthbar.graphics.beginFill("green").drawRect(0, 0, topW*0.04, topW*0.06);
            healthbar.x = topW * 0.02 + (topW * 0.05) * i;
            healthbar.y = topH/12;
            App.healthbarcontainer.addChild(healthbar);
        }

        for(var r = App.health; r < 10; r++){
            var healthbar = new createjs.Shape();
            healthbar.graphics.beginFill("red").drawRect(0, 0, topW*0.04, topW*0.06);
            healthbar.x = topW * 0.02 + (topW * 0.05) * r;
            healthbar.y = topH/12;
            App.healthbarcontainer.addChild(healthbar);
        }
        App.stage.update();
    },

    animateGiveCard : function(obj){
        var x = obj.x;
        var y = obj.y;
        obj.x = 0;
        obj.y = 0;
        createjs.Tween.get(obj).to({x:x,y:y},500);
        //obj.x = x;
        //obj.y = y;
    },

    animateCardClick : function (obj, ydiff, xoriginal){
        var x = obj.x;
        var y = obj.y;
        obj.x = xoriginal;
        obj.y = obj.y-ydiff;
        createjs.Tween.get(obj).to({x:x,y:y},140);
    },

    animateCardClickParts : function(obj,text,card, ydiff , xoriginal){
        App.animateCardClick(obj, ydiff, xoriginal);
        App.animateCardClick(text, ydiff, xoriginal);
        App.animateCardClick(card, ydiff, xoriginal);
    },
};