Game = {
    maps : [],
    players : new Array(),
    EXECUTE_DELAY : 750,
    ANIMATION_TIME : 500,
    BOTS : false,

    init : function() {
        window.scrollTo(0, 0);
    
        this.canvas = $("#canvas")[0];
        this.stage = new createjs.Stage("canvas");
        
        this.width = window.innerWidth || document.body.clientWidth; 
        this.height = window.innerHeight || document.body.clientHeight; 

        this.canvas.width = this.width;
        this.canvas.height = this.height;

        this.status.init();
        this.stage.update();

        window.addEventListener('resize', function()
        {
            Game.resize();
        });

        window.onorientationchange = Game.resize;

        sLib.init();
        //Communication with controllers
        Game.bindEvents();
        if (sLib.initialized) {
            Game.loadParameters();
            if (sLib.playersLoaded)
                Game.initPlayers();
        }
        createjs.Ticker.addEventListener("tick", this.stage);
    },

    bindEvents : function() {
        sLib.on('ready', this.round.playerReady);
        sLib.on('powerDownInfo', this.round.playerSendsPowerDownInfo);
        sLib.on('playerDisconnected', this.playerDisconnected);
        sLib.on('allPlayersLoaded', this.initPlayers);
        sLib.on('initialized', this.loadParameters);
    },

    loadParameters : function() {
        var map = "f";
        for (var i = 0; i < sLib.parameters.length; i++) {
            var p = sLib.parameters[i];

            if (p.name === 'map')
                map = p.value;
            if (p.name === 'bots')
                Game.BOTS = p.value;
        }

        Game.board.map = Game.maps[map];
        Game.board.init();
        Game.status.init();
    },

    resize : function() {
        this.width = window.innerWidth || document.body.clientWidth; 
        this.height = window.innerHeight || document.body.clientHeight; 

        this.canvas.width = this.width;
        this.canvas.height = this.height;
        
        Game.board.resize();
        Game.status.resize();

        this.players.forEach(function(ply) {
            ply.resize();
        });

        Game.stage.update();
    },

    initPlayers : function() {
        var nHumans = sLib.players.length;
        for (var i = 0; i < nHumans; i++) {
            var point = Game.board.startPoints[i + 1];
            var ply = sLib.players[i];

            var player = new Player(ply.name, ply.color, point.direction);
            player.checkpoint.x = player.x = point.x;
            player.checkpoint.y = player.y = point.y;
            player.checkpoint.direction = point.direction;
            
            player.recalculatePosition();

            Game.players.push(player);
        }

        var nBots = Game.board.startPoints.length - 1 - nHumans;

        if (Game.BOTS) {
            function uniqueName(name) {
                for (var i = 0; i < sLib.players.length; i++) {
                    if (sLib.players[i].name === name)
                        return false;
                }
                return true;
            }
            var skipped = 0;
            for (var i = 0; i < nBots; i++) {
                console.log(i);
                var name = 'Tom ' + i;
                if (!uniqueName(name)) {
                    //Not unqiue, try again with the next index.
                    nBots++;
                    skipped++;
                    continue;
                }
                var color = {
                    r : Math.floor(Math.random() * 255),
                    g : Math.floor(Math.random() * 255),
                    b : Math.floor(Math.random() * 255),
                };
                var point = Game.board.startPoints[nHumans + i - skipped + 1];

                var player = new Player(name, color, point.direction);
                player.checkpoint.x = player.x = point.x;
                player.checkpoint.y = player.y = point.y;
                player.checkpoint.direction = point.direction;
                player.bot = true;
                
                player.recalculatePosition();

                Game.players.push(player);
            }
        }

        Game.programDeck = new ProgamCardDeck(Game.players.length);
        console.log('init Cards: ' + Game.programDeck.cards.length);
        Game.start();
        Game.status.initScoreboard();
        Game.stage.update();
        Game.round.checkPowerDownState();
    },
    playerDisconnected : function(name) {
        console.log(name + ' disconnected');
        var deleted = false;

        for (var i = 0; i < Game.players.length && !deleted; i++) {
            var ply = Game.players[i];

            if (ply.name === name) {
                deleted = true;
                
                if (Game.round.phase === 0) {
                    if (!ply.doneSendingPowerDownState) {
                        Game.round.playersPoweredDown--;
                        if (Game.round.playersPoweredDown === 0) {
                            Game.round.start();
                        }
                    } else if (!ply.doneProgramming) {
                        Game.round.playersReady++;
                        if (Game.round.playersProgramming === Game.round.playersReady)
                            Game.round.execute();
                    } 
                    ply.connected = false;
                    ply.disconnecting = false;
                    Game.board.removePlayer(ply);
                } else {
                    ply.disconnecting = true;
                }

                Game.status.updatePlayer(name);
            }
        }
    },

    start : function() {
        Game.players.forEach(function(ply) {
            Game.board.addPlayer(ply);
        });
    },

    playCard : function(ply, card) {
        if (card instanceof MoveCard) {
            Game.status.logEvent(ply.name + " moved " + card.amount);
            var currMove = 0;
            var blocked = false;
            var moveDirection = (card.amount > 0) ? 1 : -1;
            var dir = (moveDirection === 1) ? ply.direction : invertDirection(ply.direction);
            //Move per move
            while (currMove != card.amount && !blocked && !ply.destroyed) {
                //Wall on current tile
                blocked = !Game.movePlayer(ply, dir);
                currMove += moveDirection;

                Game.animation.nextFrame();
            }
        } else if (card instanceof RotateCard) {
            Game.rotatePlayer(ply, card.degree)
        }

        var args = Array.prototype.slice.call(arguments, 2);
        Game.animation.animate.apply(Game.animation.animatie, args); //Animate moves
    },

    rotatePlayer : function(ply, degree) {
        var dir = "";
        switch (degree) {
            case -1: dir = "left";
                break;
            case 1: dir = "right";
                break;
            case 2: dir = "180";
                break;
        }
        Game.status.logEvent(ply.name + " rotated " + dir);
        Game.animation.addObjToFrame(ply.obj); //Start point

        if (degree === 2) {
            ply.rotate(1);
            Game.animation.nextFrame();
            Game.animation.addObjToFrame(ply.obj); //Start point
            ply.rotate(1);
        } else {
            ply.rotate(degree);
        }
    },

    movePlayer : function(ply, dir) {
        var moved = false;

        if (Game.canMove(ply, dir)) {
            var blocked = false;

            var pos = getNextSquare(ply.x, ply.y, dir);
            var robot = Game.findRoboto(pos.x, pos.y);
            var robots = [];

            //Find all robots we are pushing
            while(robot !== undefined && !blocked) {
                if (!Game.canMove(robot, dir))
                    blocked = true;
                else {
                    robots.push(robot);
                    pos = getNextSquare(robot.x, robot.y, dir);
                    robot = Game.findRoboto(pos.x, pos.y); 
                }
            }

            if (!blocked) {
                moved = true;

                function pitCheck(robot) {
                    if (Game.board.tiles[robot.x][robot.y] instanceof Pit) {
                        Game.status.logEvent("Uh oh, " + robot.name + " just picked up a bouquet of woopsy daisies");
                        Game.destroyPlayer(robot);
                    }
                }

                Game.animation.addObjToFrame(ply.obj); //Start point
                ply.push(dir);
                
                for(var i = 0; i < robots.length; i++) {
                    var robot = robots[i];

                    Game.animation.addObjToFrame(robot.obj); //Start point
                    robot.push(dir);

                    pitCheck(robot);
                }

                pitCheck(ply);
            }
        }
        return moved;
    },

    laserIndex : 0,
    laserRobots : [],
    shootLasers : function() {
        //Init variables.
        if (Game.laserIndex === 0) {
            Game.laserRobots = Game.getLivingPlayers();
        }

        if (Game.laserIndex >= Game.laserRobots.length) {
            Game.laserIndex = 0;
            Game.round.execute();
        } else {
            var ply = Game.laserRobots[Game.laserIndex];
            Game.laserIndex++;

            if (ply.powerDownState !== PowerDownState.PoweredDown)    
                Game.shootLaser(ply.x, ply.y, ply.direction, ply, ply.colour, Game.shootLasers); //try to shoot web
            else
                Game.shootLasers(); //Continue
        }
    },

    shootLaser : function(x, y, dir, ply, colour, callback) {
        if (Game.board.tiles[x][y].hasWall(dir)) {
            callback();
            return;
        }
        var pos = getNextSquare(x, y, dir);
        var found = Game.findRobotoInLine(pos, dir);

        if (found) {
            //Animation
            var coolColour = createjs.Graphics.getRGB(colour.r, colour.g, colour.b);
            Game.drawLaser(x, y, found.x, found.y, coolColour, callback);
            found.getHit(ply.weapon);
            Game.status.logEvent(ply.name + " shot " + found.name);
        } else {
            callback();
        }
    },

    laserWallIndex : 0,
    laserTileR : 0,
    laserTileC : 0,

    shootLaserWalls : function() {
        if (Game.laserTileC === Game.board.y) {
            if (Game.laserTileR === Game.board.x - 1) {
                Game.laserTileC = Game.laserTileR = 0;
                Game.round.execute();
            } else {
                //Next row
                Game.laserTileC = 0;
                Game.laserTileR++;
                Game.shootLaserWalls(); //Find better code for this?
            }
        } else {
            var tile = Game.board.tiles[Game.laserTileR][Game.laserTileC];

            if (Game.laserWallIndex === tile.walls.length) {
                Game.laserTileC++;
                Game.laserWallIndex = 0;
                Game.shootLaserWalls();
            } else {
                var wall = tile.walls[Game.laserWallIndex];
                Game.laserWallIndex++;
                //if the wall has lasers, shoot them.
                if (wall.lasercount > 0){
                    Game.shootLaserWall(tile.x, tile.y, invertDirection(wall.direction), wall.lasercount, Game.shootLaserWalls);
                } else {
                    Game.shootLaserWalls();
                }
            }
        }
    },

    shootLaserWall : function(x, y, dir, dmg, callback) {
        var found = this.findRoboto(x, y); //Robot on current tile.

        if (found === undefined) {
            if (Game.board.tiles[x][y].hasWall(dir)) {
                callback();
                return;
            }
            var pos = getNextSquare(x, y, dir);
            found = Game.findRobotoInLine(pos, dir);
        }
        if (found) {
            //Animation
            Game.drawLaser(x, y, found.x, found.y, "#FF0000", callback);
            Game.status.logEvent("Wall shot " + found.name + " for " + dmg);
            found.getHit(dmg);
        } else {
            callback();
        } 
    },

    drawLaser : function(ax, ay, bx, by, colour, callback) {
        Game.shooting = true;

        var tileInfo = Game.board.tileInfo;

        //We need to draw a rectangle, get the left most x and y from
        //and use the others to calculate the width.
        var x = (ax < bx) ? ax : bx;
        var y = (ay < by) ? ay : by;
        var width = (ax < bx) ? bx - ax : ax - bx;
        var height = (ay < by) ? by - ay : ay - by;

        //Convert position to a position we can draw.
        x = tileInfo.margin.x + x * tileInfo.width + tileInfo.width / 2;
        y = tileInfo.margin.y + y * tileInfo.height + tileInfo.height / 2;
        width = width * tileInfo.width + tileInfo.width / 8;
        height = height * tileInfo.height + tileInfo.height / 8;

        var laser = new createjs.Shape();
        laser.x = x;
        laser.y = y;
        laser.graphics.beginFill(colour).drawRect(0, 0, width, height);
        
        Game.board.container.addChild(laser);
        Game.stage.update();

        //Call callback after we removed the laser we drew.
        setTimeout(function(laser, callback) {
            Game.board.container.removeChild(laser); 
            Game.stage.update();
            callback();
        }, Game.EXECUTE_DELAY, laser, callback);
    },

    canMove : function(robot, dir) {
        var blocked = false;

        //Check if our tile has no blocking wall
        if (Game.board.tiles[robot.x][robot.y].hasWall(dir)) {
            blocked = true;
        } else  {
            //Check the next tile
            var pos = getNextSquare(robot.x, robot.y, dir);

            //Edge
            if (pos.x < 0 || pos.y < 0 || pos.x >= Game.board.x || pos.y >= Game.board.y) {
                blocked = true;
            } else {
                var invertedDir = invertDirection(dir);

                //Has blocking wall?
                if (Game.board.tiles[pos.x][pos.y].hasWall(invertedDir)) {
                    blocked = true;
                }
            }
        }

        return !blocked;
    },

    findRoboto : function(x, y) {
        var plys = Game.getLivingPlayers();

        for (var i = 0; i < plys.length; i++) {
            var otherRobot = plys[i];

            if (otherRobot.x === x && otherRobot.y === y) {
                return otherRobot;    
            }
        }
        return undefined;
    },

    findRobotoInLine : function(pos, dir) {
        var found = undefined;

        while (found === undefined) {
            if (pos.x < 0 || pos.y < 0 || pos.x >= Game.board.x || pos.y >= Game.board.y) {
                found = false;
            } else {
                var tile = Game.board.tiles[pos.x][pos.y];

                if (tile.hasWall(invertDirection(dir)))
                    found = false;
                else {
                    found = this.findRoboto(pos.x, pos.y);

                    if (found === undefined && tile.hasWall(dir))
                        found = false;
                } 
            }
            pos = getNextSquare(pos.x, pos.y, dir);
        }
        return found;
    },

    findPlayer : function(name) {
        var plys = Game.players;
        for (var i = 0; i < plys.length; i++) {
            var ply = plys[i];
            if (ply.name === name)
                return ply;
        }
        return undefined;
    },

    destroyPlayer : function(ply) {
        ply.destroy();
        if (!ply.bot)
            sLib.emit(ply.name, 'destroy');
        Game.board.removePlayer(ply);
        Game.status.updatePlayer(ply.name);
        Game.status.logEvent(ply.name + ' just got wrecked!');
    },

    respawnPlayer : function(ply) {
        ply.moveToCheckPoint();
        ply.destroyed = false;
        ply.damage = 0;
        Game.status.updatePlayer(ply.name);
        Game.board.addPlayer(ply);
    },

    checkWin : function() {
        var plys = Game.getPlayingPlayers();

        if (plys.length <= 1) {
            Game.leaderboard.show();
            return true;
        }
        return false;
    },

    hasPlayerFinished : function(ply) {
        if (ply.currentFlag === Game.board.flags) {
            var maxPos = 0;
            var plys = Game.players;

            for (var i = 0; i < plys.length; i++) {
                var pos = plys[i].finishPosition;
                if (pos > maxPos)
                    maxPos = pos;
            }

            ply.finishPosition = maxPos + 1;
            ply.destroyed = true; //A small hack so we don't show up in the living players function.
            Game.board.removePlayer(ply);

            Game.status.logEvent(ply.name + ' finished the race with position ' + ply.finishPosition);
            Game.status.displayBigText(ply.name + ' finished #' + ply.finishPosition);
        }
    },

    getLivingPlayers : function() {
        var plys = [];
        for (var i = 0; i < Game.players.length; i++) {
            var ply = Game.players[i];
            if (!ply.destroyed && ply.connected)  {
                plys.push(ply);
            }
        }
        return plys;
    },

    getPlayingPlayers : function() {
        var plys = [];
        for (var i = 0; i < Game.players.length; i++) {
            var ply = Game.players[i];
            //Finished players don't get to play
            if (ply.lives > 0 && ply.finishPosition === -1 && ply.connected) 
                plys.push(ply);
        }
        return plys;
    },

    getPlayersSortedByPosition : function(){
        var players = Game.players;

        players.sort(function(a, b) {
            if(a.finishPosition === -1 && b.finishPosition === -1) {
                return -1;
            } else {
                if (a.finishPosition < b.finishPosition) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });

        return players;
    },
};