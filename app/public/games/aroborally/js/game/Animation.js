var Game = Game || {};

Game.animation = {
	frames : [[]],
	endPoints : [],
	beginPoints : [],
	nFrame : 0,

	addObjToFrame : function(obj) {
		//Save begin point, this is the begin point of the whole frame.
		var found = false;
		for (var k = 0; k < this.beginPoints.length && !found; k++) {
			var beginPoint = this.beginPoints[k];
			if (beginPoint.obj === obj) {
				found = true;
			}
		}

		if (!found) {
			this.beginPoints.push({obj : obj, x : obj.x, y : obj.y, rotation : obj.rotation});
		}

		this.frames[this.nFrame].push({obj : obj, x : obj.x, y : obj.y, rotation : obj.rotation});
	},

	animate : function(callback) {
		Game.animation.nFrame = 0;
		Game.animation.callback = callback;
		Game.animation.args = Array.prototype.slice.call(arguments, 1);

		Game.animation.animateFrames();
	},

	animateFrames : function() {
		if (Game.animation.nFrame === 0) {
			//Move all objects back to the start of the animation
			//so they don't start at the endposition of they don't animate
			//in the first frame.
			//This is a bit hacky, but #yolo.

			//Add end points, shit is ugly fck it

			//Frames
			for (var i = 0; i < Game.animation.frames.length; i++) {
				var frame = Game.animation.frames[i];

				//Actions in frame
				for (var j = 0; j < frame.length; j++) {
					var info = frame[j];
					var obj = info.obj;

					//find if endPoint already exists.
					var found = false;
					for (var k = 0; k < Game.animation.endPoints.length && !found; k++) {
						var endPoint = Game.animation.endPoints[k];

						found = endPoint.obj === info.obj;
					}

					if (!found) {
						Game.animation.endPoints.push({obj : obj, x : obj.x, y : obj.y, rotation : obj.rotation});
					}
				}
			}
			for(var i = 0; i  < Game.animation.beginPoints.length; i++) {
				var point = Game.animation.beginPoints[i];
				var obj = point.obj;

				obj.x = point.x;
				obj.y = point.y;
				obj.rotation = point.rotation;
				Game.stage.update();
			}
		}
		if (Game.animation.nFrame < Game.animation.frames.length) {
			Game.animation.animateFrame();
			Game.animation.nFrame++;
		} else {
			Game.animation.reset();

			if (typeof Game.animation.callback === "function") {
				Game.animation.callback.apply(Game.animation.callback, Game.animation.args);
			}
		}
	},

	animateFrame : function() {
		var frame = this.frames[this.nFrame];

		for (var j = 0 ; j < frame.length; j++) { //Loop through all the moves for this frame.
			var info = frame[j];
			var obj = info.obj;
			
			//Determine end point for this animation
			var end = {}; 
			if (this.nFrame === this.frames.length - 1) {
				//End of frames, take endpoint we saved
				for (var k = 0; k < this.endPoints.length; k++) {
					var endPoint = this.endPoints[k];

					if (endPoint.obj === obj) {
						end.x = endPoint.x;
						end.y = endPoint.y;
						end.rotation = endPoint.rotation;
					}
				}
			} else { 
				//Loop through nextFrame to see if we have a frame
				var nextFrame = this.frames[this.nFrame + 1]; 
				for (var k = 0; k < nextFrame.length; k++) {
					var nextFrameInfo = nextFrame[k];

					if (nextFrameInfo.obj === obj) {
						//Found robot in the next frame.
						end.x = nextFrameInfo.x;
						end.y = nextFrameInfo.y;
						end.rotation = nextFrameInfo.rotation;
					}
				}

				if (end.x === undefined) {
					//We didn't find the info in the next frame, take the endpoint we saved

					for (var k = 0; k < this.endPoints.length; k++) {
						var endPoint = this.endPoints[k];

						if (endPoint.obj === obj) {
							end.x = endPoint.x;
							end.y = endPoint.y;
							end.rotation = endPoint.rotation;
						}
					}
				}
			}

			obj.x = info.x;
			obj.y = info.y;
			createjs.Tween.get(obj).to({x : end.x, y : end.y, rotation : end.rotation}, Game.ANIMATION_TIME); 	
		}
		setTimeout(this.animateFrames, Game.ANIMATION_TIME * 1.05);
	},

	nextFrame : function() {
		this.nFrame++;
		this.frames.push([]);
	},

	reset : function() {
		this.frames = [[]];
		this.endPoints = [];
		this.beginPoints = [];
		this.nFrame = 0;
	},
};