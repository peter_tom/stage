var Game = Game || {};

Game.board = {
    x : 12,
    y : 12,
    tiles : undefined,
    loaded : false,
    tileInfo : {},
    startPoints : [],
    flags : 0,
    
    init : function() {
        //Game container;
        this.container = new createjs.Container();
        Game.stage.addChild(this.container);

        var bgColor = new createjs.Shape();
        bgColor.graphics.beginFill("#000000").drawRect(0, 0, Game.width, Game.height);
        this.container.addChild(bgColor);
        this.bgColor = bgColor;

        //Data van map
        this.loaded = true;
        var map = this.map;
        this.x = map.width;
        this.y = map.height;

        this.tiles = createArray(this.x, this.y);

        //Map info
        for (var i = 0; i < this.y; i++) {
            for (var j = 0; j < this.x; j++) {
                //map format is order differently for eas of coding, switch i and j.
                var tile = map.board[i][j];
                var walls = [];
                var newTile;
                if (typeof tile.walls !== "undefined")
                    for (var k = 0; k < tile.walls.length; k++) {
                        var wall = tile.walls[k];
                        walls.push(new Wall(wall.direction, wall.lasercount));
                    }

                switch(tile.id) {
                    case 1: newTile = new Floor(j, i, walls);
                        break;
                    case 2: newTile = new Pit(j, i, walls);
                        break;
                    case 3: newTile = new ConveyorBelt(j, i, walls, tile.direction, tile.rotation, tile.express);
                        break;
                    case 4: newTile = new Repair(j, i, walls);
                        break;
                    case 5: if (tile.flag > this.flags) 
                                this.flags = tile.flag;
                            newTile = new Flag(j, i, walls, tile.flag);
                        break;
                    case 6: newTile = new Rotator(j, i, walls, tile.direction);
                        break;
                    case 7: newTile = new StartPoint(j, i, walls, tile.pointId, tile.direction);
                            Game.board.startPoints[tile.pointId] = newTile;
                        break;
                }

                this.tiles[j][i] = newTile;

                this.container.addChild(newTile.obj);
                newTile.walls.forEach(function(wall) {
                    Game.board.container.addChild(wall.obj);
                });
            }
        }

        this.resize();        
    },

    resize : function() {
        if (!this.loaded)
            return;
        this.width = (Game.width / 3) * 2;
        this.height = Game.height;

        //Update tileInfo
        this.updateTileInfo();

        //Update all tiles with new info
        this.tiles.forEach(function(row) {
            row.forEach(function(tile) {
                tile.resize();    
            });
        });

        //Update background color
        var g = new createjs.Graphics();
        g.beginFill("#000000").drawRect(0, 0, Game.width, Game.height);
        this.bgColor.graphics = g;
    },

    updateTileInfo : function() {
        var tileInfo = this.tileInfo || {}; 
        tileInfo.margin = {};
        
        tileInfo.height = this.height / this.y;
        tileInfo.width = this.width / this.x;

        if (tileInfo.width > tileInfo.height) {    
            tileInfo.width = tileInfo.height;
            tileInfo.margin.x = (this.width - (tileInfo.width * this.x)) / 2;
            tileInfo.margin.y = 0;
        } else {
            tileInfo.height = tileInfo.width;
            tileInfo.margin.x = 0;
            tileInfo.margin.y = (this.height - (tileInfo.height * this.y)) / 2;
        }

        tileInfo.width = Math.floor(tileInfo.width);
        tileInfo.height = Math.floor(tileInfo.height);

        this.tileInfo = tileInfo;
    },

    addPlayer : function(player) {
        player.resize();
        this.container.addChild(player.obj);
        Game.stage.update();
    },

    removePlayer : function(ply) {
        this.container.removeChild(ply.obj);
        Game.stage.update();
    },
};