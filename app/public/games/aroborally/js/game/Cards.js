function ProgramCard(priority) {
    this.priority = priority;
}

ProgramCard.prototype.equals = function(card) {
    return this.priority === card.priority;
};

function MoveCard(priority, amount) {
    ProgramCard.call(this, priority);

    this.amount = amount;
    this.type = "MoveCard";
}
MoveCard.prototype = new ProgramCard;

function RotateCard(priority, degree) {
    ProgramCard.call(this, priority);

    this.degree = degree;
    this.type = "RotateCard";
}
RotateCard.prototype = new ProgramCard;

function CardDeck() {
    this.cards = new Array();
    this.pulledCards = new Array();
}

CardDeck.prototype.pullCard = function() {
    if (this.cards.length == 0)
        throw "No cards left.";

    var i = Math.ceil(Math.random() * this.cards.length - 1);
    var card = this.cards[i];

    this.cards.splice(i, 1);

    return card;
};

//Return all cards from deck that are not in deck2
CardDeck.prototype.returnCards = function(deck, deck2) {
    // console.log('Returning cards');
    // console.log(deck);
    // console.log(deck2);
    for (var i = 0; i < deck.length; i++) {
        var card1 = deck[i];
        var found = false;

        for (var j = 0; j < deck2.length && !found; j++) {
            var card2 = deck2[j];

            if (card2.priority == card1.priority) {
                found = true;
            }
        }

        if (!found)
            this.returnCard(card1);
    }
};

CardDeck.prototype.returnCard = function(card) {
    if (card instanceof ProgramCard) {
        this.cards.push(card);
    }
};

//WARNING: Broken and used for now.
CardDeck.prototype.reset = function() {
    var plys = Game.players;
    for (var i = 0; i < plys.length; i++) {

    }

    this.pulledCards = new Array();
};

function ProgamCardDeck(nPlys) {
    CardDeck.call(this);

    //Since we offer the possibility of more then 8
    //players we have to increase the deck accordingly
    //The deck will be doubled for every 8 players.
    var multiplier = 1 + Math.floor((nPlys - 1) / 8);
    var adjuster = 10 * (multiplier - 1);

    var cat6 = 6 * multiplier;
    var cat12 = 12 * multiplier;
    var cat18 = 18 * multiplier;

    //6 U turns
    for (var i = 1; i <= cat6; i++)
        this.cards.push(new RotateCard(i * 10, 2));

    //18 Rotate left
    for (var i = 0; i < cat18; i++)
        this.cards.push(new RotateCard((70 * multiplier - adjuster) + i * 20, -1));

    //18 Rotate right
    for (var i = 0; i < cat18; i++)
        this.cards.push(new RotateCard((70 * multiplier - adjuster + 10) + i * 20, 1));

    //6 Backwards
    for (var i = 0; i < cat6; i++)
        this.cards.push(new MoveCard((430 * multiplier - adjuster) + i * 10, -1));

    //18 Move 1
    for (var i = 0; i < cat18; i++)
        this.cards.push(new MoveCard((490 * multiplier - adjuster) + i * 10, 1));

    //12 Move 2
    for (var i = 0; i < cat12; i++)
        this.cards.push(new MoveCard((670 * multiplier - adjuster) + i * 10, 2));

    //6 Move 3
    for (var i = 0; i < cat6; i++)
        this.cards.push(new MoveCard((790 * multiplier - adjuster) + i * 10, 3));

}
ProgamCardDeck.prototype = new CardDeck;