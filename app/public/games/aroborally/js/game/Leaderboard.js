var Game = Game || {};

Game.leaderboard = {

    show : function(){
        Game.stage.removeAllChildren();
        this.container = new createjs.Container();

        var width = 800;
        var height = 600;

        var bg = new createjs.Shape();
        bg.width = width;
        bg.height = height;
        bg.graphics.beginFill("#000000").drawRect(0, 0, width, height);
        this.container.addChild(bg);
    
        var text = new createjs.Text( "Leaderboard", "40px Arial", "#FFFFFF");
        text.x = (width-text.getMeasuredWidth())/2;
        text.y = 0;
        this.container.addChild(text);    
        var players = Game.getPlayersSortedByPosition();
        var textheight = (height*0.70)/16;
        for(var i =0; i < players.length && i < 32; i++){
            var place = (i+1);
            if(players[i].finishPosition === -1){
                place = "DNF";
                if (players[i].lives > 0)
                    place = "Survived"
            }

            var text = new createjs.Text( place +":\t" + players[i].name.substr(0,10), "20px Arial", "#FFFFFF");
            if(i < 16){
                text.x = width / 4;
                text.y = height*0.10 + textheight*i;
            } else {
                text.x = width / 2;
                text.y = height*0.10 + textheight*(i-16);
            }
            
            this.container.addChild(text);
        }
    
        //butons
        var nobuttontext = new createjs.Text("Return to lobby", "50px Arial", "#FFFFFF");
        nobuttontext.x = (width-nobuttontext.getMeasuredWidth())/2;
        nobuttontext.y = height*0.90;
        var marge = 20;
        var nobutton = new createjs.Shape();
        nobutton.graphics.beginFill("green").drawRect(0, 0, nobuttontext.getMeasuredWidth() + marge * 2, nobuttontext.getMeasuredHeight() + marge * 2);
        nobutton.x = nobuttontext.x - marge;
        nobutton.y = nobuttontext.y - marge;
        this.container.addChild(nobutton);
        this.container.addChild(nobuttontext);
        nobutton.on("click", Game.leaderboard.handleReturn);

        this.container.scaleX = Game.width/800;
        this.container.scaleY = Game.height/600;

        Game.stage.addChild(this.container);
        Game.stage.update();
    },

    handleReturn : function(){
        sLib.returnToLobby();
    },
};