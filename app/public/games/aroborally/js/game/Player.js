var Game = Game || {};

function Player(name, colour, direction) {
	this.name = name;
	this.colour = colour;
	this.damage = 0;
	this.lives = 3;
	this.weapon = 1; //TODO make weapon class

	this.programCards = [];
	this.selectedCards = [];
	this.lockedCards = [];

	this.checkpoint = {x : 0, y : 0, direction : 3};
	this.currentFlag = 0;

	this.powerDownState = PowerDownState.NotPoweredDown;

	this.doneProgramming = false;
	this.doneSendingPowerDownState = true;
	this.finishPosition = -1;
	this.connected = true;
	this.disconnecting = false;
	this.bot = false;

	//Board element
	this.x = 0;
	this.y = 0;
	this.xx = 0;
	this.yy = 0;
	//1 = up / 2 = right / 3 = down / 4 = right
	this.img = "robot";

	this.obj = new createjs.Bitmap(Resources.getResult(this.img));

    var Sepia = new createjs.ColorFilter(0.6, 0.6, 0.6, 1, this.colour.r, this.colour.g, this.colour.b, 0);

    this.obj.filters = [Sepia];
    this.obj.cache(0, 0, 64, 64);
    this.spins = 0;
	this.direction = 1;
	if (direction !== 1)
		this.rotate(direction - 1);
}

Player.prototype.moveToCheckPoint = function() {
	this.x = this.checkpoint.x;
	this.y = this.checkpoint.y;
	
	this.direction = this.checkpoint.direction;

	this.recalculateRotation();
	this.recalculatePosition();
};

Player.prototype.getHit = function(dmg) {
	//Protected ourselves from over damage.
	if (this.damage > 9)
		return;	
	//We need to take in account a weapon with more then 1 damage.
	for (var i = 0; i < dmg; i++) {
		this.damage++;
		if (this.damage < 10) {
			if (this.damage > 4) {
				var card;

				if (this.powerDownState === PowerDownState.PoweredDown) {
					card = Game.programDeck.pullCard();
					if (!this.bot)
						sLib.emit(this.name, 'giveLockedCard', card);
				} else {
					//Select cards in reverse order: Register 5 to 1
					var index = 4 - (this.damage - 5);
					card = this.selectedCards[index];
				}

				this.lockedCards.push(card);
			}
			//Notify player of damage / TODO: Is this the correct place for this?
			if (!this.bot)
				sLib.emit(this.name, 'takeDamage');
		} else {
			Game.destroyPlayer(this);
			break;
		}
	}

	Game.status.updatePlayer(this.name);
};

Player.prototype.repair = function(amount) {
	//We need to take in account a repair with more then 1 repair.
	for (var i = 0; i < amount; i++) {
		if (this.damage > 0) {
			this.damage--;
			//If we repaired any damage from 5 or up
			//we have to make sure the cards are still in order.
			if (this.damage >= 4) {
				var index = this.lockedCards.length - 1;
				this.lockedCards.splice(index, 1);
			}
			if (!this.bot)
				sLib.emit(this.name, 'repair', 1);
		}
	}

	Game.status.updatePlayer(this.name);
};

Player.prototype.powerDown = function() {
	this.powerDownState = PowerDownState.PoweredDown;
	this.damage = 0;
	Game.status.updatePlayer(this.name);
};

Player.prototype.move = function(ammount) {
	this.push(this.direction, ammount);
};

Player.prototype.push = function(direction, ammount) {
	ammount = (typeof ammount === "undefined") ? 1 : ammount;
	switch(direction) {
		case 1: this.y -= ammount;
			break;
		case 2: this.x += ammount;
			break;
		case 3: this.y += ammount;
			break;
		case 4: this.x -= ammount;
			break;
	}
	this.recalculatePosition();
};

Player.prototype.destroy = function() {
	this.destroyed = true;
	this.lives--;
	this.powerDownState = PowerDownState.NotPoweredDown;
};

//Visual
Player.prototype.resize = function() {
	var ratio = Game.board.tileInfo.width / Resources.getResult(this.img).width;
	this.obj.scaleX = ratio;
	this.obj.scaleY = ratio;
	this.recalculatePosition();
};

Player.prototype.recalculatePosition = function() {
	var tileInfo = Game.board.tileInfo;
   	this.obj.x = tileInfo.margin.x + tileInfo.width * this.x + this.xx * tileInfo.width;
    this.obj.y = tileInfo.margin.y + tileInfo.height * this.y + this.yy * tileInfo.height;
};

Player.prototype.rotate = function(amount) {
	//Hier gaat men ervanuit dat men niet meer als 4 roteert.
	this.direction += amount;
	if (this.direction < 1) {
		this.direction = 4 - this.direction;
		this.spins--;
	} else if (this.direction > 4) {
		this.direction = this.direction - 4;
		this.spins++;
	}

	this.recalculateRotation();
    this.recalculatePosition();
};

Player.prototype.recalculateRotation = function() {
	var rot = 0;
	this.xx = 0;
	this.yy = 0;
	switch(this.direction) {
        case 1: rot = 0; 
            break;
        case 2: rot = 90; 
                this.xx = 1;
            break;
        case 3: rot = 180;
        		this.xx = 1;
                this.yy = 1;
            break;
        case 4: rot = 270;
                this.yy = 1;
            break;
    }
    this.obj.rotation = rot + 360 * this.spins;
};