var Game = Game || {};

Game.round = {
	playersReady : 0,
	playersProgramming : 0,
	playersPoweredDown : 0,
	phase : 0,
	register : 0,

	start : function() {
		var plys = Game.getPlayingPlayers()
		Game.round.playersProgramming = plys.length;
		
		console.log('Start Cards: ' + Game.programDeck.cards.length);
		
		plys.forEach(function(ply) {
			//Spawn robots
			if (ply.destroyed) {
				Game.respawnPlayer(ply);
			}

			//Power down robots
			if (ply.powerDownState === PowerDownState.NextRound) {
				ply.powerDown();
    			Game.status.setPlayerStatus(ply.name, PlayerStatus.PoweredDown)
    			console.log(ply.name + ' poweredDown');
				Game.round.playersProgramming--;

				if (!ply.bot)
					sLib.emit(ply.name, 'poweredDown');
			} else if (ply.powerDownState === PowerDownState.NotPoweredDown) {
				//Send program cards
				
				try {
					for (var i = 0; i < 9 - ply.damage; i++) {
						ply.programCards.push(Game.programDeck.pullCard());
					}
				} catch (e) {
					console.log(e);
				}
				

				Game.status.setPlayerStatus(ply.name, PlayerStatus.NotReady);
				if (!ply.bot)
					sLib.emit(ply.name, 'sendCards', ply.programCards);
			} else {
				ply.powerDown();
				Game.round.playersProgramming--;
			}
		});
		
		console.log('start after Cards: ' + Game.programDeck.cards.length);
		
		Game.getPlayingPlayers().forEach(function(ply) {
			if (ply.bot && ply.powerDownState !== PowerDownState.PoweredDown) {
				var powerDownState = PowerDownState.NotPoweredDown;
				var cards = ply.programCards.slice(0, 5);

				for (var i = ply.lockedCards.length - 1; i >= 0; i--) {
					cards.push(ply.lockedCards[i]);
				}

				var data = {
					player : ply.name,
					data : {
						cards : cards,
						poweredDown : powerDownState,
					},
				};
				Game.round.playerReady(data);
			}
		});

		//Everyone powered down or destroyed
		if (Game.round.playersProgramming === 0) {
			Game.round.execute();
		}
	},

	checkPowerDownState : function() {
		var plys = Game.getPlayingPlayers();
		
		for (var i = 0; i < plys.length; i++) {
			var ply = plys[i];
			if (ply.powerDownState === PowerDownState.PoweredDown) {
				ply.doneSendingPowerDownState = false;
				Game.round.playersPoweredDown++;
				sLib.emit(ply.name, 'powerDownCheck');
			}
		}

		if (Game.round.playersPoweredDown === 0)
			this.start();
	},

	playerSendsPowerDownInfo : function(data) {
		var ply = Game.findPlayer(data.player);
		var state = data.data;
		
		if (state === PowerDownState.NotPoweredDown) {
			ply.powerDownState = PowerDownState.NotPoweredDown;
		}
		ply.doneSendingPowerDownState = true;
		Game.round.playersPoweredDown--;
		if (Game.round.playersPoweredDown === 0) {
			Game.round.start();
		}
	},

	playerReady : function(data) {
		var name = data.player;
		var sentCards = data.data.cards;
		var ply = Game.findPlayer(name);

		if (ply === undefined) {
			console.log("A player return cards, but we can't find him! (" + name + ")");
			return;
		} else if (ply.powerDownState === PowerDownState.PoweredDown) {
			return;
		} else if (ply.doneProgramming) {
			console.log(ply.name + ' is trying to ready more then once, what a poop.');
			return;
		}

		var cards = [];
		//We could add cheat detection here, but fuck that, aint nobody got time for that

		//Cast cards to the right prototype 'class'
		for (var i = 0; i < sentCards.length; i++) {
			var c = sentCards[i];
			var card;

			switch(c.type) {
				case "MoveCard": card = new MoveCard(c.priority, c.amount);
					break;
				case "RotateCard": card = new RotateCard(c.priority, c.degree);
					break;
			}

			cards.push(card);
		}

		//Peter sends the locked cardsa anyway so we don't have to do this.
		//Locked cards go from 5 to 1, so we have to loop through it that way.
		// for (var i = ply.lockedCards.length - 1; i >= 0; i--) {
		// 	cards.push(ply.lockedCards[i]);
		// }

		ply.selectedCards = cards;
		ply.powerDownState = data.data.poweredDown;
		ply.doneProgramming = true;

		//Return unused cards to the deck
		console.log(ply.programCards.length + ' ' + ply.selectedCards.length);
		Game.programDeck.returnCards(ply.programCards, ply.selectedCards);
		ply.programCards = []; //Reset it since we returned those cards.
		console.log('Return some Cards: ' + Game.programDeck.cards.length);
		Game.status.setPlayerStatus(ply.name, PlayerStatus.Ready);

		Game.round.playersReady++;
		if (Game.round.playersReady === Game.round.playersProgramming) {
			Game.round.execute();
		} else if (Game.round.playersReady === Game.round.playersProgramming - 1) {
			//send timer data
			//Start timer on screen
		}
	},

	execute : function() {
		if (Game.checkWin())
			return;

		switch(Game.round.phase) {
			case 0: var x = Game.round.register + 1;
					Game.status.displayBigText("Phase " + x, Game.round.execute);
					Game.round.phase++;
				break;
			case 1: Game.round.phase++;
					Game.round.executeProgramCards();
				break;
			case 2: Game.round.phase++; //Do move board stuff
					Game.round.executeBoardElements(); 
				break;
			case 3: Game.round.phase++; //Board lasers
					Game.shootLaserWalls();
				break;		
			case 4: Game.round.phase++;
					Game.shootLasers();
				break;
			case 5: Game.round.phase++; //Flag and repair
					Game.round.executePostRound();
				break;
			case 6: Game.round.phase = 0;
					Game.round.register++;
					if (Game.round.register < 5)
						Game.round.execute();
					else {
						Game.round.cleanup();
						Game.round.checkPowerDownState();
					}
				break;
		}
	},

	executeProgramCards : function() {
		var cardQueue = [];
		var plys = Game.getLivingPlayers();

		//If no players are alive we just continue
		if (plys.length === 0) {
			Game.round.execute();
		}

		for(var j = 0; j < plys.length; j++) {
			var ply = plys[j];

			if (ply.powerDownState !== PowerDownState.PoweredDown) {
				cardQueue.push({ply : ply, card : ply.selectedCards[this.register]});
			}
		}

		cardQueue.sort(function(a, b) {
			//Cards never have the same priority.
			if (a.card.priority > b.card.priority)
				return 1;
			else
				return -1;
		});

		var runCard = function(cardQueue, index) {
			if (index < cardQueue.length) {
				var data = cardQueue[index];
				// console.log(data.ply.name + ' ' + data.card.type + " " + data.card.priority);
				
				index++;
				
				Game.playCard(data.ply, data.card, runCard, cardQueue, index);
			} else {
				Game.round.execute();
			}
		};

		if (cardQueue.length !== 0)
			runCard(cardQueue, 0);
		else
			Game.round.execute();
	},

	executeIndex : 0,
	executePhase : 0, //0 = express, 1 = express + normal, 2 = rotate
	executePlayers : [],
	executeBoardElements : function() {
		if (Game.round.executeIndex === 0) {
			Game.round.executePlayers = Game.getLivingPlayers();
		}

		if (Game.round.executeIndex === Game.round.executePlayers.length) {
			Game.round.executeIndex = 0;
			Game.round.executePhase++;
			if (Game.round.executePhase <= 2)
				Game.round.executeBoardElements();
			else {
				Game.round.executePhase = 0;
				Game.round.execute();
			}
		} else {
			var ply = Game.round.executePlayers[Game.round.executeIndex];
			var tile = Game.board.tiles[ply.x][ply.y];
			var executed = false;

			Game.round.executeIndex++;

			if (tile instanceof BoardElement) {	
				switch (Game.round.executePhase) {
					case 0: 
						if (tile instanceof ConveyorBelt && tile.express) {
							executed = true;
						}
						break;
					case 1:
						if (tile instanceof ConveyorBelt) {
							executed = true;
						}
						break;
					case 2:
						if (tile instanceof Rotator) {
							executed = true;
							
						} 
						break;
				}
			}
			if (executed)
				tile.execute(ply, Game.round.executeBoardElements);
			else
				Game.round.executeBoardElements();
		}
	},
	postRoundPlys : undefined,
	executePostRound : function() {
		if (Game.round.executeIndex === 0) {
			Game.round.postRoundPlys = Game.getLivingPlayers();
		}

		var plys = Game.round.postRoundPlys;

		if (Game.round.executeIndex >= plys.length) {
			Game.round.executeIndex = 0;
			Game.round.execute();
		} else {
			var ply = plys[Game.round.executeIndex]; 
			//Get tile which player is standing on
			var tile = Game.board.tiles[ply.x][ply.y];
			var executed = false;

			Game.round.executeIndex++;

			if (tile instanceof PostRoundElement) {	
				executed = true;
				tile.execute(ply);
			}

			//The tile didn't execute we don't have to wait.
			if (executed)
				setTimeout(Game.round.executePostRound, Game.EXECUTE_DELAY);
			else
				Game.round.executePostRound();
		}
	},

	//Return all the cards from the players to the deck,
	//don't return locked cards though.
	returnPlayerCards : function() {
		var plys = Game.players;
		var lockedCards = 0;
		for (var i = 0; i < plys.length; i++) {
			var ply = plys[i];
			lockedCards += ply.lockedCards.length;
			console.log(ply.selectedCards.length + " " + ply.lockedCards.length);

			if (ply.destroyed) {
				Game.programDeck.returnCards(ply.selectedCards, []);
				lockedCards -= ply.lockedCards.length;
				ply.lockedCards = [];
			} else
				Game.programDeck.returnCards(ply.selectedCards, ply.lockedCards);
			
			ply.selectedCards = []; // Reset variable
		}
		console.log('Cards after return: ' + Game.programDeck.cards.length + " " + lockedCards);
	},

	cleanup : function() {
		//Reset variables
		this.playersReady = 0;
		this.phase = 0;
		this.register = 0;

		//Reset player variables
		Game.getPlayingPlayers().forEach(function(ply) {
			ply.doneProgramming = false;

			if (ply.disconnecting) {
				ply.disconnecting = false;
				ply.connected = false;
				Game.status.updatePlayer(ply.name);
				Game.board.removePlayer(ply);
			}
		});

		//Return cards to the deck
		this.returnPlayerCards();
	},
};

