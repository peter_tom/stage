var Game = Game || {};

Game.status = {
	//Constants
	DEFAULT_FONT : "18px Optima",
	//Background elements
	bgColor : new createjs.Shape(),
	returnBg : new createjs.Shape(),

	//Return
	returnText : new createjs.Text("Return to lobby", "10px Optima", "#FFFFFF"),

	//PlayerInfo
	plyColours : {},
	plyNames : {},
	plyDamages : {},
	plyLives : {},
	plyCheckpoints : {},
	plys : [],

	//Logging
	logBg : new createjs.Shape(),
	logText : new createjs.Text("", this.DEFAULT_FONT, "#FFFFFF"),

	//Variables
	loaded : false,
	scoreboardLoaded : false,
	events : [],
    ow : 360,
    oh: 780,

	init : function() {
        //Game container;
        this.container = new createjs.Container();
        Game.stage.addChild(this.container);
        this.addComponents();
        this.bindEvents();

        this.loaded = true;
        console.log("should resize in init now");
        this.resize();
    },

    initScoreboard : function() {
        var plys = Game.players; 

        //Add header
        var header = {
            header : true,
            colour : "#000000",
            name : "",
            damage : "D",
            lives : "♥",
            currentFlag : "F"
        }
        plys.unshift(header);

        for (var i = 0; i < plys.length; i++) {
        	var ply = plys[i];
        	if (ply.header)
        		this.plys.push(ply)
        	else
        		this.plys.push(ply.name);
        	
        	var colour = new createjs.Shape()
        	this.plyColours[ply.name] = colour;
        	this.container.addChild(colour);

        	var name = new createjs.Text(ply.name.substr(0, 10) + " Bot", Game.status.DEFAULT_FONT, "#FFFFFF");
        	this.plyNames[ply.name] = name;
        	this.container.addChild(name);

        	var dmg = new createjs.Text(ply.damage, Game.status.DEFAULT_FONT, "#FFFFFF");
        	this.plyDamages[ply.name] = dmg;
        	this.container.addChild(dmg);

        	var lives = new createjs.Text(ply.lives, Game.status.DEFAULT_FONT, "#FFFFFF");
        	this.plyLives[ply.name] = lives;
        	this.container.addChild(lives);

        	var checkpoint = new createjs.Text(ply.currentFlag, Game.status.DEFAULT_FONT, "#FFFFFF");
        	this.plyCheckpoints[ply.name] = checkpoint;
        	this.container.addChild(checkpoint);
        }
        plys.shift();
        this.resizeScoreboard();
        this.resize();
    },

    addComponents : function() {
        //Add background elements
        this.container.addChild(this.bgColor);
        this.container.addChild(this.returnBg);

        //Add Return text
        this.container.addChild(this.returnText);

        //Logging
        // this.container.addChild(this.logBg);    
        // this.container.addChild(this.logText);	

        //position them
        this.resizeScoreboard();
        this.resizeReturnButton();
        // this.resizeLogging();
    },

    bindEvents : function() {
    	this.returnText.on('click', sLib.returnToLobby);
    	this.returnBg.on('click', sLib.returnToLobby);
    },

    //Since we're only working with landscape view, for now
    //we can just take 33% of the width for the status
    resize : function() {
    	if (!this.loaded)
            return;

    	this.width = Game.width / 3;
        this.height = Game.height;

        this.container.x = Game.width - this.width;

        //Update background color
        // var g = new createjs.Graphics();
        // g.beginFill("#000000").drawRect(0, 0, this.width, this.height);
        // this.bgColor.graphics = g;

		//Resize scoreboard
        this.container.scaleX = this.width/this.ow;
        this.container.scaleY = this.height/this.oh;
    },

    resizeReturnButton : function() {
        var width = this.ow;
        var height = this.oh;

        var returnAreaH = height * 0.10;

    	//Background
    	var g = new createjs.Graphics();
        g.beginFill("#660066").drawRect(0, 0, width, returnAreaH)
        this.returnBg.graphics = g;

        //Text
        var fontSize = width / 6.4; 
        var font = (fontSize > 50) ? 50 : fontSize; //Limit the font so we don't go crazy, probably not a good way though 
        this.returnText.font = fontSize + "px Optima";

        //Center text
        this.returnText.x = (width - this.returnText.getMeasuredWidth()) / 2;
        this.returnText.y = (returnAreaH - this.returnText.getMeasuredHeight()) / 2;
    },

    resizeScoreboard : function() {
    	//Oh god have mercy upon my pixels, let them ever be correct.
        var height = this.oh;
        var width = this.ow;
    	var h = height * 0.90;//Game.height * 0.50;
    	var offsetY = height * 0.10;//Game.height * 0.10;
    	
    	var scaling = Game.players.length;
    	if (scaling < 8)
    		scaling = 8;
    	var lineHeight = h / (scaling + 1);

    	for (var i = 0; i < this.plys.length; i++) {
    		var name = this.plys[i];
            if (name.header) { //Header 
                var ply = name;
                name = ply.name;
            } else
    		  var ply = Game.findPlayer(name);
			var offsetLine = offsetY + lineHeight * i + lineHeight / 2;

			//Colour
			var colour = this.plyColours[name];
			var colourRGB = createjs.Graphics.getRGB(ply.colour.r, ply.colour.g, ply.colour.b);
			var colourH = lineHeight;
			var colourW = this.width * 0.10;

			colour.graphics.clear();
			colour.graphics.beginFill(colourRGB).drawRect(0, 0, colourW, colourH); //Square aw yeee
			colour.x = 5;
			colour.y = offsetY + lineHeight * i; //Make the colour not edge

			//Name
			var nameUI = this.plyNames[name];
			nameUI.x = colourW + 10;
			nameUI.y = offsetLine - (nameUI.getMeasuredHeight() / 2);

			var infoStart = width/2;//this.width / 2;

			//Damage
			var dmg = this.plyDamages[name];
			dmg.x = infoStart + infoStart * 0.25;
			dmg.y = offsetLine - (nameUI.getMeasuredHeight() / 2);

			//Lives
			var live = this.plyLives[name];
			live.x = infoStart + infoStart * 0.50;
			live.y = offsetLine - (nameUI.getMeasuredHeight() / 2);

			//Checkpoint
			var checkpoint = this.plyCheckpoints[name];
			checkpoint.x = infoStart + infoStart * 0.75;
			checkpoint.y = offsetLine - (nameUI.getMeasuredHeight() / 2);

            this.scoreboardLoaded = true;
    	}
    },

    updatePlayer : function(name) {
    	var ply = Game.findPlayer(name);

    	if (typeof ply === "undefined") {
    		return;
    	}
    	
    	if (ply.connected) {
    		this.plyDamages[ply.name].text = "" + ply.damage;
    		this.plyLives[ply.name].text = "" + ply.lives;
    		this.plyCheckpoints[ply.name].text = "" + ply.currentFlag; 
    		if (ply.destroyed)
    			Game.status.setPlayerStatus(name, PlayerStatus.Destroyed);
    	} else {
			this.plyNames[name].text = name + " Bot";
			Game.status.setPlayerStatus(name, PlayerStatus.Disconnected);
    	}
    	Game.stage.update();
    },

    setPlayerStatus : function(ply, status) {
    	this.plyNames[ply].color = status;
    	this.plyDamages[ply].color = status;
    	this.plyLives[ply].color = status;
    	this.plyCheckpoints[ply].color = status;

    	Game.stage.update();
    },

    resizeLogging : function() {
    	//Static resize test
    	//Yey
        var width = this.ow;
        var height = this.oh;
    	var loggingAreaH = height * 0.40;
    	var offsetY = height - loggingAreaH;

    	//Background
    	var g = new createjs.Graphics();
    	g.beginFill("#006600").drawRect(0, 0, width, loggingAreaH);
    	this.logBg.graphics = g;
    	this.logBg.y = offsetY;

    	//Text
    	this.logText.font = "20px Optima";
    	this.logText.x = 30;
    	this.logText.y = 30 + offsetY;
    },

    displayBigText : function(text, callback) {
    	var fontSize = 100;
    	var bigText = new createjs.Text(text, fontSize + "px Optima", "#FF0066");

    	while(bigText.getMeasuredWidth() > Game.width) {
    		fontSize = fontSize * 0.9;
    		bigText.font = fontSize + "px Optima";
    	}
    	
    	var bigBg = new createjs.Shape();
    	var w = bigText.getMeasuredWidth();
    	var h = bigText.getMeasuredHeight();

    	bigBg.x = bigText.x = (Game.width - w) / 2;
    	bigBg.y = bigText.y = (Game.height - h) / 2;
    	bigBg.alpha = 0.75;

    	bigBg.graphics.beginFill("#000000").drawRect(0, 0, w, h);

    	Game.stage.addChild(bigBg);
    	Game.stage.addChild(bigText);
    	Game.stage.update();

    	setTimeout(function(txt, bigBg, callback) {
    		Game.stage.removeChild(txt);
    		Game.stage.removeChild(bigBg);

    		Game.stage.update();
    		if (typeof callback !== "undefined")
    			callback();
    	}, Game.EXECUTE_DELAY, bigText, bigBg, callback);
    },

    logEvent : function(txt) {
    	return;
    	this.events.push(txt);
    	
    	if (this.events.length > 10) {
    		this.events.splice(0, 1);
    	}
    	var text = "";
    	for (var i = this.events.length - 1; i >= 0; i--) {
    		text += this.events[i] + "\n";
    	}
    	this.logText.text = text;
    	Game.stage.update();
    },
};