var Game = Game || {};

function Tile(x, y, walls) {
	this.x = x;
	this.y = y;
	this.rotationX = this.rotationY = 0;
	this.walls = walls;
	if (typeof this.walls === "undefined")
		this.walls = [];

	for(var i = 0; i < this.walls.length; i++)
		this.walls[i].tile = this;
	
	this.obj = new createjs.Bitmap(Resources.getResult(this.img));
}

Tile.prototype.resizeWalls = function() {
	if (this.walls !== undefined)
	    this.walls.forEach(function(wall) {
	    	wall.resize();
	    });
};

Tile.prototype.resize = function() {
	var tileInfo = Game.board.tileInfo;
    var ratio = tileInfo.width / Resources.getResult(this.img).width;

    this.obj.scaleX = ratio;
    this.obj.scaleY = ratio;
    
    this.obj.x = Math.floor(tileInfo.margin.x + this.x * tileInfo.width + this.rotationX * tileInfo.width);
    this.obj.y = Math.floor(tileInfo.margin.y + this.y * tileInfo.height + this.rotationY * tileInfo.height);

    this.resizeWalls();
};

Tile.prototype.hasWall = function(direction) {
	for (var i = 0; i < this.walls.length; i++) {
		if (this.walls[i].direction === direction)
			return true;
	}
	return false;
};

function Floor(x, y, walls) {
	this.img = "floor_tile";

	Tile.call(this, x, y, walls);
}
Floor.prototype = new Tile;

function StartPoint(x, y, walls, id, direction) {
	this.img = "start_point";
	this.id = id;
	this.direction = direction;
	
	Tile.call(this, x, y, walls);
}
StartPoint.prototype = new Tile

function Pit(x, y, walls) {
	this.img = "pit";

	Tile.call(this, x, y, walls);
}
Pit.prototype = new Tile;

function BoardElement(x, y, walls) {
	Tile.call(this, x, y, walls);
}
BoardElement.prototype = new Tile;

function Rotator(x, y, walls, rotation) {
	if (rotation === "left") {
		this.rotation = -1;
		this.img = "rotate_left";
	} else {
		this.rotation = 1;
		this.img = "rotate_right";
	}

	BoardElement.call(this, x, y, walls);
}
Rotator.prototype = new BoardElement;

Rotator.prototype.execute = function(ply) {
	Game.rotatePlayer(ply, this.rotation);
	
	var args = Array.prototype.slice.call(arguments, 1);
    Game.animation.animate.apply(Game.animation.animatie, args); //Animate moves
};

function ConveyorBelt(x, y, walls, direction, rotation, express) {
	this.express = (typeof express === "undefined") ? false : express;
	// this.rotation = (typeof rotation === "undefined") ? false : rotation;

	if (rotation === "left") {
		this.rotation = -1;
		this.img = "conveyorbelt_left";
	} else if (rotation === "right") {
		this.rotation = 1;
		this.img = "conveyorbelt_right";
	} else {
		this.img = "conveyorbelt";
	}

	if (this.express)
		this.img += "_express";

	BoardElement.call(this, x, y, walls);
	this.direction = direction;

	var rot = 0;
	switch(this.direction) {
        case 2: rot = 90; 
                this.rotationX = 1;
            break;
        case 3: rot = 180;
        		this.rotationX = 1;
                this.rotationY = 1;
            break;
        case 4: rot = 270;
                this.rotationY = 1;
            break;
    }
    this.obj.rotation = rot;
}
ConveyorBelt.prototype = new BoardElement;

ConveyorBelt.prototype.execute = function(ply) {
	var nextTilePos = getNextSquare(this.x, this.y, this.direction);
	var args = Array.prototype.slice.call(arguments, 1);
	var args2 = Array.prototype.slice.call(args, 1);
	if (nextTilePos.x < 0 || nextTilePos.y < 0 || nextTilePos.x >= Game.board.x || nextTilePos.y >= Game.board.y) {
		args[0].apply(args[0], args2);
    } else {
		var nextTile = Game.board.tiles[nextTilePos.x][nextTilePos.y];
		//if (this.express === nextTile.express)
		if (Game.movePlayer(ply, this.direction)) {
			if (nextTile instanceof ConveyorBelt && nextTile.rotation !== undefined) {
				Game.animation.nextFrame();
				Game.rotatePlayer(ply, nextTile.rotation);
				// Game.animation.addObjToFrame(robot.obj);
				// ply.rotate(nextTile.rotation); 
			}

			Game.animation.animate.apply(Game.animation.animate, args);
		} else {
			args[0].apply(args[0], args2);
		}
	}
};

function PostRoundElement(x, y, walls) {
	Tile.call(this, x, y, walls);
}
PostRoundElement.prototype = new Tile;

function Repair(x, y, walls) {
	this.img = "repair";

	PostRoundElement.call(this, x, y, walls);
}

Repair.prototype = new PostRoundElement;

Repair.prototype.execute = function(ply) {
	if (!ply.destroyed) {
		if (ply.damage > 0) {
			ply.repair(1);
			Game.status.logEvent(ply.name + ' repaired one damage.');
		}
	}
};

function Flag(x, y, walls, id) {
	this.id = id;
	this.img = "flag" + id;

	PostRoundElement.call(this, x, y, walls);
}

Flag.prototype = new PostRoundElement;

Flag.prototype.execute = function(ply) {
	if (this.id === ply.currentFlag + 1) {
		Game.status.logEvent(ply.name + " just got to flag #" + this.id + "!");
		ply.currentFlag++;
		ply.checkpoint.x = this.x;
		ply.checkpoint.y = this.y;
		ply.checkpoint.direction = ply.direction;

		Game.hasPlayerFinished(ply);
		Game.status.updatePlayer(ply.name);
	}
};