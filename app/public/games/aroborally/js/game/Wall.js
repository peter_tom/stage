function Wall(direction, lasercount) {
	this.direction = direction;
	this.lasercount = ((typeof lasercount !== "undefined") ? lasercount : 0);
	this.xx = 0;
	this.yy = 0;
	this.tile = undefined;

	this.img = "wall" + ((this.lasercount > 0) ? this.lasercount : "");
	this.obj = new createjs.Bitmap(Resources.getResult(this.img));

	switch(this.direction) {
        case 1: rot = 0; 
            break;
        case 2: rot = 90; 
                this.xx = 1;
            break;
        case 3: rot = 180;
        		this.xx = 1;
                this.yy = 1;
            break;
        case 4: rot = 270;
                this.yy = 1;
            break;
    }
    this.obj.rotation = rot;
}

Wall.prototype.resize = function() {
	if (this.tile !== undefined) {
		var tileInfo = Game.board.tileInfo;
		var ratio = tileInfo.width / Resources.getResult(this.img).width;

	 	this.obj.scaleX = ratio;
	    this.obj.scaleY = ratio;
    
	    this.obj.x = Math.floor(tileInfo.margin.x + this.tile.x * tileInfo.width + tileInfo.width * this.xx);
	    this.obj.y = Math.floor(tileInfo.margin.y + this.tile.y * tileInfo.height + tileInfo.height * this.yy);
	}
};
