var Game = Game || {};

Game.maps.Arena = {
	width : 6,
	height : 6,
	board : [
		//eerste rij
		[
			{id : 4, },
			{id : 7, pointId : 9, direction : 3},
			{id : 7, pointId : 1, direction : 3},
			{id : 7, pointId : 5, direction : 3},
			{id : 7, pointId : 11, direction : 3},
			{id : 4, },
		],
		//2de rij
		[
			{id : 7, pointId : 13, direction : 2},
			{id : 1, },
			{id : 1, },
			{id : 1, },
			{id : 1, },
			{id : 7, pointId : 14, direction : 4},
		],
		//3de rij
		[
			{id : 7, pointId : 3, direction : 2},
			{id : 1, },
			{id : 5, flag : 1,},
			{id : 5, flag : 2,},
			{id : 1, },
			{id : 7, pointId : 4, direction : 4},
		],
		//4de rij
		[
			{id : 7, pointId : 7, direction : 2},
			{id : 1, },
			{id : 5, flag : 4,},
			{id : 5, flag : 3,},
			{id : 1, },
			{id : 7, pointId : 8, direction : 4},
		],
		//5de rij
		[
			{id : 7, pointId : 15, direction : 2},
			{id : 1, },
			{id : 1, },
			{id : 1, },
			{id : 1, },
			{id : 7, pointId : 16, direction : 4},
		],
		//6de rij
		[
			{id : 4, },
			{id : 7, pointId : 10, direction : 1},
			{id : 7, pointId : 2, direction : 1},
			{id : 7, pointId : 6, direction : 1},
			{id : 7, pointId : 12, direction : 1},
			{id : 4, },
		],
	],
};