Resources = new createjs.LoadQueue(true);

Resources.init = function() {
    var manifest = [
        {id:"floor_tile", src:"assets/images/floor_tile2.svg", type:createjs.LoadQueue.IMAGE},
        {id:"robot", src:"assets/images/robot.svg", type:createjs.LoadQueue.IMAGE},
        {id:"wall", src:"assets/images/wall.svg", type:createjs.LoadQueue.IMAGE},
        {id:"wall1", src:"assets/images/wall1.svg", type:createjs.LoadQueue.IMAGE},
        {id:"wall2", src:"assets/images/wall2.svg", type:createjs.LoadQueue.IMAGE},
        {id:"wall3", src:"assets/images/wall3.svg", type:createjs.LoadQueue.IMAGE},
        {id:"conveyorbelt", src:"assets/images/conveyorbelt.svg", type:createjs.LoadQueue.IMAGE},
        {id:"conveyorbelt_left", src:"assets/images/conveyorbelt_left.svg", type:createjs.LoadQueue.IMAGE},
        {id:"conveyorbelt_right", src:"assets/images/conveyorbelt_right.svg", type:createjs.LoadQueue.IMAGE},
        {id:"conveyorbelt_express", src:"assets/images/conveyorbelt_express.svg", type:createjs.LoadQueue.IMAGE},
        {id:"conveyorbelt_left_express", src:"assets/images/conveyorbelt_left_express.svg", type:createjs.LoadQueue.IMAGE},
        {id:"conveyorbelt_right_express", src:"assets/images/conveyorbelt_right_express.svg", type:createjs.LoadQueue.IMAGE},
        {id:"rotate_left", src:"assets/images/rotate_left.svg", type:createjs.LoadQueue.IMAGE},
        {id:"rotate_right", src:"assets/images/rotate_right.svg", type:createjs.LoadQueue.IMAGE},
        {id:"pit", src:"assets/images/pit2.svg", type:createjs.LoadQueue.IMAGE},
        {id:"start_point", src:"assets/images/start_point.svg", type:createjs.LoadQueue.IMAGE},
        {id:"repair", src:"assets/images/repair.svg", type:createjs.LoadQueue.IMAGE},
        {id:"flag1", src:"assets/images/flag1.svg", type:createjs.LoadQueue.IMAGE},
        {id:"flag2", src:"assets/images/flag2.svg", type:createjs.LoadQueue.IMAGE},
        {id:"flag3", src:"assets/images/flag3.svg", type:createjs.LoadQueue.IMAGE},
        {id:"flag4", src:"assets/images/flag4.svg", type:createjs.LoadQueue.IMAGE},
    ];

    Resources.on("complete", function() {
        console.log('Resources: Loading complete.');
        Game.init();
    } , this);

    console.log('Resources: Loading started.');
    Resources.loadManifest(manifest);
}