Resources = new createjs.LoadQueue(false);

Resources.init = function() {
    if(!document.createElement('svg').getAttributeNS){
      alert("We're sorry, your browser does not support svg and cannot play this game.");
      window.location = "index.html";
    } else {
      console.log(!document.createElement('svg').getAttributeNS);
    }
    var manifest = [
        {id:"move_1", src:"assets/images/move1.svg", type:createjs.LoadQueue.IMAGE},
        {id:"move_2", src:"assets/images/move2.svg", type:createjs.LoadQueue.IMAGE},
        {id:"move_3", src:"assets/images/move3.svg", type:createjs.LoadQueue.IMAGE},
        {id:"move_backwards", src:"assets/images/movebackwards.svg", type:createjs.LoadQueue.IMAGE},
        {id:"rotate_left", src:"assets/images/card_rotate_left.svg", type:createjs.LoadQueue.IMAGE},
        {id:"rotate_right", src:"assets/images/card_rotate_right.svg", type:createjs.LoadQueue.IMAGE},
        {id:"u_turn", src:"assets/images/card_uturn.svg", type:createjs.LoadQueue.IMAGE},
    ];

    Resources.on("complete", function() {
        console.log('Loading complete.');
        if (Resources.errors) {
            alert("We're sorry, you had errors preloading necessary files, most likely related to your browser being bad :(\nTry using a better browser.")
            cLib.init()
            cLib.on('initialized', function() { window.location = '/index.html'});
        } else {
            App.init();
        }
    } , this);

    Resources.on("error", function(target, type) {
        console.log('Error: ' + target + " " + type);
        Resources.errors = true;
    } , this);
    console.log('Preloading files..');
    Resources.loadManifest(manifest);
}