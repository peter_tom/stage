PowerDownState = {
	NotPoweredDown : 1,
	NextRound : 2,
	PoweredDown : 3,
};

PlayerStatus = {
	Ready : "#00FF00",
	NotReady : "#ED8618",
	Disconnected : "#CF1919",
	PoweredDown : "#F01AA5",
	Destroyed : "F06007",
};