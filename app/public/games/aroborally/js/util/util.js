//Source: http://stackoverflow.com/questions/966225/how-can-i-create-a-two-dimensional-array-in-javascript 
function createArray(length) {
    var arr = new Array(length || 0),
        i = length;

    if (arguments.length > 1) {
        var args = Array.prototype.slice.call(arguments, 1);
        while(i--) arr[length-1 - i] = createArray.apply(this, args);
    }

    return arr;
}

function invertDirection(dir) {
	var iDir = dir + 2;
    if (iDir > 4)
        iDir = iDir - 4;

    return iDir;
}

function getNextSquare(x, y, dir) {
	var sq = {}; sq.x = x; sq.y = y;
	switch(dir) {
		case 1: sq.y -= 1;
			break;
		case 2: sq.x += 1;
			break;
		case 3: sq.y += 1;
			break;
		case 4: sq.x -= 1;
			break;
	}
	return sq;
}