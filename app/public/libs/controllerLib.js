cLib = {
    playerData : undefined,

    init : function() {
        var tmp        = document.createElement ('a');
        ;   tmp.href   = document.URL;
        cLib.socket = io.connect(tmp.hostname, {
          'sync disconnect on unload': true
        });
        cLib.bindEvents(); 
    },

    bindEvents : function() {
        cLib.socket.on('connected', cLib.onConnected);
        cLib.socket.on('initialized', cLib.onInitialized);
        cLib.socket.on('sendPlayerData', cLib.onReceivePlayerData);
        cLib.socket.on('redirect', cLib.onRedirect);
    },

    onRedirect : function(url) {
        window.location.href = url;
    },

    onConnected : function() {
        console.log('Player library connected to server.');
        cLib.socket.emit('gamePlayerSocket');
    },

    onInitialized : function() {
        console.log('Server socket initialized.');
    },

    onReceivePlayerData : function(data) {
        cLib.playerData = data;
    },

    emit : function(func, data) {
        var args = {
            func : func,
        };
        if (typeof data !== "undefined")
            args.data = data;
        cLib.socket.emit('emit', args);
    },

    on : function(strFunc, func) {
        cLib.socket.on(strFunc, func);
    },
};