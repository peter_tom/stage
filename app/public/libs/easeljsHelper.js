function createCoolButton(container, text, font, x, y, func, color, borderColor, fontColor) {
    var borderSize = 2;
    var radius = 5;
    
    if (color === undefined) {
        color = "#5cb85c";
        borderColor = "#4cae4c";
        fontColor = "#FFF";
    }

    var button = new createjs.Shape();
    var buttonText = new createjs.Text(text, font, fontColor);

    var paddingX = 10;
    var paddingY = 10;

    var xMiddle = buttonText.getMeasuredWidth() / 2;
    var yMiddle = buttonText.getMeasuredHeight() / 2;

    buttonText.x = x - xMiddle;
    buttonText.y = y - yMiddle;

    //De helft van de width
    var buttonWidth = paddingX + xMiddle + borderSize;
    var buttonHeight = paddingY + yMiddle + borderSize;

    button.x = buttonText.x + xMiddle - buttonWidth;
    button.y = buttonText.y + yMiddle - buttonHeight;

    button.graphics.beginFill(borderColor).drawRoundRect(0, 0, buttonWidth * 2, buttonHeight * 2, radius);
    button.graphics.beginFill(color).drawRoundRect(borderSize, borderSize, buttonWidth * 2 - borderSize * 2, buttonHeight * 2 - borderSize * 2, radius);

    button.on("click", func);
    buttonText.on("click", func);

    container.addChild(button);
    container.addChild(buttonText);  
}