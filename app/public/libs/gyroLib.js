var lastMove = {
	beta : 0,
	gamma : 0,
};

if (window.DeviceMotionEvent == undefined) {
	initGyroAlternative();
} else {
    //Global var so we can clear it if necessary.
    gyroListener = window.addEventListener("deviceorientation", onDeviceOrientation, false);
	gyroChecker = window.setInterval(checkGyroData, 2);

    function onDeviceOrientation(event){
        lastMove.beta = event.beta.toFixed(2);
        lastMove.gamma = event.gamma.toFixed(2);
    }

    function checkGyroData() {
		if (lastMove.beta == 0 && lastMove.gamma == 0) {
			initGyroAlternative();
			window.clearInterval(gyroListener);
		}
		window.clearInterval(gyroChecker);
	}
}

function initGyroAlternative() {
	touchAccelerometer();
	mouseAccelerometer();

   	function handleTouchMove(event) {
		lastMove.beta = (event.touches[0].pageY-(window.innerHeight/2)) / (window.innerHeight/2)*45;
	    lastMove.gamma = (event.touches[0].pageX-(window.innerWidth/2)) / (window.innerWidth/2)*45;

	    lastMove.beta.toFixed(2);
	    lastMove.gamma.toFixed(2);
	}

	function touchAccelerometer() {
	    window.addEventListener("touchstart", handleTouchMove, false);
	    window.addEventListener("touchend", handleTouchMove, false);
	    window.addEventListener("touchcancel", handleTouchMove, false);
	    window.addEventListener("touchleave", handleTouchMove, false);
	    window.addEventListener("touchmove", handleTouchMove, false);
	}

	function handleMouseMove(event) {
		lastMove.beta = (event.y-(window.innerHeight/2)) / (window.innerHeight/2)*45;
	    lastMove.gamma = (event.x-(window.innerWidth/2)) / (window.innerWidth/2)*45;

	    lastMove.beta.toFixed(2);
	    lastMove.gamma.toFixed(2);
	}

	function mouseAccelerometer() {
	    window.addEventListener("mousemove", handleMouseMove, false);
	}
}

function sendGyroData() {
	cLib.emit('gyro', lastMove);
}

window.setInterval(sendGyroData, 250);