var sLib = {
    players : { },
    playersLoaded : false,
    parameters : [],
    initialized : false,

    init : function() {
        var tmp        = document.createElement ('a');
        ;   tmp.href   = document.URL;
        sLib.socket = io.connect(tmp.hostname, {
          'sync disconnect on unload': true
        });
        sLib.bindEvents(); 
    },

    bindEvents : function() {
        sLib.socket.on('connected', sLib.onConnected);
        sLib.socket.on('initialized', sLib.onInitialized);

        sLib.socket.on('redirect', sLib.onRedirect);
        //mss nie
        sLib.socket.on('allPlayersLoaded', sLib.onAllPlayersLoaded);
        sLib.socket.on('playerDisconnected', sLib.onPlayerDisconnected);
        sLib.socket.on('playerJoined', sLib.onPlayerJoined);
        sLib.socket.on('sError', function(error) {
            console.log('sLib Error: ' + error);
        });
    },

    onRedirect : function(url) {
        window.location.href = url;
    },

    onAllPlayersLoaded : function(data) {
        sLib.players = data;
        sLib.playersLoaded = true;
        console.log('sLib: all players loaded');
    },

    onPlayerDisconnected : function(player) {
        for (var i = 0; i < sLib.players.length; i++) {
            if (sLib.players[i].name === player) {
                sLib.players.splice(i, 1);
                break;
            }
        };
    },

    onConnected : function() {
        console.log('sLib: connected to server.');
        sLib.socket.emit('gameHostSocket');
    },

    onInitialized : function(parameters) {
        console.log('sLib: Server socket initialized.');
        sLib.parameters = parameters;
        initialized = true;
    },

    on : function(strFunc, func) {
        sLib.socket.on(strFunc, func);
    },

    getAllPlayers : function() {
        //Logica etc
        return sLib.players;
    },

    emit : function(player, func, data) {
        var args = {
            player : player,
            func : func,
        };
        if (typeof data !== "undefined")
            args.data = data;
        
        sLib.socket.emit('emit', args);
    },

    emitAll : function(func, data) {
        var args = {
            func : func,
            data : data,
        }
        sLib.socket.emit('emitAll', args);
    },

    returnToLobby : function() {
        sLib.socket.emit('returnToLobby');
    },
};
