    var IO = {

        init: function() {
            var tmp        = document.createElement ('a');
            ;   tmp.href   = document.URL;
            IO.socket = io.connect(tmp.hostname, {
              'sync disconnect on unload': true
            });
            IO.bindEvents();
        },

        bindEvents : function() {
            IO.socket.on('connected', IO.onConnected);
            IO.socket.on('receiveData', IO.onReceiveData);
            IO.socket.on('playGame', IO.onPlayGame);
            IO.socket.on('loadGames', IO.onLoadGames);
            IO.socket.on('redirect', IO.onRedirect);
            IO.socket.on('update', IO.onUpdate);
            IO.socket.on('sendLobbyData', IO.onReceiveLobbyData);
        },

        onConnected : function() {
            IO.socket.emit('lobbyHostSocket');
        },

        onReceiveData : function(data) {
            App.setInitialData(data);
        },

        onRedirect : function(url) {
            window.location.href = url;
        },

        onUpdate : function(data) {
            App.clearPlayers();
            if (data !== undefined) {
                $('#nPlayers').text('Playerlist (' + data.length + ')');
                data.forEach(function(player) {
                    App.addPlayer(player);
                });

                IO.players = data;
                App.verifyStartButton();
            }
        },

        onReceiveLobbyData : function(data) {
            IO.data = data;

            console.log(IO.data);
            
            App.onReceiveLobbyData();
        },

        startGame : function() {
           
            //send option data too
            var options = [];

            var selectedGame = $("#game").val();
            IO.data.games.forEach(function(game) {
                if (game.path === selectedGame) {
                    var opts = game.options;
                    if (opts !== undefined)
                    for(var i = 0; i < opts.length;i++){
                        var name = opts[i].settings.name;
                        var obj = new Object();
                        if(opts[i].settings.type === "dropdown"){
                            obj.name = name;
                            obj.value = $('#option' + name).val();
                        } else if(opts[i].settings.type === "checkbox"){
                            obj.name = name;
                            obj.value = $('#option' + name).prop('checked');
                        }
                        options.push(obj);
                    }
                }
            });

            var data = new Object();
            data.game = selectedGame;
            data.parameters = options;

            IO.socket.emit('startGame', data);
        },
    };

    var App = {
        init: function() {
            App.$doc = $(document);
            App.bindEvents();
        },

        bindEvents : function() {
            App.$doc.on('click', '#btnHostGame', App.onHostGame);
            App.$doc.on('click', '#btnJoinGame', App.onJoinGame);
            App.$doc.on('click', '#btnStart', IO.startGame);
            App.$doc.on('click', '#btnLeave', function() {
                IO.onRedirect('/');
            });
            
            $('#game').change(App.onSelectGame);
        },

        onReceiveLobbyData : function() {
            $('#lobbyId').text(IO.data.id);

            IO.data.games.forEach(function(game) {
                $('#game').append('<option value="' + game.path +'">' + game.name + '</option>');
            });
            
            console.log(IO.data.selectedGame);

            if (IO.data.selectedGame !== undefined) {
                $('select[id="game"]').find('option[value="' + IO.data.selectedGame + '"]').attr("selected",true);
            }

            $('#game').change();
            new QRCode(document.getElementById("qrcode"), "http://pettom.xploregroup.cloudbees.net/lobby/" + IO.data.id);
        },

        onSelectGame : function() {
            var selectedGame = $(this).val();

            IO.data.games.forEach(function(game) {
                if (game.path === selectedGame) {
                    IO.selectedGame = game;
                    $('#lobbyTitel').text(game.name);
                    $('#description').text(game.description);
                    IO.socket.emit('selectGame', game.path);

                    $('body').css('background-image','url(../games/' + game.path +'/cover.png)');
                    $('#options').empty();
                    if(game.options !== undefined){
                    $('#options').append('<strong>Options</strong><br/>');
                        for(var i = 0; i < game.options.length; i++){
                            var obj = game.options[i];
                            if(obj.settings.type === "dropdown"){
                                $('#options').append('' + obj.settings.name +'');
                                $('#options').append('<select id="' + "option" + obj.settings.name +'"></select><br/>');
                                if(obj.settings.changesMaxPlayers === true){
                                    $('#option' + obj.settings.name).on('change', function() {
                                        game.maxPlayers = App.findAmountPlayersForOption(this.value, game);
                                        $('#playerMinMax').text('Players ' + game.minPlayers + ' - ' + game.maxPlayers);
                                    });
                                }
                                for(var u = 0; u < obj.choices.length;u++){
                                    var choice = obj.choices[u];
                                    $('#option' + obj.settings.name).append('<option value="' + choice[Object.keys(choice)[0]] +'">' + choice[Object.keys(choice)[0]]  + '</option>');
                                }
                            } else if(obj.settings.type === "checkbox"){
                                    $('#options').append(' <input type="checkbox" id="' + "option" + obj.settings.name +'">' + obj.settings.name + '<br/>');
                            }
                        }
                    }
                    $('#playerMinMax').text('Players ' + game.minPlayers + ' - ' + game.maxPlayers);
                }
            });

            App.verifyStartButton();
        },

        findAmountPlayersForOption : function(name, game) {
            for(var i = 0; i < game.options.length; i++){
                var obj = game.options[i];
                if(obj.settings.type === "dropdown"){
                    if(obj.settings.changesMaxPlayers === true){
                        for(var u = 0; u < obj.choices.length;u++){
                            var choice = obj.choices[u];
                            if(choice.value === name){
                                return choice.nPlayers;
                            }    
                        }
                    }
                }
            }
        },

        onHostGame : function() {
            console.log("Trying to host game..");
            var game = $("#gamesDropDown").val();

            console.log("Game:" + game);
            IO.socket.emit('hostGame', game);
        },

        onJoinGame : function() {
            console.log("Trying to join a game..");
            var room = $("#inputGameRoom").val();

            IO.socket.emit('joinGame', room);
        },

        clearPlayers : function() {
            $('#playerList').empty();
        },

        addPlayer : function(player) {
            var colorString = 'rgb(' + player.color.r + ', ' + player.color.g + ', ' + player.color.b + ')';
            var tableString = '<tr><td>' + player.name + '</td>';
            
            tableString += '<td><button class="btn" style="background-color : ' + colorString + '">colour</button></td>';
            
            if (player.connected)
                tableString += '<td><span class="label label-success">connected</span></td>';
            else
                tableString += '<td><span class="label label-danger">not connected</span></td>';
            var name = player.name;
            tableString += '<td><button onClick=\'App.kickPlayer("' + name + '")\' class="btn">Kick</button></td></tr>';

            $("#playerList").append(tableString);
        },

        kickPlayer : function(player) {
            IO.socket.emit('kick', player);
        },

        verifyStartButton : function() {
            if (IO.players === undefined) return;

            if (IO.players.length < IO.selectedGame.minPlayers){
                $('#btnStart').prop("disabled", true);                
            } else if (IO.players.length > IO.selectedGame.minPlayers) {
                $('#btnStart').prop("disabled", false);                
            } else {
                $('#btnStart').prop("disabled", false);                
            }
        },
    };
jQuery(function($){ 
    IO.init();
    App.init();
}($));