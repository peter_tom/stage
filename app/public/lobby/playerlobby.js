jQuery(function($){    
    'use strict';

    var IO = {

        init: function() {
            var tmp        = document.createElement ('a');
            ;   tmp.href   = document.URL;
            IO.socket = io.connect(tmp.hostname, {
              'sync disconnect on unload': true
            });
            IO.bindEvents();
        },

        bindEvents : function() {
            IO.socket.on('connected', IO.onConnected);
            IO.socket.on('receiveData', IO.onReceiveData);
            IO.socket.on('playGame', IO.onPlayGame);
            IO.socket.on('loadGames', IO.onLoadGames);
            IO.socket.on('redirect', IO.onRedirect);  
            IO.socket.on('getPlayerData', App.onGetPlayerData); 
        },

        onConnected : function() {
            var url = window.location.pathname
            var lobby = url.substring(7, url.length);

            IO.socket.emit('lobbyPlayerSocket', lobby);
        },

        onReceiveData : function(data) {
            App.setInitialData(data);
        },

        onRedirect : function(url) {
            window.location.href = url;
        },
    };

    var App = {
        init: function() {
            App.$doc = $(document);
            App.bindEvents();

            $('lobbyTitle').text("test");
        },

        bindEvents : function() {
            App.$doc.on('click', '#btnUpdate', App.onUpdatePlayerData);
        },

        setInitialData : function(data) { 
            $('room').text(data.room);
        },

        onHostGame : function() {
            console.log("Trying to host game..");
            var game = $("#gamesDropDown").val();

            console.log("Game:" + game);
            IO.socket.emit('hostGame', game);
        },

        onJoinGame : function() {
            console.log("Trying to join a game..");
            var room = $("#inputGameRoom").val();

            IO.socket.emit('joinGame', room);
        },

        onUpdatePlayerData : function() {
            var colorString = $('#color').css('background-color');
            var name = $('#name').val();
            console.log(colorString);  
            var rgb = colorString.match(/\d+/g);
            var color = {
                r : rgb[0],
                g : rgb[1],
                b : rgb[2],
            };

            console.log(color);
            var data = {
                name : name,
                color : color,
            };

            IO.socket.emit('updatePlayerData', data);
        },

        onGetPlayerData : function(data) {
            console.log('onGetPlayerData');
            console.log(data);
            $('#name').val(data.name);
            //$('#color').color.fromRGB(data.color.r, data.color.g, data.color.b);
            var myPicker = new jscolor.color(document.getElementById('color'), {})
            console.log(myPicker);
            myPicker.fromRGB(data.color.r / 255, data.color.g / 255, data.color.b / 255);
        },
    };

    IO.init();
    App.init();
}($));