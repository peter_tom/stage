jQuery(function($){    
    'use strict';

    var App = {
        init: function() {

            cLib.on('initialized', App.onInitialized);

            App.$doc = $(document);
            window.onkeydown = function(e){
                switch(e.which){
                    case 90:
                    App.onMove(0, 0);
                    break;
                    case 83:
                    App.onMove(0.25, 0);
                    break;
                    case 81:
                    App.onMove(1, 0);
                    break;
                    case 68:
                    App.onMove(-0.25, 0);
                    break;
                }
            }
            /*
            $(document).swipe( {
                //Generic swipe handler for all directions
                swipe:function(event, direction, distance, duration, fingerCount) {
                    console.log("direction");
                    if(direction == "up"){
                        swipeupHandler();
                    } else if( direction == "down"){
                        swipedownHandler();
                    } else if (direction == "left") {
                        swipeleftHandler();
                    } else if(direction == "right"){
                        swiperightHandler();
                    }
                },
                //Default is 75px, set to 0 for demo so any distance triggers swipe
                 threshold:0
            });
 
            function swipeupHandler(){
                App.onMove(1);
            }

            function swipedownHandler(){
                App.onMove(2);
            }

            function swipeleftHandler(){
                App.onMove(3);
            }

            function swiperightHandler(){
                App.onMove(4);
            }*/
        },

        onMove : function(y, x) {
            cLib.emit('move', {beta : x, gamma : y});
        },

        onInitialized : function() {
            var rgb = cLib.playerData.color;
            var rgbString = 'rgb(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')';
            document.body.style.background = rgbString;
        },
    };

    App.init();
}($));