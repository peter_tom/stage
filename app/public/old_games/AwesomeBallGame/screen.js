jQuery(function($){  
    var App = {
        init: function() {
            sLib.on('allPlayersLoaded', Game.loadPlayers);
            sLib.on('playerDisconnected', Game.removePlayer);
            sLib.on('gyro', Game.getInputEvent);

            App.$doc = $(document);
            App.bindEvents();
        },

        bindEvents : function() {

        },
    };

     var Game = {
        stage : null,
        context : 'shit',
        canvas : null,
        width : null,
        height : null,
        players : new Array(),
        countdead : null,
        FRICTION : 0.95,
        BALL_SIZE : 0,
        MAX_ACCEL : 0,
        walls : new Array(),
        counter: 0,

        init: function() {
            Game.$doc = $(document);
            Game.canvas = $('#canvas')[0];
            Game.canvas.height = window.innerHeight;
            Game.canvas.width = window.innerWidth;
            
            Game.height = Game.canvas.height;
            Game.width = Game.canvas.width;

            Game.initConstants();

            Game.stage = new createjs.Stage("canvas");
            Game.countdead = 0;

            createjs.Ticker.addEventListener("tick", Game.tick);

            Game.showMenubuttons();

            createjs.Ticker.setFPS(60);
            createjs.Ticker.setPaused(true);
        },

        initConstants : function() {
            var speed_multiplier = 0.000001;
            var ball_multiplier = 0.02;

            if (Game.height > Game.width) {
                Game.BALL_SIZE = Game.height * ball_multiplier;
                Game.MAX_ACCEL = Game.height * speed_multiplier;
            } else {
                Game.BALL_SIZE = Game.width * ball_multiplier;
                Game.MAX_ACCEL = Game.width * speed_multiplier;
            }
        },

        startGame : function() {
            Game.resetGame();
            Game.stage.removeAllChildren();
            createCoolButton(Game.stage, "Back", "32px arial", Game.width * 0.10, Game.height * 0.10, sLib.returnToLobby);
            Game.initMap();
            Game.players.forEach(function(ply) {
                Game.stage.addChild(ply.ball);
            });
            Game.stage.update();
        },

        loadPlayers : function(data) {
            console.log('Loading players');

            data.forEach(function(player) {
                Game.createSpeler(player);
            });
        },

        removePlayer : function(player) {
            console.log('Removed player: ' + player);
            for (var i = 0; i < Game.spelers.length; i++) {
                if (Game.spelers[i].naam == player) {
                    Game.spelers.splice(i, 1);
                    break;
                }
            }

            if(Game.spelers.length < 2){
                sLib.returnToLobby();
            }
        },

        recalculatePosition : function() {
            Game.players.forEach(function(ply) {  
                ply.sX += ply.aX * Game.MAX_ACCEL;
                ply.sY += ply.aY * Game.MAX_ACCEL;

                ply.ball.x += ply.sX;
                ply.ball.y += ply.sY;

                Game.applyFriction(ply);
                Game.checkCollision(ply);
                Game.checkCollisionWithWalls(ply);

                Game.counter++;
                if (Game.counter > 10) {
                    Game.counter = 0;
                    //console.log(Game.MAX_ACCEL);
                    //console.log(ply.ball.x.toFixed(0) + " " + ply.ball.y.toFixed(0) + " A " + ply.aX.toFixed(2) + " " + ply.aY.toFixed(2) + " S " + ply.sX.toFixed(2) + " " + ply.sY.toFixed(2));   
                }
            });
        },

        applyFriction : function(ply) {
            ply.sX = ply.sX * 0.99;
            ply.sY = ply.sY * 0.99;
        },

        checkCollision : function(ply) {
            var SPEEDLOSS = 0.50;
            var margin = Game.BALL_SIZE;
            var x = ply.ball.x;
            var y = ply.ball.y;

            if (x < margin) {
                ply.sX = -(ply.sX * SPEEDLOSS);
                ply.ball.x = margin;
            } else if (x >= Game.width - margin) {
                ply.sX = -(ply.sX * SPEEDLOSS);
                ply.ball.x = Game.width - margin;         
            }

            if (y < margin) {
                ply.sY = -(ply.sY * SPEEDLOSS);
                ply.ball.y = margin;
            } else if (y >= Game.height - margin) {
                ply.sY = -(ply.sY * SPEEDLOSS);
                ply.ball.y = Game.height - margin;   
            }
        },

        checkCollisionWithWalls : function(ply){
            for(var i = 0; i < Game.walls.length; i++){
                if(Game.checkCollisionWithWall(Game.walls[i], ply)){
                    return true;
                }
            }
        },

        intersection : function(wall, ply){
            var SPEEDLOSS = 0.50;
            var margin = Game.BALL_SIZE;
            var x = ply.ball.x + margin/2 + ply.sX;
            var y = ply.ball.y + margin/2 + ply.sY;
            if((x + margin/2 > wall.x) && ((x - (margin*2) + margin/2) < wall.x+wall.width) && (y + margin/2 > wall.y) && ((y - (margin*2) + margin/2) < wall.y+wall.height)){
                console.log("collision");
                return true;
            }
            return false;
        },

        checkCollisionWithWall : function(wall, ply) {
            //Check 4 wall
            var bs = Game.BALL_SIZE;

            function collide(ax, ay, bx, px, py) {
                
                var x = px + (bs / 2);
                //EZ
                if (x  > ax && x < bx)
                    if (py + bs > ay && py < ay)
                        return true;

                if ((Math.sqrt(Math.pow(ax - px, 2) + Math.pow(ay - py, 2))) <= bs)
                    return true;

                if ((Math.sqrt(Math.pow(bx - px, 2) + Math.pow(ay - py, 2))) <= bs)
                    return true;
            }
            var w = wall;
            var x = ply.ball.x;
            var y = ply.ball.y;
            //Upper wall
            if (collide(w.x, w.y, w.x + w.width, x, y)) {
                ply.ball.y -= ply.sY;
                ply.sY = -ply.sY;
                return true;
            } else if (collide(w.y, w.x + w.width, w.y + w.height, y, x)) {
                ply.ball.x -= ply.sX;
                ply.sX = -ply.sX;
            }
            return false
        },
        /*
        checkCollisionWithWall : function(wall, ply){
            if(Game.intersection(wall, ply)){
                var distancex;
                var distancey;
                var margin = Game.BALL_SIZE;
                var x = ply.ball.x + margin/2 + ply.sX;
                var y = ply.ball.y + margin/2 + ply.sY;
                if(x - wall.x < wall.x+wall.width-x){
                    distancex = x - wall.x;
                } else {
                    distancex = wall.x+wall.width-x;
                }

                if(y - wall.y < wall.y+wall.height-y){
                    distancey = y - wall.y;
                } else {
                    distancey = wall.y+wall.height-y;
                }


                console.log(Math.abs(distancex) + " " + Math.abs(distancey));
                if(distancex < distancey){
                    ply.sX = -ply.sX;
                } else {
                    ply.sY = -ply.sY;
                }
                /*
                if(ply.sX > 0){
                    if(ply.sX > ply.sY){
                        ply.sX = -ply.sX;  
                        //ply.ball.x = wall.x-1; 
                    } else {
                        ply.sY = -ply.sY
                        //ply.ball.y = wall.y-1; 
                    }
                } else {
                    if(ply.sX < ply.sY){
                        ply.sX = -ply.sX;   
                        //ply.ball.x = wall.x+wall.width+1; 
                    } else {
                        ply.sY = -ply.sY;
                        //ply.ball.y = wall.y+wall.height+1; 
                    } 
                }
                
                return true;
            }
        },
        */
        tick : function(){
            if(!createjs.Ticker.getPaused()) {
                Game.recalculatePosition();

                Game.stage.update();
            }
        },

        centerObj : function(obj) {
            obj.x = Game.width / 2 - obj.getMeasuredWidth() / 2;
            obj.y = Game.height / 2 - obj.getMeasuredHeight() / 2;
        },

        findSpeler : function(name) {
            for (var i = 0; i < Game.players.length; i++) {
                if (Game.players[i].name == name)
                    return Game.players[i];
            }
            return undefined;
        },

        createSpeler : function(ply){
            var player = new Object();

            player.color = createjs.Graphics.getRGB(ply.color.r, ply.color.g, ply.color.b, 1);

            player.ball = new createjs.Shape();
            player.ball.graphics.beginFill(player.color).drawCircle(0, 0, Game.BALL_SIZE);

            player.name = ply.name;
            Game.reset(player);

            Game.players.push(player); 
        },

        getInputEvent : function(data){
            var player = Game.findSpeler(data.player);
            if (player !== undefined) {
                player.aY = data.data.beta; // 0 - 1
                player.aX = data.data.gamma;// 0 -1
            }
        },

        resetGame : function(){
            Game.players.forEach(function(player) {
                Game.reset(player);
            });

            Game.countdead = 0;
            createjs.Ticker.setPaused(false);
        },

        reset : function(player){
            player.ball.x = Game.width / 2 ;
            player.ball.y = Game.height / 2;
            
            player.aX = 0;
            player.aY = 0;

            player.sX = 0;
            player.sY = 0;
            
            player.dead = false;
            player.wins = 0;
        },

        die : function(player){
            player.dead = true;
            Game.countdead++;
        },

        showMenubuttons : function(){
            createCoolButton(Game.stage, "Start game", "48px arial", Game.width * 0.50, Game.height * 0.15, Game.startGame);
            createCoolButton(Game.stage, "Return to lobby", "48px arial", Game.width * 0.50, Game.height * 0.60, sLib.returnToLobby);
           
            Game.stage.update();
        },

        initMap : function(){
            Game.createWall(Game.width/4, Game.height/2, Game.BALL_SIZE, Game.BALL_SIZE*4);
  /*          Game.createWall(Game.width/4, Game.height/2, Game.BALL_SIZE*4, Game.BALL_SIZE);
            Game.createWall((Game.width/4)-Game.BALL_SIZE*4, Game.height/2, Game.BALL_SIZE*4, Game.BALL_SIZE);
            Game.createWall((Game.width/4)-Game.BALL_SIZE*4, (Game.height/2)-Game.BALL_SIZE*4, Game.BALL_SIZE, Game.BALL_SIZE*4);
    */    },

        createWall : function(x,y, width, height){
            wall = new createjs.Shape();
            
            wall.x = x;
            wall.y = y;
            wall.width = width;
            wall.height = height;  
            
            wall.graphics.beginFill("rgba(266,266,266, 1)").drawRect(0, 0, width, height);
            Game.walls.push(wall);
            Game.stage.addChild(wall);
        },

    };

    App.init();
    Game.init();
}($));