jQuery(function($){    
    'use strict';

    var App = {
        moving : undefined,
        shooting : false,

        init: function() {
            cLib.init();
            cLib.on('initialized', App.onInitialized);

            App.$doc = $(document);
            window.onkeydown = function(e){   
                switch(e.which){
                    case 90:
                    App.onShoot();
                    break;
                    case 81:
                    App.onMove("Left");
                    break;
                    case 68:
                    App.onMove("Right");
                    break;
                }
            }

            window.onkeyup = function(e) {
                switch(e.which){
                    case 90:
                    App.onStopShoot();
                    break;
                    case 81:
                    App.onStopMove("Left");
                    break;
                    case 68:
                    App.onStopMove("Right");
                    break;
                }
            }
        },

        onMove : function(dir) {
            if (App.moving === undefined) {
                console.log("Move start " + dir);
                cLib.emit('move', dir);
                App.moving = dir;
            }
        },

        onStopMove : function(dir) {
            if (App.moving === dir) {
                console.log("Move stop " + dir);
                cLib.emit('stopMove', dir);
                App.moving = undefined;
            }
        },

        onShoot : function() {
            if (!App.shooting) {
                console.log("Shoot start");
                cLib.emit('shoot');
                App.shooting = true;
            }
        },

        onStopShoot : function() {
            console.log("Shoot stop");
            cLib.emit('stopShoot');
            App.shooting = false;
        },

        onInitialized : function() {
            var rgb = cLib.playerData.color;
            var rgbString = 'rgb(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')';
            document.body.style.background = rgbString;
        },
    };

    App.init();
}($));