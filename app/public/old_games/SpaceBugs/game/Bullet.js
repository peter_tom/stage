var bulletId = 0;

function Bullet(owner, movePattern) {
    //undefined owner = enemy
    this.owner = owner;
    this.id = bulletId++;

    var bulletW = 3;
    var bulletH = Math.ceil(Game.height * 0.01);

    var obj = new createjs.Shape();
    obj.graphics.beginStroke(owner.color).drawRect(0, 0, bulletW, bulletH);

    obj.x = owner.shootPos().x - bulletW / 2;
    obj.y = owner.shootPos().y - bulletH;
    this.obj = obj;

    this.movePattern = movePattern;
}

Bullet.prototype.tick = function() {
    this.obj.x += this.movePattern.x;
    this.obj.y -= this.movePattern.y;
    this.checkBounds();
};

Bullet.prototype.checkBounds = function() {
    if (this.obj.x < 0 || this.obj.x > Game.width)
        this.destroy();
    else if (this.obj.y < 0 || this.obj.y > Game.height)
        this.destroy();
};

Bullet.prototype.destroy = function() {
    Game.field.removeChild(this.obj);

    var index = -1;
    for (var i = 0; i < Game.bullets.length && index === -1; i++)
        if (Game.bullets[i].id === this.id)
            index = i;
    
    Game.bullets.splice(index, 1);
};