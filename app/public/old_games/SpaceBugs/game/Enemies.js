//Standaard shootPos midden vanonder
function shootPos() {
	return {
		x : this.obj.x + this.obj.w / 2,
		y : this.obj.y + this.obj.h,
	};
}
var enemyId = 0;
function Basic() {
	this.color = "#B53C3C";

	var obj = new createjs.Shape();
	
	obj.w = Game.width * 0.05;
	obj.h = Game.height * 0.08;

	obj.graphics.beginStroke(this.color).drawRect(0, 0, obj.w, obj.h);
	obj.x = Game.width * Math.random();
	obj.y = 0;

	this.id = enemyId++;
	this.obj = obj;
	this.shootPos = shootPos;
	this.weapon = new SingleEnemy(this);
	this.weaponTick = 0;
	this.pattern = {
		x : function() {
			return Game.width * (Math.sin(Game.getTime() * 4) / 200);
		},
		y : function() {
			return Game.height * 0.003;
		},
	};
}

Basic.prototype.tick = function() {
	this.obj.x += this.pattern.x();
	this.obj.y += this.pattern.y();

	//console.log("y: " + Math.round(this.obj.y) + " x: " + Math.round(this.obj.x));

	if (this.obj.y > Game.height) {
		this.destroy();
	}

	if (this.weaponTick == this.weapon.speed) {
		this.weapon.shoot();
		this.weaponTick = 0;
	} else 
		this.weaponTick++;
};

Basic.prototype.destroy = function() {
    Game.field.removeChild(this.obj);

    var index = -1;
    for (var i = 0; i < Game.enemies.length && index === -1; i++)
        if (Game.enemies[i].id === this.id)
            index = i;
    
    Game.enemies.splice(index, 1);
};