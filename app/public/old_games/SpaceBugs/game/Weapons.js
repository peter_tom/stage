function TripleTrouble(owner) {
	this.nBullet = 3;
	this.owner = owner;
	this.speed = 8;
}

TripleTrouble.prototype.shoot = function() {
	for(var i = 0; i < this.nBullet; i++)
		this.shootBullet(i)
};

//Shoot a bullet based on the index of the bullt
TripleTrouble.prototype.shootBullet = function(index) {
	var change = Game.width * 0.005;
	var pattern = {
		x : change - change * index,
		y : Game.height * 0.02,
	};

	var bullet = new Bullet(this.owner, pattern);
	Game.bullets.push(bullet);
	Game.field.addChild(bullet.obj);
};

function SingleEnemy(owner) {
	this.nBullet = 1;
	this.owner = owner;
	this.speed = 16;
}

SingleEnemy.prototype.shoot = function() {
	var pattern = {
		x : 0,
		y : -(Game.height * 0.01),
	};

	console.log('shotting');

	var bullet = new Bullet(this.owner, pattern);
	Game.bullets.push(bullet);
	Game.field.addChild(bullet.obj);
}