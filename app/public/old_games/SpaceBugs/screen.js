var App = {
    init: function() {
        sLib.on('allPlayersLoaded', Game.loadPlayers);
        sLib.on('playerDisconnected', Game.removePlayer);
        sLib.on('move', Game.playerStartMove);
        sLib.on('stopMove', Game.playerStopMove);
        sLib.on('shoot', Game.playerStartShoot);
        sLib.on('stopShoot', Game.playerStopShoot);


        App.$doc = $(document);
    },
};

 var Game = {
    stage : null,
    canvas : null,
    width : null,
    height : null,
    players : new Array(),
    bullets : new Array(),
    enemies : new Array(),
    countdead : null,
    bg : new createjs.Container(),
    field : new createjs.Container(),
    time : 0,
    
    init: function() {
        Game.$doc = $(document);
        Game.canvas = $('#canvas')[0];
        Game.canvas.height = window.innerHeight;
        Game.canvas.width = window.innerWidth;
        
        Game.height = Game.canvas.height;
        Game.width = Game.canvas.width;

        Game.stage = new createjs.Stage("canvas");
        //TODO Clean up pls
        var bg = Game.bg;
        bg.x = 0;
        bg.y = 0;
        bg.width = Game.width;
        bg.height = Game.height;

        var bgColor = new createjs.Shape();
        bgColor.graphics.beginFill("#C7D0D4").drawRect(0, 0, bg.width, bg.height);

        bg.addChild(bgColor);

        bg.fps = new createjs.Text("", "10px Arial", "#37464D");
        bg.squares = new Array();
        for (var i = 0; i < 10; i++) {
            var w = Math.random() * Game.width / 5 + 50;
            var h = Math.random() * Game.height / 5 + 50;
            var sq = new createjs.Shape();
            sq.graphics.beginStroke("#A6A6A6").drawRect(0, 0, w, h);
            sq.x = (Math.random() * Game.width);
            sq.y = (Math.random() * Game.height);
            sq.h = h;
            sq.speed = (Game.width / 300);
            sq.tick = function() {
                this.y += sq.speed;
                if (this.y > Game.height) {
                    this.y = -this.h;
                    this.x = Math.random() * Game.width;
                }
            };

            bg.squares.push(sq);
            bg.addChild(sq);
        }

        bg.addChild(bg.fps);
        createCoolButton(bg, "Return", "10px Arial", 40, 30, sLib.returnToLobby);

        Game.stage.addChild(bg);
        Game.stage.addChild(Game.field);
        Game.countdead = 0;

        createjs.Ticker.addEventListener("tick", Game.tick);

        Game.showMenubuttons();

        createjs.Ticker.setFPS(60);
        createjs.Ticker.setPaused(true);
    },

    startGame : function() {
        Game.field.removeAllChildren();
        Game.resetGame();
        createjs.Ticker.setPaused(false);
        var e = new Basic();
                Game.enemies.push(e);
                Game.field.addChild(e.obj);
    },

    loadPlayers : function(data) {
        console.log('Loading players');
        console.log(data);

        data.forEach(function(player) {
            Game.createPlayer(player);
        });
    },

    removePlayer : function(player) {
        for (var i = 0; i < Game.players.length; i++) {
            if (Game.players[i].naam == player) {
                Game.players.splice(i, 1);
                break;
            }
        }

        if(Game.players.length < 1){
            sLib.returnToLobby();
        }
    },

    tick : function(){
        if(!createjs.Ticker.getPaused()){
            Game.bullets.forEach(function(bullet) {
                bullet.tick();
            });

            Game.players.forEach(function(ply) {
                if (ply.moving !== undefined) {
                    if (ply.moving === "Left")
                        ply.obj.x -= 5;
                    else if (ply.moving === "Right")
                        ply.obj.x += 5;
                    //Bounds
                    if (ply.obj.x < 0)
                        ply.obj.x = 0;
                    else if (ply.obj.x > Game.width - ply.objWidth)
                        ply.obj.x = Game.width - ply.objWidth;
                }
                if (ply.shooting) {
                    if (ply.shootCounter === ply.weapon.speed) {
                        ply.weapon.shoot();
                        ply.shootCounter = 0;
                    } else {
                        ply.shootCounter++;
                    }
                }
            });

            Game.enemies.forEach(function(e) {
                e.tick();
            })

            Game.bg.squares.forEach(function(square) {
                square.tick();
            });

            Game.bg.fps.text = createjs.Ticker.getMeasuredFPS();
            Game.stage.update();
            Game.time += 0.0166
            
            if (Game.getTime() === Math.ceil(Game.time)) {
                var e = new Basic();
                Game.enemies.push(e);
                Game.field.addChild(e.obj);
            }
        }
    },

    getTime : function() {
        //2 getallen na de komma
        return Math.ceil(Game.time * 100) / 100;
    },

    centerObj : function(obj) {
        obj.x = Game.width / 2 - obj.getMeasuredWidth() / 2;
        obj.y = Game.height / 2 - obj.getMeasuredHeight() / 2;
    },

    getPlayer : function(name) {
        for (var i = 0; i < Game.players.length; i++) {
            if (Game.players[i].name == name)
                return Game.players[i];
        }
        return false;
    },

    createPlayer : function(ply){
        var player = new Object();
        player.color = createjs.Graphics.getRGB(ply.color.r, ply.color.g, ply.color.b, 1);
        player.name = ply.name;

        Game.players.push(player); 
    },

    playerStartMove : function(data) {
        var ply = Game.getPlayer(data.player);
        //Left or Right
        ply.moving = data.data;
        console.log("start move left");
    },

    playerStopMove : function(data) {
        var ply = Game.getPlayer(data.player);
        ply.moving = undefined;
    },

    playerStartShoot : function(data){
        ply = Game.getPlayer(data.player);
        ply.shooting = true;
    },

    playerStopShoot : function(data){
        ply = Game.getPlayer(data.player);
        ply.shooting = false;
    },

    resetGame : function() {
        Game.players.forEach(function(ply) {
            Game.initPlayer(ply);
        });
    },

    initPlayer : function(player) {
        player.lives = 3;
        player.kills = 0;
        player.score = 0;

        player.objHeight = Game.height * 0.06;
        player.objWidth = Game.width * 0.03;

        //The object we will be drawing on the canvas.
        var obj = new createjs.Shape();
        obj.graphics.beginStroke(player.color);
        obj.graphics.moveTo(player.objWidth/2, 0).lineTo(0, player.objHeight).lineTo(player.objWidth, player.objHeight).lineTo(player.objWidth/2, 0);

        obj.x = Game.width / 2;
        obj.y = Game.height - player.objHeight;

        player.obj = obj;

        player.weapon = new TripleTrouble(player);
        player.shootCounter = 0;
        player.shootPos = function() {
            return {
                x : obj.x + player.objWidth / 2,
                y : obj.y,
            }
        };

        Game.field.addChild(obj);
    },

    die : function(player){
        player.dead = true;
        Game.countdead++;
    },

    showMenubuttons : function(){
        createCoolButton(Game.field, "Start game", "48px arial", Game.width * 0.50, Game.height * 0.15, Game.startGame);
        createCoolButton(Game.field, "Return to lobby", "48px arial", Game.width * 0.50, Game.height * 0.60, sLib.returnToLobby);
       
        Game.stage.update();
    },
};
jQuery(function($){  
    App.init();
    Game.init();
}($));
