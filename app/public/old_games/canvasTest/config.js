var config = {
	name : 'Canvas Test',
	description : 'Easeljs canvas test',
	minPlayers : 0,
	maxPlayers : 64,
};

module.exports = config;