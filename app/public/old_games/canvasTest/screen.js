jQuery(function($){  
    var App = {
        init: function() {
            App.$doc = $(document);
            App.bindEvents();
        },

        bindEvents : function() {
            App.$doc.on('click', '#btnReturn', sLib.returnToLobby);
        },
    };

    var Game = {
        stage : undefined,

        width : undefined,
        height : undefined,

        init : function() {
            Game.$doc = $(document);
            Game.canvas = $('#canvas')[0];
            Game.canvas.height = window.innerHeight;
            Game.canvas.width = window.innerWidth;
            
            Game.height = window.innerHeight;
            Game.width =  window.innerWidth;

            Game.stage = new createjs.Stage("canvas");

            createCoolButton(Game.stage, "Start", "24px arial", 100, 200);
            createCoolButton(Game.stage, "Stop", "18px arial", 100, 400, "red", "yellow", "pink");

            Game.stage.update();
        },
    };

    App.init();
    Game.init();
}($));