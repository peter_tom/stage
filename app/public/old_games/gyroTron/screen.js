jQuery(function($){  
    var App = {
        init: function() {
            sLib.on('allPlayersLoaded', Game.loadPlayers);
            sLib.on('playerDisconnected', Game.removePlayer);
            sLib.on('gyro', Game.getInputEvent);

            App.$doc = $(document);
            App.bindEvents();
        },

        bindEvents : function() {

        },
    };

     var Game = {
        stage : null,
        context : 'shit',
        canvas : null,
        width : null,
        height : null,
        spelers : new Array(),
        line : null,
        linewidth : null,
        arrayline : new Array(),
        countdead : null,
        color : '#000000',

        init: function() {
            Game.$doc = $(document);
            Game.canvas = $('#canvas')[0];
            Game.canvas.height = window.innerHeight;
            Game.canvas.width = window.innerWidth;
            
             //Game.canvas.height = $(document).height() - $(document).height() * 0.15;
             //Game.canvas.width = $(document).width() - $(document).height() * 0.15;

            Game.context = canvas.getContext('2d');
            Game.height = Game.canvas.height;
            Game.width = Game.canvas.width;

            Game.stage = new createjs.Stage("canvas");
            Game.countdead = 0;

            Game.movespeed = 1;
            Game.linewidth = 5;
            Game.context.lineWidth = Game.linewidth;
            createjs.Ticker.addEventListener("tick", Game.tick);

            Game.context.font = "20pt sans-serif";
            Game.context.fillStyle = "#000000";
            Game.context.fillText("Waiting for players..", Game.width / 2 - 250, Game.height / 2); 
            Game.context.stroke();

            Game.showMenubuttons();

            createjs.Ticker.setFPS(120);
            createjs.Ticker.setPaused(true);
            //
        },

        startGame : function() {
            Game.resetGame();
            Game.stage.removeAllChildren();
            Game.stage.update();
        },

        loadPlayers : function(data) {
            console.log('Loading players');
            console.log(data);

            data.forEach(function(player) {
                Game.createSpeler(player);
            });
            /*
            Game.context.fillStyle = "#FFFFFF";
            Game.context.fillRect(0,0, Game.width, Game.height);
            Game.context.stroke();
            Game.stage.update();

            */

            //$('#btnStart').prop("disabled", false); 
        },

        removePlayer : function(player) {
            console.log('Removed player: ' + player);
            for (var i = 0; i < Game.spelers.length; i++) {
                if (Game.spelers[i].naam == player) {
                    Game.spelers.splice(i, 1);
                    break;
                }
            }

            if(Game.spelers.length < 2){
                sLib.returnToLobby();
            }
        },

        tick : function(){
            var movSpd = Game.movespeed;
            if(!createjs.Ticker.getPaused()){
                Game.spelers.forEach(function(speler){  
                    if(!speler.dead){
                        Game.playerMoveTo(speler);
                        if(speler.forward){
                            if(speler.y -movSpd < 0){
                                Game.die(speler);
                            } else {
                                speler.y -=movSpd;
                            }
                        } else if(speler.backward){
                            if(speler.y + movSpd > Game.height){
                                Game.die(speler);
                            } else {
                                speler.y +=movSpd;
                            }
                        }
                        else if(speler.left){
                            if(speler.x - movSpd < 0){
                                Game.die(speler);
                            } else {
                                speler.x -=movSpd;
                            }
                        }
                        else if(speler.right){
                            if(speler.x + movSpd > Game.width){
                                Game.die(speler);
                            } else {
                                speler.x +=movSpd;
                            }
                        }

                        for(var i =0; i < Game.arrayline.length; i++){
                            if(Game.arrayline[i].x  == speler.x  && Game.arrayline[i].y == speler.y ){
                                if(!speler.dead){
                                Game.die(speler);
                                }
                            }
                        }

                        if(Game.countdead == Game.spelers.length -1){
                            Game.spelers.forEach(function(player){
                                if(!player.dead){
                                    var background = new createjs.Shape();
                                    console.log(player.color);
                                    background.graphics.beginFill(player.color).drawRect(0, 0, Game.width, Game.height);
                                    Game.stage.addChild(background);
                                    
                                    Game.showMenubuttons();
                                    if(!player.won){
                                        player.won = true;
                                        player.wins++;   
                                    }
                                    var winnertext = new createjs.Text(player.naam + " won the game " + player.wins + " times",  "23pt sans-serif", "white");
                                    Game.centerObj(winnertext);

                                    //TODO: Rect behind text
                                    var highscore = new createjs.Text("", "arial 28px", "#FFF");

                                    var highString = "Player highscore table\n";

                                    var highscoreArray = Game.spelers;
                                    function compare(a, b) {
                                        console.log(a);
                                        return b.wins-a.wins;
                                        /*
                                        if (a.wins < b.wins)
                                            return -1;
                                        if (b.wins < a.wins)
                                            return 1;
                                        return 0;*/
                                    }
                                    highscoreArray.forEach(function(ply) {
                                        console.log(ply.wins);
                                    });
                                    highscoreArray.sort(compare);
                                    console.log('na sort');
                                    highscoreArray.forEach(function(ply) {
                                        console.log(ply.wins);
                                    });
                                    highscoreArray.forEach(function(ply) {
                                        highString += ply.wins + "  -  " + ply.naam + "\n";
                                    });

                                    highscore.text = highString;
                                    highscore.x = 25;
                                    highscore.y = 25;

                                    Game.stage.addChild(highscore);

                                    Game.stage.addChild(winnertext);

                                    Game.stage.update();
                                    Game.stage.updateContext(Game.context);
                                    createjs.Ticker.setPaused(true);
                                }
                            });
                        } else {
                           Game.playerLineTo(speler); 
                        }

                        

                        var position = new Object();
                        position.x = speler.x;
                        position.y = speler.y;

                        if(speler.skip < 20 && speler.boolskip == true){
                            speler.skip++;
                        } else {
                            speler.boolskip = false;
                            speler.skip = 0;
                            Game.arrayline.push(position);
                        }

                        if(speler.boolskip == false){
                            var getal = Math.random() * 100;
                            var random = getal < 0.5;
                            speler.boolskip = random;
                        }
                        
                        
                    }
                });
            }
        },

        centerObj : function(obj) {
            obj.x = Game.width / 2 - obj.getMeasuredWidth() / 2;
            obj.y = Game.height / 2 - obj.getMeasuredHeight() / 2;
        },

        findSpeler : function(name) {
            for (var i = 0; i < Game.spelers.length; i++) {
                if (Game.spelers[i].naam == name)
                    return Game.spelers[i];
            }
            return false;
        },

        createSpeler : function(ply){
            var player = new Object();
            player.x = 10 + Math.floor(Math.random() * (Game.width - 200));
            player.y = 10 + Math.floor(Math.random() * (Game.height - 200));

            Game.setDefaultDirection(player);

            player.color = createjs.Graphics.getRGB(ply.color.r, ply.color.g, ply.color.b, 1);
            player.dead = false;
            player.naam = ply.name;
            player.skip = 0;
            player.boolskip = false;
            player.won = false;
            player.wins = 0;

            Game.spelers.push(player); 
        },

        showPlayers : function(){
            Game.spelers.forEach(function(speler){
                var newlabel = document.createElement("label");
                newlabel.textContent = "Player " + speler.naam;
                newlabel.style.backgroundColor = speler.color;
                document.getElementById("spelers").appendChild(newlabel);
            });
        },

        getInputEvent : function(data){
            if (!createjs.Ticker.getPaused())
                console.log(data);
            var player = Game.findSpeler(data.player);
            var newB, newG;

            var b = data.data.beta;
            var g = data.data.gamma;

            if(b <= 45 && b >= -45){
                newB = b;
            } else{
                if(b < 45){
                    b = -45;
                } else {
                    b = 45;
                }
                newB = b;
            }
            if(g <= 45 && g >= -45){
               newG = g;
            } else {
                if(g < 45){
                    g = -45;
                } else {
                    g = 45;
                }
                newG = g;
            }
            if (Math.abs(newG / 45) > Math.abs(newB / 45)) {
                dir = 3;
                if (newG > 0)
                    dir = 4;
            } else {
                dir = 1;
                if (newB > 0)
                    dir = 2;
            }
            if (player) {
                switch(dir){
                    case 1:
                    if(!player.backward){
                        player.forward = true;
                        player.backward = false;
                        player.left = false;
                        player.right = false;
                    }
                    break;
                    case 2:
                    if(!player.forward){
                        player.forward = false;
                        player.backward = true;
                        player.left = false;
                        player.right = false;
                    }
                    break;
                    case 3:
                    if(!player.right){
                        player.forward = false;
                        player.backward = false;
                        player.left = true;
                        player.right = false;
                    }   
                    break;
                    case 4:
                    if(!player.left){
                        player.forward = false;
                        player.backward = false;
                        player.left = false;
                        player.right = true;
                    }
                    break;
                }
            }
        },

        resetGame : function(){
            Game.spelers.forEach(function(player) {
                Game.reset(player);
            });

            Game.context.clearRect(0, 0, Game.width, Game.height);
            Game.arrayline = new Array();
            Game.countdead = 0;
            createjs.Ticker.setPaused(false);
        },

        reset : function(speler){
            speler.x = 10 + Math.floor(Math.random() * (Game.width - 200));
            speler.y = 10 + Math.floor(Math.random() * (Game.height - 200));

            Game.setDefaultDirection(speler);
            speler.dead = false;
            speler.won = false;
        },

        playerMoveTo : function(player){
            if(!player.boolskip){
                Game.context.beginPath();
                Game.context.strokeStyle = player.color;
                Game.context.moveTo(player.x, player.y);     
            }
        },

        playerLineTo : function(player){
            if(!player.boolskip){
                Game.context.lineTo(player.x, player.y);
                Game.context.stroke();       
            }     
        },

        die : function(player){
            player.dead = true;
            Game.countdead++;
        },

        setDefaultDirection : function(player){
            player.forward = false;
            player.backward = false;
            player.left = false;
            player.right = false;

            var direction = false;
            if(player.x < Game.width/4 && !direction){
                player.right = true;
                direction = true;
            } else if (player.x > Game.width-Game.width/4  && !direction){
                player.left = true;
                direction = true;
            } else if(player.y < Game.height/2  && !direction){
                player.backward = true;
                direction = true;
            } else if(player.y > Game.height/2 && !direction) {
                player.forward = true;
                direction = true;
            }
        },

        showMenubuttons : function(){
            createCoolButton(Game.stage, "Start game", "48px arial", Game.width * 0.50, Game.height * 0.15, Game.startGame);
            createCoolButton(Game.stage, "Return to lobby", "48px arial", Game.width * 0.50, Game.height * 0.60, sLib.returnToLobby);
           
            Game.stage.update();
        },

    };

    App.init();
    Game.init();
}($));