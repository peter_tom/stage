var App = {
    init: function() {
        App.$doc = $(document);
        App.bindEvents();
    },

    bindEvents : function() {
        App.$doc.on('click', '#btnUp', App.onTest1);
        cLib.on('test1', App.onTest1Recieve);
        cLib.on('cError', function(msg) {
            console.log('error: ' + msg);
        });
        cLib.on('1ply', function(msg) {
            console.log(msg);
        });
    },

    onTest1Recieve : function(msg) {
        console.log(msg);
    },

    onTest1 : function() {
        console.log('test1 press');
        cLib.emit('cTest1', 'test1');
    },

    onTest2 : function() {
        data = {
            test1 : "test1",
            test2 : "test2",
        };

        sLib.emit("test2", data);
    },

    onPlayersLoaded : function() {

    },
};

jQuery(function($){  
    App.init();
}($));