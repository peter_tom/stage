var App = {
    init: function() {
        App.$doc = $(document);
        App.bindEvents();
    },

    bindEvents : function() {
        App.$doc.on('click', '#btnTest1', App.onTest1);
        App.$doc.on('click', '#btnReturn', App.onReturnToLobby);
        sLib.on('cTest1', App.onCTest1);
        sLib.on('allPlayersLoaded', function(players) {
            console.log('allPlayersLoaded client');
        });
    },

    onReturnToLobby : function() {
        sLib.returnToLobby()
    },

    onCTest1 : function(data) {
        console.log(data.player + ' send this: ' + data.data);
    },

    onTest1 : function() {
        sLib.emit(sLib.players[0].name, "test1", "Test 1!");
    },

    onTest2 : function() {
        data = {
            test1 : "test1",
            test2 : "test2",
        };

        sLib.emit("test2", data);
    },

    onPlayersLoaded : function() {

    },
};

jQuery(function($){  
    App.init();
}($));