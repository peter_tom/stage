jQuery(function($){    
    'use strict';

    var IO = {

        init: function() {
            IO.socket = io.connect();
            IO.bindEvents();
        },

        bindEvents : function() {
            IO.socket.on('connected', IO.onConnected);
        },

        onConnected : function() {
            var pageUrl = window.location.search.substring(1);
            var urlVariables = pageUrl.split('&');
            var parameter = urlVariables[0].split('=');

            var roomP = -1;

            if (parameter[0] == 'room')
                roomP = parameter[1];

            console.log('room:' + roomP);

            var data = {
                game : 'spel1',
                mode : 'controller',
                room : roomP,
            };
            IO.socket.emit('loadSocket', data);
        },

        move : function(dir) {
            IO.socket.emit('moveController', dir);
        },
    };

    var App = {
        init: function() {
            App.$doc = $(document);
            App.bindEvents();
        },

        bindEvents : function() {
            App.$doc.on('click', '#btnLeft', App.onMove);
            App.$doc.on('click', '#btnRight', App.onMove);
            App.$doc.on('click', '#btnUp', App.onMove);
            App.$doc.on('click', '#btnDown', App.onMove);
        },

        onMove : function() {
            console.log("Moving..");
            
            var dir = $(this).val();
            IO.move(dir);
        }
    };

    IO.init();
    App.init();
    //Game.init();
}($));