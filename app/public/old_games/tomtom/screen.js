jQuery(function($){  
    var IO = {

        init: function() {
            IO.socket = io.connect();

            IO.bindEvents(); 
        },

        bindEvents : function() {
            IO.socket.on('connected', IO.onConnected);
            IO.socket.on('sync', IO.onSync);
            IO.socket.on('roomId', IO.onRoomId);
            IO.socket.on('socketLoaded', IO.onSocketLoaded);
        },

        onConnected : function() {
            console.log(IO.socket);
            data = {
                game : 'tomtom',
                mode : 'screen',
            };
            IO.socket.emit('loadSocket', data);
        },

        onSocketLoaded : function() {
            IO.socket.emit('getRoom');
        },

        onRoomId : function(id) {
            console.log(id);
        },

        onSync : function(data) {
            Game.sync(data)
        },

    };

    var App = {
        init: function() {
            App.$doc = $(document);
            App.bindEvents();
        },

        bindEvents : function() {
            App.$doc.on('click', '#btnTest', App.onMove);
        },

        onMove : function() {
            console.log("Moving..");
            
            IO.socket.emit('getRoom');
        }
    };

    var Game = {

        init: function() {
            Game.stage = new createjs.Stage("canvas");
            Game.rectangle = new createjs.Shape();
            Game.rectangle.graphics.beginFill("red").drawRect(100, 100, 100, 100);
            
            Game.stage.addChild(Game.rectangle);
            Game.stage.update();
        },

        sync : function(data) {
            console.log('syncing..');
            Game.rectangle.x = data.x;
            Game.rectangle.y = data.y;

            Game.stage.update();
        },
    };

    IO.init();
    App.init();
    Game.init();
}($));