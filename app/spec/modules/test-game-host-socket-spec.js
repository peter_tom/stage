var GameHostSocket = require('../../gameHostSocket');
var lobbyManager = require('../../lobbyManager');
var User = require('../../user');
var Lobby = require('../../lobby');

describe("gameHostSocket", function() {
	var HOST_SESSION_ID = "100";
	var PLAYER_SESSION_ID = "200";
	var hostSocket;

	beforeEach(function() {
		//Setup a fresh socket
		hostSocket = {
			hasRedirected : false,
			hasEmited : false,
			hasEmitedAll: false,
			initialized : false,
			sError : false,
			allPlayersLoaded : false,
			events : new Array(),
			handshake : {
				sessionId : HOST_SESSION_ID,
			},

			emit : function(event, args) {
				if (event === "redirect")
					hostSocket.hasRedirected = true;
				if (event === "initialized")
					hostSocket.initialized = true;
				if(event === "sError")
					hostSocket.sError = true;
				if(event === "allPlayersLoaded")
					hostSocket.allPlayersLoaded = true;
			},
			join : function(room) {
				hostSocket.room = room;
			},
			on : function(event, func) {
				hostSocket.events[event] = func;
			},
			broadcast : {
				emit : function(func, data){
					hostSocket.hasEmitedAll = true;
				},
			},
		};
	});

	afterEach(function() {
		//Reset lobbyManager
		lobbyManager.reset();
	});

	describe("init", function() {
		it("socket defined", function() {
			var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
			lobby.playing = true;

			GameHostSocket.init(undefined, hostSocket);
			expect(hostSocket.initialized).toBe(true);

		});

		it("soccket undefined", function() {
			GameHostSocket.init(undefined, hostSocket);
			expect(hostSocket.initialized).toBe(false);
			expect(hostSocket.hasRedirected).toBe(true);
		});
	});

	describe("onEmit", function() {
		describe("lobby defined", function() {
			describe("data defined", function() {
				it("player defined", function() {
					var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
					var user1 = lobby.addPlayer("user1");

					user1.name = "playername";
					user1.socket = {
						emit : function(event, args) {
							if (event === "foo")
								hostSocket.hasEmited = true;
						},
					};
					
					lobby.playing = true;

					GameHostSocket.init(undefined, hostSocket);
					hostSocket.onEmit = hostSocket.events["emit"];
					var data = {
						player : "playername",
						data : "foo",
						func : "foo",
					};

					hostSocket.onEmit(data);
					expect(hostSocket.hasEmited).toBe(true);
				});

				it("player undefined", function() {
					var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
					lobby.playing = true;

					GameHostSocket.init(undefined, hostSocket);
					hostSocket.onEmit = hostSocket.events["emit"];
					var data = {
						player : "playername",
						data : "foo",
						func : "foo",
					};

					hostSocket.onEmit(data);
					expect(hostSocket.sError).toBe(true);
				});
			});

			it("data undefined", function() {
				var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
				var user1 = lobby.addPlayer("user1");
				user1.name = "playername";
				user1.socket = {
					emit : function(event, args) {
						if (event === "foo")
							hostSocket.hasEmited = true;
					},
				};

				lobby.playing = true;

				GameHostSocket.init(undefined, hostSocket);
				hostSocket.onEmit = hostSocket.events["emit"];
				var data = undefined;
				hostSocket.onEmit(data);
				expect(hostSocket.sError).toBe(true);
			});
		});

		it("lobby undefined", function () {
			var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
			lobby.playing = true;

			GameHostSocket.init(undefined, hostSocket);
			hostSocket.onEmit = hostSocket.events["emit"];
			lobbyManager.removeLobby(lobby.id);

			hostSocket.onEmit("foo");
			expect(hostSocket.hasRedirected).toBe(true);
		});
	});

	describe("onEmitAll", function (){
		it("lobby defined", function () {
			lobby = lobbyManager.createLobby(HOST_SESSION_ID);
			lobby.playing = true;

			GameHostSocket.init(undefined, hostSocket);
			hostSocket.onEmitAll = hostSocket.events["emitAll"];

			var data = {
				player : "playername",
				data : "foo",
				func : "foo",
			};

			hostSocket.onEmitAll(data);
			expect(hostSocket.hasEmitedAll).toBe(true);
		});

		it("lobby undefined", function () {
			var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
			lobby.playing = true;

			GameHostSocket.init(undefined, hostSocket);
			hostSocket.onEmitAll = hostSocket.events["emit"];
			lobbyManager.removeLobby(lobby.id);

			hostSocket.onEmitAll("foo");
			expect(hostSocket.hasRedirected).toBe(true);
		});
	});

	describe("onReturnToLobby", function() {
		describe("lobby defined", function () {
			it("player socket defined in foreach" , function(){
				var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
				var user1 = lobby.addPlayer("user1");

				user1.name = "playername";
				user1.socket = {
					emit : function(event, args) {
						if (event === "foo")
							hostSocket.hasEmited = true;
					},
				};

				lobby.playing = true;

				GameHostSocket.init(undefined, hostSocket);
				hostSocket.onReturnToLobby = hostSocket.events["returnToLobby"];

				hostSocket.onReturnToLobby();
				expect(hostSocket.hasRedirected).toBe(true);

				lobby = lobbyManager.lobby(lobby.id);
				user1 = lobby.getPlayerByName(user1.name);
				expect(user1.socket).not.toBeDefined();
			});
		});

		it("lobby undefined", function () {
			var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
			lobby.playing = true;

			GameHostSocket.init(undefined, hostSocket);
			hostSocket.onReturnToLobby = hostSocket.events["returnToLobby"];
			lobbyManager.removeLobby(lobby.id);

			hostSocket.onReturnToLobby();
			expect(hostSocket.hasRedirected).toBe(true);
		});
	});

	describe("checkPlayerState", function() {
		it("allPlayersLoaded", function ()  {
			var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
			var user1 = lobby.addPlayer("user1");
			user1.name = "playername";
			user1.socket = {
				emit : function(event, args) {
					if (event === "foo")
						hostSocket.hasEmited = true;
				},
			};

			lobby.playing = true;

			GameHostSocket.init(undefined, hostSocket);

			var test = undefined;

			hostSocket.emit = function(func, data) {
				if (func === "allPlayersLoaded") {
					test = data;
				}
			};
			
			hostSocket.notifyConnect();
			expect(test).toBeDefined();
			expect(hostSocket.allPlayersLoaded).toBe(true);
		});
	});

	describe("onPlayerDisconnect", function (){
		it("onPlayerDisconnect emit", function (){
			var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
			var user1 = lobby.addPlayer("user1");
			user1.name = "playername";
			user1.socket = {
				emit : function(event, args) {
					if (event === "foo")
						hostSocket.hasEmited = true;
				},
			};
			
			lobby.playing = true;

			GameHostSocket.init(undefined, hostSocket);

			var test = undefined;

			hostSocket.emit = function(func, data) {
				if (func === "playerDisconnected") {
					test = data;
				}
			};
			
			hostSocket.notifyDisconnect(user1);
			expect(test).toBeDefined(user1);
		});
	});

	describe("onDisconnect", function() {
		it("onDisconnect", function() {
			var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
			var user1 = lobby.addPlayer("user1");

			user1.name = "playername";
			user1.socket = {
				emit : function(event, args) {
					if (event === "redirect")
						hostSocket.hasEmited = true;
				},
			};
			
			lobby.playing = true;

			GameHostSocket.init(undefined, hostSocket);

			hostSocket.onDisconnect = hostSocket.events["disconnect"];
			hostSocket.onDisconnect();

			expect(hostSocket.hasEmited).toBe(true);
		});
	});

});