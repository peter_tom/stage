var GamePlayerSocket = require('../../gamePlayerSocket');
var lobbyManager = require('../../lobbyManager');
var User = require('../../user');
var Lobby = require('../../lobby');

describe("gamePlayerSocket", function() {
	var HOST_SESSION_ID = "100";
	var PLAYER_SESSION_ID = "200";
	var playerSocket;

	beforeEach(function() {	
		//Setup a fresh socket
		playerSocket = {
			hasRedirected : false,
			events : new Array(),
			handshake : {
				sessionId : PLAYER_SESSION_ID,
			},

			emit : function(event, args) {
				if (event === "redirect")
					playerSocket.hasRedirected = true;
				if (event === "cError")
					playerSocket.cError = true;
			},
			join : function(room) {
				playerSocket.room = room;
			},
			on : function(event, func) {
				playerSocket.events[event] = func;
			},
		};
	});

	afterEach(function() {
		//Reset lobbyManager
		lobbyManager.reset();
	});

	describe("init", function() {
		describe("lobby defined", function() {
			it("socket defined", function() {
				var lobby = lobbyManager.createLobby(HOST_SESSION_ID);

				var ply = lobby.addPlayer(PLAYER_SESSION_ID);
				ply.socket = playerSocket;

				GamePlayerSocket.init(undefined, playerSocket);

				expect(playerSocket.hasRedirected).toBe(true);
			});		

			describe("socket undefined", function() {
				it("host on lobby screen", function() {
					var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
					lobby.host.socket = "foo";

					lobby.addPlayer(PLAYER_SESSION_ID);

					GamePlayerSocket.init(undefined, playerSocket);

					expect(playerSocket.hasRedirected).toBe(true);
				});

				it("host on game screen", function() {
					var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
					
					lobby.addPlayer(PLAYER_SESSION_ID);

					GamePlayerSocket.init(undefined, playerSocket);

					expect(playerSocket.hasRedirected).toBe(false);
				});
			});
		});

		it("lobby undefined", function() {
			GamePlayerSocket.init(undefined, playerSocket);

			expect(playerSocket.hasRedirected).toBe(true);
		});
	});

	describe("onEmit", function() {
		describe("lobby defined", function (){
			describe("host defined", function (){
				it("user defined", function () {
					var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
					lobby.playing = true;
					
					var host = {
						emit : function(func, data) {
							host.func = func;
							host.data = data;
						},
					};
					lobby.host.socket = host;

					var ply = lobby.addPlayer(PLAYER_SESSION_ID);

					var sendData = {
						func : "foo",
						data : "bar",
					};

					GamePlayerSocket.init(undefined, playerSocket);
					
					playerSocket.onEmit = playerSocket.events["emit"];
					playerSocket.onEmit(sendData);

					expect(lobby.host.socket.func).toBe(sendData.func);
					expect(lobby.host.socket.data.player).toBe(ply.name);
					expect(lobby.host.socket.data.data).toBe(sendData.data);
				});

				it("user undefined", function () {
					var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
					lobby.playing = true;
					lobby.host.socket = "host";

					var ply = lobby.addPlayer(PLAYER_SESSION_ID);

					GamePlayerSocket.init(undefined, playerSocket);
					playerSocket.sessionId = "1234569784";

					playerSocket.onEmit = playerSocket.events["emit"];
					playerSocket.onEmit(sendData);

					expect(playerSocket.hasRedirected).toBe(true);
				});
			});

			it("host undefined", function (){
					var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
					var ply = lobby.addPlayer(PLAYER_SESSION_ID);

					GamePlayerSocket.init(undefined, playerSocket);
					
					playerSocket.onEmit = playerSocket.events["emit"];
					playerSocket.onEmit(sendData);

					expect(playerSocket.cError).toBe(true);
			});
		});

		it("lobby undefined", function (){
			var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
			lobby.playing = true;
			var ply = lobby.addPlayer(PLAYER_SESSION_ID);

			GamePlayerSocket.init(undefined, playerSocket);
			playerSocket.onEmit = playerSocket.events["emit"];
			lobbyManager.removeLobby(lobby.id);
			playerSocket.onEmit("foo");
			expect(playerSocket.hasRedirected).toBe(true);
		});
	});

	describe("onDisconnect", function() {
		it("lobby defined, user defined, lobby host socket defined", function() {
			var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
			lobby.playing = true;
			var ply = lobby.addPlayer(PLAYER_SESSION_ID);
			var host = {
				notifyDisconnect : function(name) {
					host.test = name;
				},
			};
			lobby.host.socket = host;

			GamePlayerSocket.init(undefined, playerSocket);
			playerSocket.onDisconnect = playerSocket.events["disconnect"];
			playerSocket.onDisconnect();
			expect(lobby.host.socket.test).toBe(ply.name);
		});
	});
});