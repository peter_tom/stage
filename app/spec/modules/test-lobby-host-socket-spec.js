var LobbyHostSocket = require('../../lobbyHostSocket');
var lobbyManager = require('../../lobbyManager');
var Lobby = require('../../lobby');

describe("lobbyHostSocket", function() {
	var HOST_SESSION_ID = "100";
	var hostSocket;

	beforeEach(function() {
		//Setup a fresh socket
		hostSocket = {
			hasRedirected : false,
			room : undefined,
			events : new Array(),
			handshake : {
				sessionId : HOST_SESSION_ID,
			},
			
			emit : function(event, args) {
				if (event === "redirect")
					hostSocket.hasRedirected = true;
			},
			join : function(room) {
				hostSocket.room = room;
			},
			on : function(event, func) {
				hostSocket.events[event] = func;
			},
		};
	});

	afterEach(function() {
		//Reset lobbyManager
		lobbyManager.reset();
	});

	describe("init lobby", function(){
		it("no existing lobby", function() {
			LobbyHostSocket.init(undefined, hostSocket);

			//Test more properties and functions to be present
			expect(hostSocket.hasRedirected).toBe(false);
			expect(hostSocket.lobbyId).toBeDefined();
		});

		describe("existing lobby", function() {
			it("host socket undefined", function() {
				var lobby = lobbyManager.createLobby(HOST_SESSION_ID);

				LobbyHostSocket.init(undefined, hostSocket);

				expect(hostSocket.hasRedirected).toBe(false);
				expect(hostSocket.lobbyId).toBeDefined();
			});

			it("host socket defined", function() {
				var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
				lobby.host.socket = 1;

				LobbyHostSocket.init(undefined, hostSocket);

				expect(hostSocket.hasRedirected).toBe(true);
			});
		});
	});

	describe("onSelectGame", function() {
		it("lobby defined", function() {
			LobbyHostSocket.init(undefined, hostSocket);

			hostSocket.selectGame = hostSocket.events["selectGame"];
			hostSocket.selectGame("kak");

			var lobby = lobbyManager.lobby(hostSocket.lobbyId);

			expect(lobby.game).toBe("kak");
		});

		it("lobby undefined", function() {
			LobbyHostSocket.init(undefined, hostSocket);

			hostSocket.selectGame = hostSocket.events["selectGame"];

			hostSocket.lobbyId = "84518512851";
			hostSocket.selectGame("kak");

			expect(hostSocket.hasRedirected).toBe(true);
		});
	});

	describe("onStartGame", function() {
		it("lobby undefined", function() {
			LobbyHostSocket.init(undefined, hostSocket);

			//Set to undefined lobby
			hostSocket.lobbyId = 156115111;

			hostSocket.startGame = hostSocket.events["startGame"];
			hostSocket.startGame();

			expect(hostSocket.hasRedirected).toBe(true);
		});

		it("lobby defined", function() {
			LobbyHostSocket.init(undefined, hostSocket);

			var lobby = lobbyManager.lobby(hostSocket.lobbyId);

			var user1 = lobby.addPlayer("user1"); 
			lobby.addPlayer("user2");
			
			user1.hasRedirected = false;

			var plySocket = {
				emit : function(event, args) {
					user1.hasRedirected = true;
				},
			};

			user1.socket = plySocket;

			hostSocket.startGame = hostSocket.events["startGame"];
			hostSocket.startGame();

			expect(lobby.playing).toBe(true);
			expect(lobby.host.socket).not.toBeDefined();
			expect(user1.hasRedirected).toBe(true);
			expect(hostSocket.hasRedirected).toBe(true);
		});
	});

	it("update", function() {
		LobbyHostSocket.init(undefined, hostSocket);
		var test;

		hostSocket.emit = function(func, data) {
			if (func === "update") {
				test = data;
			}
		};

		var testString = "We can dance if we want to.";
		hostSocket.update(testString);

		expect(test).toBe(testString);
	});

	describe("onDisconnect", function() {
		describe("lobby defined", function() {
			it("not playing", function () {
				LobbyHostSocket.init(undefined, hostSocket);

				var lobby = lobbyManager.lobby(hostSocket.lobbyId);

				var user1 = lobby.addPlayer("user1");
				lobby.addPlayer("user2");
				lobby.playing = false;

				var plySocket = {
					emit : function(event, args) {
						user1.hasRedirected = true;
					},
				};
				user1.socket = plySocket;

				hostSocket.disconnect = hostSocket.events["disconnect"];
				hostSocket.disconnect();

				expect(user1.hasRedirected).toBe(true);
				expect(lobbyManager.lobby(lobby.id)).not.toBeDefined();
			});

			it("playing", function () {
				LobbyHostSocket.init(undefined, hostSocket);

				var lobby = lobbyManager.lobby(hostSocket.lobbyId);

				lobby.addPlayer("user1");
				lobby.addPlayer("user2");
				lobby.playing = true;

				hostSocket.disconnect = hostSocket.events["disconnect"];
				hostSocket.disconnect();
				expect(lobbyManager.lobby(lobby.id)).toBeDefined();
			});
		});
	});
});