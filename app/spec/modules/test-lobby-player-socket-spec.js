var LobbyPlayerSocket = require('../../lobbyPlayerSocket');
var lobbyManager = require('../../lobbyManager');
var Lobby = require('../../lobby');

describe("lobbyPlayerSocket", function() {
	var HOST_SESSION_ID = "100";
	var PLAYER_SESSION_ID = "200";
	var playerSocket;

	beforeEach(function() {
		//Setup a fresh socket
		playerSocket = {
			hasRedirected : false,
			events : new Array(),
			handshake : {
				sessionId : PLAYER_SESSION_ID,
			},
			
			emit : function(event, args) {
				if (event === "redirect")
					playerSocket.hasRedirected = true;
			},
			join : function(room) {
				playerSocket.room = room;
			},
			on : function(event, func) {
				playerSocket.events[event] = func;
			},
		};
	});

	afterEach(function() {
		//Reset lobbyManager
		lobbyManager.reset();
	});

	describe("init", function() {
		it("no existing lobby", function() {
			LobbyPlayerSocket.init(undefined, playerSocket, undefined);

			expect(playerSocket.hasRedirected).toBe(true);
		});

		describe("existing lobby", function() {
			it("player undefined", function() {
				var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
				
				LobbyPlayerSocket.init(undefined, playerSocket, lobby.id);

				expect(playerSocket.hasRedirected).toBe(false);
				expect(lobby.getPlayerById(PLAYER_SESSION_ID)).toBeDefined();
			});

			describe("player defined", function() {
				it("player socket undefined", function() {
					var lobby = lobbyManager.createLobby(HOST_SESSION_ID);

					var ply = lobby.addPlayer(PLAYER_SESSION_ID);

					LobbyPlayerSocket.init(undefined, playerSocket, lobby.id);

					expect(playerSocket.hasRedirected).toBe(false);
					expect(lobby.getPlayerById(PLAYER_SESSION_ID)).toBe(ply);
				});

				it("player socket defined", function() {
					var lobby = lobbyManager.createLobby(HOST_SESSION_ID);
					
					var ply = lobby.addPlayer(PLAYER_SESSION_ID);
					ply.socket = playerSocket;

					LobbyPlayerSocket.init(undefined, playerSocket, lobby.id);

					expect(playerSocket.hasRedirected).toBe(true);
				});
			});

			it("already hosting a lobby", function() {
				var lobby = lobbyManager.createLobby(PLAYER_SESSION_ID);

				LobbyPlayerSocket.init(undefined, playerSocket, lobby.id);

				expect(playerSocket.hasRedirected).toBe(true);
			});
		});
	});

	describe("onUpdatePlayerData", function() {
		it("lobby undefined", function() {
			var lobby = lobbyManager.createLobby(HOST_SESSION_ID);

			LobbyPlayerSocket.init(undefined, playerSocket, lobby.id);

			lobbyManager.removeLobby(lobby.id);

			playerSocket.updatePlayerData = playerSocket.events["updatePlayerData"];
			playerSocket.updatePlayerData("foo");

			expect(playerSocket.hasRedirected).toBe(true);			
		});

		it("lobby defined", function() {
			var TEST_DATA = {
				name : "foo bar",
				color : {
					r : 1,
					g : 1,
					b : 1,
				},
			}

			var lobby = lobbyManager.createLobby(HOST_SESSION_ID);

			LobbyPlayerSocket.init(undefined, playerSocket, lobby.id);

			playerSocket.updatePlayerData = playerSocket.events["updatePlayerData"];
			playerSocket.updatePlayerData(TEST_DATA);
			
			var ply = lobbyManager.lobby(lobby.id).getPlayerById(PLAYER_SESSION_ID);
			
			expect(ply.name).toBe(TEST_DATA.name);			
			expect(playerSocket.hasRedirected).toBe(false);
		});
	});

	describe("onDisconnect", function() {
		describe("lobby defined", function() {
			it("not playing", function () {
				var lobby = lobbyManager.createLobby(HOST_SESSION_ID);

				LobbyPlayerSocket.init(undefined, playerSocket, lobby.id);

				playerSocket.disconnect = playerSocket.events["disconnect"];
				playerSocket.disconnect();

				var lobby = lobbyManager.lobby(lobby.id);
				
				expect(lobby.getPlayerById(PLAYER_SESSION_ID)).not.toBeDefined();
			});

			it("playing", function () {
				var lobby = lobbyManager.createLobby(HOST_SESSION_ID);

				LobbyPlayerSocket.init(undefined, playerSocket, lobby.id);

				lobby.playing = true;

				playerSocket.disconnect = playerSocket.events["disconnect"];
				playerSocket.disconnect();

				var lobby = lobbyManager.lobby(lobby.id);
				
				expect(lobby.getPlayerById(PLAYER_SESSION_ID)).toBeDefined();
			});
		});
	});
});				