//test-spec.js
var lobbyManager = require('../../lobbyManager');
var User = require('../../user');
var Lobby = require('../../lobby');
var HOST_SESSION_ID = 1;
var USER_SESSION_ID = 2;

describe("user", function() {
	it("User, equals", function() {
		var user1 =  new User("1");
		var user2 = new User("2");
		expect(user1.equals(user2)).toBe(false);
		expect(user1.equals(1)).toBe(false);
		expect(user1.equals(undefined)).toBe(false);
		expect(user1.equals(user1)).toBe(true);
	});
}); 

describe("Lobby", function() {
	var ply;
	var host = new User("1");
	var lobby = new Lobby("1", host);
	var socket = {
		update : function() {

		},
	};
	lobby.host.socket = socket;

	it("addPlayer", function(){
		//creating lobby, check players is empty list
		expect(lobby).toBeDefined();
		expect(lobby.players.length).toBe(0);

		//check addplayer function adds player to players
		ply = lobby.addPlayer("2");
		ply.name = "tom";
		var count = 0;
		lobby.players.forEach(function(val) {
		    if (val !== undefined) { ++count; }
		});
		expect(count).toBe(1);

	});

	it("getPlayerData", function(){
		//check getPlayerData
		expect(lobby.getPlayerData(ply)).toBeDefined();
	});

	it("getAllPlayerData", function(){
		expect(lobby.getAllPlayerData().length).toBe(1);
	});

	it("getPlayerById", function(){
		//test getPlayerById
		expect(lobby.getPlayerById("2")).toBeDefined();
		expect(lobby.getPlayerById("notanid")).not.toBeDefined();
	});

	it("getPlayerByName", function(){
		//test getplayerbyname
		expect(lobby.getPlayerByName(ply.name)).toBeDefined();
	});

	it("removePlayer", function(){
		//test remove player
		lobby.removePlayer("2");
		expect(lobby.players.length).toBe(0);
	});

	it("updatePlayer", function(){
		//test update player, host socket moet defined zijn hiervoor, voorlopg fake socket
		var ply = lobby.addPlayer("2");
		var data = {
			name : "newname",
			color : "#FFFFFF",
		};

		lobby.updatePlayer("2", data);
		expect(ply.name).toBe("newname");
		expect(ply.color).toBe("#FFFFFF");
	});

	it("getUniqueName", function(){		
		//test getUniqueName
		expect(lobby.getUniqueName()).toBeDefined();
	});

	it("isUniqueName", function(){
		ply.name = "newname";
		expect(lobby.isUniqueName(ply.name)).toBe(false);
		expect(lobby.isUniqueName("iets totaal anders")).toBe(true);
	});
});

describe("lobbyManager", function() {
	var lobby;

	beforeEach(function() {
		lobby = lobbyManager.createLobby(HOST_SESSION_ID)
	});

	afterEach(function() {
		lobbyManager.reset();
	});	

	it("createLobby", function() {
	  	expect(lobby).toBeDefined();
	  	expect(lobby.host.sessionId).toBe(1);

	  	var lobby2 = lobbyManager.createLobby(HOST_SESSION_ID);

		expect(lobby2).not.toBeDefined();
  	});

	it("getHostLobby", function() {
	  	var gethostlobby = lobbyManager.getHostLobby(1);
	  	
	  	expect(gethostlobby).toBe(lobby);
	});  	

	it("lobby", function() {
		expect(lobbyManager.lobby(lobby.id)).toBe(lobby);
		expect(lobbyManager.lobby(95511571)).toBe(undefined);
		expect(lobbyManager.lobby(undefined)).toBe(undefined);
	});

	it("removeLobby", function() {
		var id = lobby.id;
		lobbyManager.removeLobby(id);

		expect(lobbyManager.lobby(id)).toBe(undefined);
	});


	it("getPlayerLobby", function() {
		lobby.addPlayer(USER_SESSION_ID);

		expect(lobbyManager.getPlayerLobby(USER_SESSION_ID)).toBe(lobby);
		expect(lobbyManager.getPlayerLobby(9999841599)).toBe(undefined);
		expect(lobbyManager.getPlayerLobby(undefined)).toBe(undefined);
	});
});

