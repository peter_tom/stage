var stats = [];	
var id = 0;

var types = { //enum
	Controller : 0,
	Screen : 1,
};

var subscribers = [];

function subscribe(statId, socket) {
	subscribers.push({statId : statId, socket : socket});
}

function unsubscribe(socket) {
	//TODO: make and use this.

	//subscribers.remove(socket);
}

function notify(lobbyStats) {
	for (var i = 0; i < subscribers.length; i++) {
		var sub = subscribers[i];

		if (sub.statId == lobbyStats.id) {
			sub.socket.emit('notify', lobbyStats);
		}
	}
}

function getLobbyStats(statId) {
	for (var i = 0; i < stats.length; i++) {
		var stat = stats[i];
		
		if (stat.id == statId) {
			return stat;
		}
	}
	return undefined;
}

function registerGame(lobbyId, game) {
	lobbyObj = {
		id : id++,
		lobbyId : lobbyId,
		controllerEmits : {},
		screenEmits : {},
		game : game,
		active : true,
	};

	stats.push(lobbyObj);
}

function finishGame(lobbyId, game) {
	//TODO: Make this and use it.
}

function log(lobbyId, type, func, user) {
	var logged = false;

	for (var i = stats.length - 1; i >= 0 && !logged; i--) {
		if (stats[i].lobbyId === lobbyId) {
			var emits;
			
			//Determine emit type
			if (type === types.Controller) {
				emits = stats[i].controllerEmits;

				if (user !== undefined) {
					if (emits[user] === undefined) {
						emits[user] = {}; //Make new object for user
					}
					if (emits[user][func] === undefined) {
						emits[user][func] = 0;
					}
				}				

				emits[user][func]++;
			} else if (type === types.Screen) {
				emits = stats[i].screenEmits;

				if (emits[func] === undefined) {
					emits[func] = 0;
				} 

				emits[func]++;
			} else {
				console.log(type.Screen);
				console.log('ERROR: logging received invalid type: ' + type);
				break;
			}

			logged = true;
			notify(stats[i]);
		}
	}
}

function getAll() {
	return stats;
}

module.exports.registerGame = registerGame;
module.exports.log = log;
module.exports.type = types;
module.exports.getAll = getAll;
module.exports.subscribe = subscribe;
module.exports.unsubscribe = unsubscribe;
module.exports.notify = notify;
module.exports.getLobbyStats = getLobbyStats;
