function User(sessionId) {
	this.socket = undefined;
	this.sessionId = sessionId;
	this.name = undefined;
	this.color = {
		r : 128,
		g : 0,
		b : 133,
	};
}

User.prototype.equals = function(user) {
	return user !== undefined && user.sessionId === this.sessionId;
};

module.exports = User;