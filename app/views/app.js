jQuery(function($){    
    'use strict';

    var IO = {

        init: function() {
            IO.socket = io.connect();
            IO.bindEvents();
        },

        bindEvents : function() {
            IO.socket.on('connected', IO.onConnected);
            IO.socket.on('joinSite', IO.onJoinSite);
            IO.socket.on('playGame', IO.onPlayGame);
        },

        onConnected : function(data) {
            console.log(data.games);
            data.games.forEach(function(game) {
                $("#gamesDropDown").append('<option value="' + game + '">' + game + '</option>');
            })
        },

        onJoinSite : function(data) {
            console.log("Joined site, people " + data.people);
            Game.addPlayer();
        },

        onPlayGame : function(game) {
            window.location.href = game;
        }
    };

    var App = {
        init: function() {
            App.$doc = $(document);
            App.bindEvents();
        },

        bindEvents : function() {
            App.$doc.on('click', '#btnHostGame', App.onHostGame);
            App.$doc.on('click', '#btnJoinGame', App.onJoinGame);
        },

        onHostGame : function() {
            console.log("Trying to host game..");
            var game = $("#gamesDropDown").val();

            console.log("Game:" + game);
            IO.socket.emit('hostGame', game);
        },

        onJoinGame : function() {
            console.log("Trying to join a game..");
            var room = $("#inputGameRoom").val();

            IO.socket.emit('joinGame', room);
        }
    };

    IO.init();
    App.init();
}($));